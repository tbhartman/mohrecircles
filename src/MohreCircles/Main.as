﻿package MohreCircles
{
	import flash.display.Bitmap;
	import flash.display.GradientType;
	import flash.display.SpreadMethod;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.ActivityEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TextEvent;
	import flash.events.TimerEvent;
	import flash.net.*;
	import flash.text.TextField;
	import flash.ui.Mouse;
	import flash.utils.Timer;
	import flash.text.*;
	import MohreCircles.graphics.IntroAnimObject;
	import MohreCircles.graphics.*;
	
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class Main extends Sprite
	{
		[Embed(source='../../lib/TBH.ttf', fontName = "TBH", unicodeRange="U+0030-U+0039,U+0041-U+005A,U+0061-U+007A,U+003A,U+002E,U+003D,U+0028,U+0029,U+002D,U+00AD", mimeType='application/x-font')]
		public static var TBH:Class;
		Font.registerFont(TBH);
		[Embed(source='../../lib/arialn.ttf', fontName = "ArialNarrow", unicodeRange="U+0030-U+0039,U+0041-U+005A,U+0061-U+007A,U+003A,U+002E,U+003D,U+0028,U+0029,U+002D,U+00AD", mimeType='application/x-font')]
		public static var ArialNarrow:Class;
		Font.registerFont(ArialNarrow);
		[Embed(source='../../lib/ariblk.ttf', fontName = "ArialBlack", unicodeRange="U+0030-U+0039,U+0041-U+005A,U+0061-U+007A,U+003A,U+002E,U+003D,U+0028,U+0029,U+002D,U+00AD", mimeType='application/x-font')]
		public static var ArialBlack:Class;
		Font.registerFont(ArialBlack);
		//[Embed(source = '../../lib/somerights20.png')]
		//public static var CCClass:Class;
		//private var rights:Bitmap = new CCClass();
		private var rightsSprite:Sprite = new Sprite();
		private var rightsSpriteOver:Sprite = new Sprite();

		private var stress3D:Stress3D = new Stress3D(false);
		private var stress2D:Stress3D = new Stress3D(true);
		private var intro:Sprite = new Sprite();
		private var sprite2d:Sprite = new Sprite();
		private var sprite3d:Sprite = new Sprite();
		private var sprite2d_over:Sprite = new Sprite();
		private var sprite3d_over:Sprite = new Sprite();
		private var sprite2d_fore:Sprite = new Sprite();
		private var sprite3d_fore:Sprite = new Sprite();
		
		private var contents:Sprite = new Sprite();
		private var background:Sprite = new Sprite();
		private var background2:Sprite = new Sprite();
		private var animArray:Array = new Array(10);
		private var bgTimer:Timer;
		
		private var slideInTimer2d:Timer = new Timer(1);
		private var slideInTimer3d:Timer = new Timer(1);
		
		private var textField:TextField;
		private var textFormat:TextFormat;
		
		private var txtAuthor:TextField;
		private var clickAuthorSprite:Sprite;
		
		public function Main():void
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		private function Only3d():void
		{
			this.addChild(stress3D);
		}
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			stage.scaleMode = StageScaleMode.SHOW_ALL;
			
			stage.align = StageAlign.TOP;
			
			if (false)
			{
				this.Only3d();
			}
			else
			{
				this.addChild(background2);
				this.background2.graphics.beginFill(0xffffff, 1);
				this.background2.graphics.drawRect(800, -100, 1500, 1025);
				this.background2.graphics.drawRect(-500, -100, 500, 1025);
				this.background2.graphics.drawRect(-300, 525, 2300, 500);
				this.background2.graphics.endFill();
				this.Build();
			}
		}
		private function Build():void
		{
			this.addChildAt(contents, 0);
			this.contents.addChild(intro);
			{
				var txtForm:TextFormat = new TextFormat();
				//txtForm.font = "TBH";
				txtForm.size = 48;
				txtForm.bold = true;
				txtForm.font = "TBH";
				
				var txtFormVersion:TextFormat = new TextFormat();
				txtFormVersion.size = 10;
				txtFormVersion.bold = false;
				txtFormVersion.font = "TBH";
				
				var txtMo:TextField = new TextField();
				txtMo.y = 250;
				txtMo.x = 275;
				txtMo.selectable = false;
				txtMo.embedFonts = true;
				txtMo.defaultTextFormat = txtForm;
				txtMo.text = "Mo";
				this.intro.addChild(txtMo);
				
				var txtH:TextField = new TextField();
				txtH.x = txtMo.x + txtMo.textWidth;
				txtH.y = txtMo.y;
				txtH.selectable = false;
				txtH.embedFonts = true;
				txtH.defaultTextFormat = txtForm;
				txtH.textColor = 0x0000ff;
				txtH.text = "h";
				this.intro.addChild(txtH);
				
				var txtRe:TextField = new TextField();
				txtRe.x = txtH.x + txtH.textWidth;
				txtRe.y = txtMo.y;
				txtRe.selectable = false;
				txtRe.embedFonts = true;
				txtRe.defaultTextFormat = txtForm;
				txtRe.text = "re";
				this.intro.addChild(txtRe);
				
				
				var txtCircle:TextField = new TextField();
				txtCircle.x = txtRe.x + txtRe.textWidth + 10;
				txtCircle.y = txtRe.y;
				txtCircle.selectable = false;
				txtCircle.embedFonts = true;
				txtCircle.defaultTextFormat = txtForm;
				txtCircle.autoSize = TextFieldAutoSize.LEFT;
				txtCircle.text = "Circles";
				this.intro.addChild(txtCircle);
				
				var txtVersion:TextField = new TextField();
				txtVersion.selectable = false;
				txtVersion.embedFonts = true;
				txtVersion.defaultTextFormat = txtFormVersion;
				txtVersion.autoSize = TextFieldAutoSize.LEFT;
				txtVersion.text = "version   1.2";
				this.intro.addChild(txtVersion);
				
				txtAuthor = new TextField();
				txtAuthor.selectable = false;
				txtAuthor.embedFonts = true;
				txtAuthor.defaultTextFormat = txtFormVersion;
				txtAuthor.autoSize = TextFieldAutoSize.LEFT;
				txtAuthor.text = "created by: Tim Hartman";
				this.intro.addChild(txtAuthor);
				
				clickAuthorSprite = new Sprite();
				clickAuthorSprite.graphics.lineStyle(0, 0, 0);
				clickAuthorSprite.graphics.beginFill(0, 0);
				clickAuthorSprite.graphics.moveTo(0, 0);
				clickAuthorSprite.graphics.drawRect(0, 0, txtAuthor.textWidth, txtAuthor.textHeight);
				clickAuthorSprite.graphics.endFill();
				this.intro.addChild(clickAuthorSprite);
				
				clickAuthorSprite.addEventListener(MouseEvent.CLICK, AuthorClickHandler);
				clickAuthorSprite.addEventListener(MouseEvent.MOUSE_OVER, AuthorOverHandler);
				
				txtMo.x = 400 - (txtCircle.x + txtCircle.textWidth - txtMo.x)/2
				txtH.x = txtMo.x + txtMo.textWidth;
				txtRe.x = txtH.x + txtH.textWidth;
				txtCircle.x = txtRe.x + txtRe.textWidth + 20;
				
				var txtWidth:Number = (txtCircle.x + txtCircle.textWidth - txtMo.x);
				var txtHeight:Number = txtH.textHeight;
				
				txtVersion.x = txtMo.x + txtWidth/2 - txtVersion.textWidth/2 - 3;
				txtVersion.y = txtMo.y + txtHeight - 10;
				txtAuthor.x = txtMo.x + txtWidth/2 - txtAuthor.textWidth/2 - 3;
				txtAuthor.y = txtMo.y + txtHeight + 5;
				clickAuthorSprite.x = txtAuthor.x;
				clickAuthorSprite.y = txtAuthor.y;
				
				var logoo:Logo = new Logo(txtWidth + 20, txtHeight/2 + 20, 1/3, false);
				this.intro.addChildAt(logoo,0);
				logoo.x = txtMo.x - 10;
				logoo.y = txtMo.y;
			}
			
			var txtForm2:TextFormat = new TextFormat();
			txtForm2.size = 16;
			txtForm2.bold = true;
			txtForm2.font = "TBH";
			
			sprite2d.x = 200;
			sprite2d.y = 350;
			var height2d:Number = 60;
			var width2d:Number = 150;
			sprite2d.graphics.lineStyle(2, 0xff3333, 1);
			sprite2d.graphics.beginFill(0xff8888, 1);
			sprite2d.graphics.moveTo(0, height2d / 2);
			sprite2d.graphics.lineTo(width2d/3,height2d);
			sprite2d.graphics.lineTo(width2d/3,height2d * 3/4);
			sprite2d.graphics.lineTo(width2d,height2d * 3/4);
			sprite2d.graphics.lineTo(width2d,height2d * 1/4);
			sprite2d.graphics.lineTo(width2d/3,height2d * 1/4);
			sprite2d.graphics.lineTo(width2d/3,0);
			sprite2d.graphics.moveTo(0, height2d / 2);
			sprite2d.graphics.endFill();
			
			sprite2d_fore.x = sprite2d.x;
			sprite2d_fore.y = sprite2d.y;
			sprite2d_fore.graphics.lineStyle(2, 0xffffff, 0);
			sprite2d_fore.graphics.beginFill(0xffffff, 0);
			sprite2d_fore.graphics.moveTo(0, height2d / 2);
			sprite2d_fore.graphics.lineTo(width2d/3,height2d);
			sprite2d_fore.graphics.lineTo(width2d/3,height2d * 3/4);
			sprite2d_fore.graphics.lineTo(width2d,height2d * 3/4);
			sprite2d_fore.graphics.lineTo(width2d,height2d * 1/4);
			sprite2d_fore.graphics.lineTo(width2d/3,height2d * 1/4);
			sprite2d_fore.graphics.lineTo(width2d/3,0);
			sprite2d_fore.graphics.moveTo(0, height2d / 2);
			sprite2d_fore.graphics.endFill();
			
			var txt2d:TextField = new TextField();
			txt2d.defaultTextFormat = txtForm2;
			txt2d.text = "Enter 2D";
			txt2d.embedFonts = true;
			txt2d.selectable = false;
			txt2d.x = sprite2d.x + (width2d * 1/2 - txt2d.textWidth/2);
			txt2d.y = sprite2d.y + (height2d/2 - txt2d.textHeight / 2);
			
			sprite2d_over.visible = false;
			sprite2d_over.x = sprite2d.x;
			sprite2d_over.y = sprite2d.y;
			sprite2d_over.graphics.lineStyle(2, 0x33ff33, 1);
			sprite2d_over.graphics.beginFill(0x88ff88, 1);
			sprite2d_over.graphics.moveTo(0, height2d / 2);
			sprite2d_over.graphics.lineTo(width2d/3,height2d);
			sprite2d_over.graphics.lineTo(width2d/3,height2d * 3/4);
			sprite2d_over.graphics.lineTo(width2d,height2d * 3/4);
			sprite2d_over.graphics.lineTo(width2d,height2d * 1/4);
			sprite2d_over.graphics.lineTo(width2d/3,height2d * 1/4);
			sprite2d_over.graphics.lineTo(width2d/3,0);
			sprite2d_over.graphics.moveTo(0, height2d / 2);
			sprite2d_over.graphics.endFill();
			
			this.sprite2d_fore.addEventListener(MouseEvent.MOUSE_OVER, MouseOver2dHandler);
			this.sprite2d_fore.addEventListener(MouseEvent.MOUSE_DOWN, MouseDown2dHanlder);
			this.contents.addChild(sprite2d);
			this.contents.addChild(sprite2d_over);
			this.contents.addChild(txt2d);
			this.contents.addChild(sprite2d_fore);
			
			sprite3d.x = 800 - sprite2d.x - width2d;
			sprite3d.y = sprite2d.y;
			sprite3d.graphics.lineStyle(2, 0xff3333, 1);
			sprite3d.graphics.beginFill(0xff8888, 1);
			sprite3d.graphics.moveTo(width2d, height2d / 2);
			sprite3d.graphics.lineTo(width2d*2/3,height2d);
			sprite3d.graphics.lineTo(width2d*2/3,height2d * 3/4);
			sprite3d.graphics.lineTo(0,height2d * 3/4);
			sprite3d.graphics.lineTo(0,height2d * 1/4);
			sprite3d.graphics.lineTo(width2d*2/3,height2d * 1/4);
			sprite3d.graphics.lineTo(width2d*2/3,0);
			sprite3d.graphics.moveTo(width2d, height2d / 2);
			sprite3d.graphics.endFill();
			
			sprite3d_over.x = sprite3d.x;
			sprite3d_over.y = sprite3d.y;
			sprite3d_over.visible = false;
			sprite3d_over.graphics.lineStyle(2, 0x33ff33, 1);
			sprite3d_over.graphics.beginFill(0x88ff88, 1);
			sprite3d_over.graphics.moveTo(width2d, height2d / 2);
			sprite3d_over.graphics.lineTo(width2d*2/3,height2d);
			sprite3d_over.graphics.lineTo(width2d*2/3,height2d * 3/4);
			sprite3d_over.graphics.lineTo(0,height2d * 3/4);
			sprite3d_over.graphics.lineTo(0,height2d * 1/4);
			sprite3d_over.graphics.lineTo(width2d*2/3,height2d * 1/4);
			sprite3d_over.graphics.lineTo(width2d*2/3,0);
			sprite3d_over.graphics.moveTo(width2d, height2d / 2);
			sprite3d_over.graphics.endFill();
			
			sprite3d_fore.x = sprite3d.x;
			sprite3d_fore.y = sprite3d.y;
			sprite3d_fore.graphics.lineStyle(2, 0xffffff, 0);
			sprite3d_fore.graphics.beginFill(0xffffff, 0);
			sprite3d_fore.graphics.moveTo(width2d, height2d / 2);
			sprite3d_fore.graphics.lineTo(width2d*2/3,height2d);
			sprite3d_fore.graphics.lineTo(width2d*2/3,height2d * 3/4);
			sprite3d_fore.graphics.lineTo(0,height2d * 3/4);
			sprite3d_fore.graphics.lineTo(0,height2d * 1/4);
			sprite3d_fore.graphics.lineTo(width2d*2/3,height2d * 1/4);
			sprite3d_fore.graphics.lineTo(width2d*2/3,0);
			sprite3d_fore.graphics.moveTo(width2d, height2d / 2);
			sprite3d_fore.graphics.endFill();
			
			var txt3d:TextField = new TextField();
			txt3d.defaultTextFormat = txtForm2;
			txt3d.text = "Enter 3D";
			txt3d.embedFonts = true;
			txt3d.selectable = false;
			txt3d.x = sprite3d.x + width2d * 1/2 - txt2d.textWidth/2;
			txt3d.y = sprite3d.y + height2d/2 - txt2d.textHeight / 2;
			
			this.sprite3d_fore.addEventListener(MouseEvent.MOUSE_DOWN, MouseDown3dHandler);
			this.sprite3d_fore.addEventListener(MouseEvent.MOUSE_OVER, MouseOver3dHandler);
			this.contents.addChild(sprite3d);
			this.contents.addChild(sprite3d_over);
			this.contents.addChild(txt3d);
			this.contents.addChild(sprite3d_fore);
			
			//this.stress3D = new Stress3D();
			//addChild(stress3D);
			
			//Things yet to do before project complete
			//cleared 15 Nov 08 -tbh- TODO Snap to max shear orientation
			//cleared 15 Nov 08 -tbh- TODO Snap to principal orientation
			//cleared 15 Nov 08 -tbh- TODO Create and place icons
			//cleared 15 Nov 08 -tbh- TODO Beautify sliders
			//cleared 17 Nov 08 -tbh- TODO Create intro animation
			//deleted 15 Nov 08 -tbh- TODO Display body output stress tensor
			//cleared 15 Nov 08 -tbh- TODO Display principal stresses
			//deleted 17 Nov 08 -tbh- TODO Show 3d mohrs circle graphic (arc segments intersection for shear)??
			//cleared 15 Nov 08 -tbh- TODO Change mohr circle dots to represent output, not input stresses
			//cleared 15 Nov 08 -tbh- TODO Change default orientation to orthotropic view
			//cleared 15 Nov 08 -tbh- TODO Add icon to top right corner
			//cleared 17 Nov 08 -tbh- TODO Add copyleft info
			//deleted 17 Nov 08 -tbh- TODO Add legend for axes
			//cleared 15 Nov 08 -tbh- TODO Show resultant vectors
			
			this.contents.addChildAt(background, 0);
			this.bgTimer = new Timer(Math.random()*500 + 1000, 0);
			this.bgTimer.addEventListener(TimerEvent.TIMER, Handler);
			this.bgTimer.start();
			for (var i:uint = 0; i < this.animArray.length; i++)
			{
				this.animArray[i] = new IntroAnimObject(Math.random() * 550 - 25, Math.random() * 40 + 110, Math.random() * 40 + 60,Math.random(), Math.random() * 500, 0);
			}
		
			//Rights Box
			{
				this.contents.addChild(rightsSprite);
				this.rightsSprite.graphics.clear();
				//rightsSprite.addChild(rights);
				rightsSprite.x = 800 - 50;
				rightsSprite.y = 525 - 20;
				rightsSprite.graphics.beginFill(0, 1);
				rightsSprite.graphics.drawRect(-50, -20, 100, 40);
				rightsSprite.graphics.endFill();
				var colorsArray:Array = new Array(2);
				colorsArray[0] = 0xffffff;
				colorsArray[1] = 0xbbbbbb;
				var alphasArray:Array = new Array(2);
				alphasArray[0] = 1;
				alphasArray[1] = 1;
				var ratiosArray:Array = new Array(2);
				ratiosArray[0] = 0;
				ratiosArray[1] = 70;
				
				rightsSprite.graphics.beginGradientFill(GradientType.RADIAL, colorsArray, alphasArray, ratiosArray);
				rightsSprite.graphics.drawRect( -48, -18, 96, 25);
				rightsSprite.graphics.endFill();
				
				var ccForm:TextFormat = new TextFormat();
				ccForm.font = "ArialBlack";
				ccForm.size = 10;
				ccForm.bold = true;
				
				var cc:TextField = new TextField();
				cc.defaultTextFormat = ccForm;
				cc.embedFonts = true;
				cc.text = "CC";
				cc.x = -10;
				cc.y = -15;
				cc.selectable = false;
				this.rightsSprite.addChild(cc);
				
				var someForm:TextFormat = new TextFormat();
				someForm.font = "Calibri";
				someForm.size = 12;
				someForm.bold = true;
				someForm.color = 0xffffff;
				someForm.letterSpacing = -1.3;
				
				var some:TextField = new TextField();
				some.defaultTextFormat = someForm;
				some.embedFonts = true;
				some.text = "SOME RIGHTS RESERVED";
				some.x = -50;
				some.y = 4;
				some.selectable = false;
				textField
				
				this.rightsSprite.addChild(some);
				
				this.rightsSprite.graphics.lineStyle(1.5, 0, 1);
				this.rightsSprite.graphics.beginFill(0xffffff, 1);
				this.rightsSprite.graphics.drawCircle(0, -5.5, 9);
				this.rightsSprite.graphics.endFill();
				
				
				rightsSprite.addEventListener(MouseEvent.CLICK, RightsClickedHandler);
				rightsSprite.addEventListener(MouseEvent.MOUSE_OVER, RightsOverHandler);
				
				this.rightsSprite.addChild(rightsSpriteOver);
				var thick:Number = 1;
				rightsSpriteOver.graphics.lineStyle(thick, 0xffff00, 1);
				//rightsSpriteOver.graphics.beginFill(0xffff00, 0.2);
				rightsSpriteOver.graphics.drawRect( -50, -20, 100 - thick, 40 - thick);
				//rightsSpriteOver.graphics.endFill();
				rightsSpriteOver.visible = false;
			}
			
			
		}
		private function RightsOverHandler(e:MouseEvent):void
		{
			this.rightsSpriteOver.visible = true;
			rightsSprite.addEventListener(MouseEvent.MOUSE_OUT, RightsOutHandler);
		}
		private function RightsOutHandler(e:MouseEvent):void
		{
			this.rightsSpriteOver.visible = false;
			rightsSprite.removeEventListener(MouseEvent.MOUSE_OUT, RightsOutHandler);
		}
		private function TearDown():void
		{
			this.removeChild(contents);
			for (var i:int = 0; i < this.animArray.length; i++)
			{
				this.animArray[i] = new Sprite();
			}
			this.intro = new Sprite();
			this.contents = new Sprite();
			this.sprite2d = new Sprite();
			this.sprite2d_fore = new Sprite();
			this.sprite2d_over = new Sprite();
			this.sprite3d = new Sprite();
			this.background = new Sprite();

		}
		private function RightsClickedHandler(e:MouseEvent):void
		{
			var url:URLRequest = new URLRequest("http://creativecommons.org/licenses/by-nc-sa/3.0/us/");
			navigateToURL(url);
			//trace("u");
		}
		private function AuthorClickHandler(e:MouseEvent):void
		{
			var url:URLRequest = new URLRequest("mailto:tbhartman@vt.edu");
			navigateToURL(url);
			this.AuthorOutHandler(e);
		}
		private function AuthorOverHandler(e:MouseEvent):void
		{
			clickAuthorSprite.addEventListener(MouseEvent.MOUSE_OUT, AuthorOutHandler);
			txtAuthor.textColor = 0xff0000;
		}
		private function AuthorOutHandler(e:MouseEvent):void
		{
			clickAuthorSprite.removeEventListener(MouseEvent.MOUSE_OUT, AuthorOutHandler);
			txtAuthor.textColor = 0x000000;
		}
		private function Handler(e:TimerEvent):void
		{
			//trace("hey");
			var tmpInt:uint = Math.floor(Math.random() * 10);
			if (this.background.contains(animArray[tmpInt]) && this.animArray[tmpInt].x > 800)
			{
				this.background.removeChild(animArray[tmpInt]);
				this.animArray[tmpInt] = new IntroAnimObject(Math.random() * 550 - 25, Math.random() * 190 + 10, Math.random() * 140 + 10,(Math.random()*80+10)/100, Math.random() * Math.random() *100, tmpInt);
			}
			else if (!this.background.contains(animArray[tmpInt]))
			{
				this.animArray[tmpInt] = new IntroAnimObject(Math.random() * 550 - 25, Math.random() * 190 + 10, Math.random() * 140 + 10,(Math.random()*80+10)/100, Math.random() * Math.random() * 100, tmpInt);
				this.background.addChildAt(animArray[tmpInt],0);
			}
		}
		private function MouseOver2dHandler(e:MouseEvent):void
		{
			if (!this.slideInTimer3d.running && !this.slideInTimer2d.running)
			{
				this.sprite2d_over.visible = true;
				this.sprite2d.visible = false;
				this.sprite2d_fore.addEventListener(MouseEvent.MOUSE_OUT, MouseOut2dHandler);
			}
		}
		private function MouseOut2dHandler(e:MouseEvent):void
		{
			this.sprite2d.visible = true;
			this.sprite2d_over.visible = false;
			this.sprite2d_fore.removeEventListener(MouseEvent.MOUSE_OUT, MouseOut2dHandler);
		}
		private function MouseDown2dHanlder(e:MouseEvent):void
		{
			if (!this.slideInTimer3d.running && !this.slideInTimer2d.running)
			{
				this.MouseOut2dHandler(e);
				this.sprite2d_over.removeEventListener(MouseEvent.MOUSE_OUT, MouseOut2dHandler);
				this.stress2D.x = -800;
				this.addChildAt(stress2D,1);
				this.stress2D.ExitButton.addEventListener(MouseEvent.MOUSE_DOWN, Exit2dHandler);
				this.slideInTimer2d = new Timer(1, 0);
				this.slideInTimer2d.addEventListener(TimerEvent.TIMER, TimerEvent2dHandler);
				this.slideInTimer2d.start();
			}
		}
		private function Exit2dHandler(e:MouseEvent):void
		{
			this.slideInTimer2d = new Timer(1, 0);
			this.Build();
			this.slideInTimer2d.addEventListener(TimerEvent.TIMER, TimerEvent2dOutHandler);
			this.stress2D.ExitButton.removeEventListener(MouseEvent.MOUSE_DOWN, Exit2dHandler);
			this.slideInTimer2d.start();
		}
		private function TimerEvent2dOutHandler(e:TimerEvent):void
		{
			this.stress2D.x -= 20;
			if (this.stress2D.x <= -800)
			{
				this.removeChild(stress2D);
				this.slideInTimer2d.removeEventListener(TimerEvent.TIMER, TimerEvent2dOutHandler);
				this.slideInTimer2d.stop();
			}
		}
		private function TimerEvent2dHandler(e:TimerEvent):void
		{
			this.stress2D.x += 20;
			if (this.stress2D.x >= 0)
			{
				this.stress2D.x = 0;
				this.slideInTimer2d.removeEventListener(TimerEvent.TIMER, TimerEvent2dHandler);
				this.slideInTimer2d.stop();
				this.TearDown();
			}
		}
		private function MouseOver3dHandler(e:MouseEvent):void
		{
			if (!this.slideInTimer3d.running && !this.slideInTimer2d.running)
			{
				this.sprite3d_over.visible = true;
				this.sprite3d.visible = false;
				this.sprite3d_fore.addEventListener(MouseEvent.MOUSE_OUT, MouseOut3dHandler);
			}
		}
		private function MouseOut3dHandler(e:MouseEvent):void
		{
			this.sprite3d.visible = true;
			this.sprite3d_over.visible = false;
			this.sprite3d_fore.removeEventListener(MouseEvent.MOUSE_OUT, MouseOut3dHandler);
		}
		private function MouseDown3dHandler(e:MouseEvent):void
		{
			if (!this.slideInTimer3d.running && !this.slideInTimer2d.running)
			{
				this.MouseOut3dHandler(e);
				this.stress3D.x = 800;
				this.addChildAt(stress3D,1);
				this.stress3D.ExitButton.addEventListener(MouseEvent.MOUSE_DOWN, Exit3dHandler);
				this.slideInTimer3d = new Timer(1, 0);
				this.slideInTimer3d.addEventListener(TimerEvent.TIMER, TimerEvent3dHandler);
				this.slideInTimer3d.start();
			}
		}
		private function TimerEvent3dHandler(e:TimerEvent):void
		{
			this.stress3D.x -= 20;
			if (this.stress3D.x <= 0)
			{
				this.stress3D.x = 0;
				this.slideInTimer3d.removeEventListener(TimerEvent.TIMER, TimerEvent3dHandler);
				this.slideInTimer3d.stop();
				this.TearDown();
			}
		}
		
		private function Exit3dHandler(e:MouseEvent):void
		{
			this.slideInTimer3d = new Timer(1, 0);
			this.Build();
			this.slideInTimer3d.addEventListener(TimerEvent.TIMER, TimerEvent3dOutHandler);
			this.stress3D.ExitButton.removeEventListener(MouseEvent.MOUSE_DOWN, Exit3dHandler);
			this.slideInTimer3d.start();
		}
		private function TimerEvent3dOutHandler(e:TimerEvent):void
		{
			this.stress3D.x += 20;
			if (this.stress3D.x >= 800)
			{
				this.removeChild(stress3D);
				this.slideInTimer3d.removeEventListener(TimerEvent.TIMER, TimerEvent3dOutHandler);
				this.slideInTimer3d.stop();
			}
		}
	}
	
}
