﻿package MohreCircles.ddd
{
	import flash.geom.Point;
	
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class Quadrilateral extends Surface
	{
		private var points:Array = new Array(3);
		public function get Point(): Array
		{
			return this.points;
		}
		public function Quadrilateral(point1:SpaceVector,point2:SpaceVector,point3:SpaceVector,point4:SpaceVector):void
		{
			this.points[0] = point1;
			this.points[1] = point2;
			this.points[2] = point3;
			this.points[3] = point4;
			
		}
		
		public function get outwardNormal():SpaceVector
		{
			var r:SpaceVector = VMath.Add(this.points[1], VMath.ScalarProduct( -1, this.points[0]));
			var f:SpaceVector = VMath.Add(this.points[2], VMath.ScalarProduct( -1, this.points[1]));
			var result:SpaceVector = VMath.ScalarQuotient(VMath.Cross(r,f),VMath.Magnitude(VMath.Cross(r, f)))
			return new SpaceVector(result.x,result.y,result.z,this.points[0].coordinateSystem);
		}
		//public function get centroid():SpaceVector
		//{
			//
		//}
		
	}
	
}
