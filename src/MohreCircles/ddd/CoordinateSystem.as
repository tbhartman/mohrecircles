﻿package MohreCircles.ddd
{
	import MohreCircles.math.Tensor;
	
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class CoordinateSystem
	{
		private var _xAxis:SpaceVector;
		public function get xAxis():SpaceVector
		{
			return this._xAxis;
		}
		private var _yAxis:SpaceVector;
		public function get yAxis():SpaceVector
		{
			return this._yAxis;
		}
		public function get zAxis():SpaceVector
		{
			return VMath.Cross(this.xAxis, this.yAxis);
		}
		
		private var _transformTo:Tensor = new Tensor();
		public function get transformTo():Tensor
		{
			return this._transformTo;
		}
		private var _transformFrom:Tensor = new Tensor();
		
		public function CoordinateSystem(xAxis:SpaceVector, yAxis:SpaceVector)
		{
			SetAxes(xAxis, yAxis);
		}
		public function SetAxes(xAxis:SpaceVector, yAxis:SpaceVector):void
		{
			this._xAxis = VMath.Unity(xAxis.TransformToBase());
			this._yAxis = VMath.Unity(yAxis.TransformToBase());
			this.FindTransform();
		}
		public function TransformToBase(v:SpaceVector):SpaceVector
		{
			return TensorMultiply(this._transformTo, v);
		}
		public function TransformFromBase(v:SpaceVector):SpaceVector
		{
			return TensorMultiply(this._transformFrom, v);
		}
		public function Rotate(v:SpaceVector, rad:Number):void
		{
			//I believe there is a error in this.  It might rotate CS's in the opposite direction.
			//However, it works so I'll leave it.
			/**
			 * using a rotation formula:
			 * http://mathworld.wolfram.com/RotationFormula.html
			 * r' = O-N + N-V + V-Q
			 */
			var n:SpaceVector = VMath.Unity(v.TransformToBase());
			var O_Nx:SpaceVector = VMath.ScalarProduct(VMath.Dot(n, this.xAxis), n);
			var N_Vx:SpaceVector = VMath.ScalarProduct(Math.cos(rad), VMath.Add(this.xAxis, VMath.ScalarProduct( -1, O_Nx)));
			var V_Qx:SpaceVector = VMath.ScalarProduct(Math.sin(rad), VMath.Cross(this.xAxis, n));
			var O_Ny:SpaceVector = VMath.ScalarProduct(VMath.Dot(n, this.yAxis), n);
			var N_Vy:SpaceVector = VMath.ScalarProduct(Math.cos(rad), VMath.Add(this.yAxis, VMath.ScalarProduct( -1, O_Ny)));
			var V_Qy:SpaceVector = VMath.ScalarProduct(Math.sin(rad), VMath.Cross(this.yAxis, n));
			
			//trace(this.yAxis.TransformToBase());
			//trace (n, rad);
			//trace(O_Ny, N_Vy, V_Qy);
			this._xAxis = VMath.Add(O_Nx, N_Vx, V_Qx);
			this._yAxis = VMath.Add(O_Ny, N_Vy, V_Qy);
			//trace(this.yAxis);
			this.FindTransform();
			
		}
		private function FindTransform():void
		{
			var xBase:SpaceVector = new SpaceVector(1, 0, 0);
			var yBase:SpaceVector = new SpaceVector(0, 1, 0);
			var zBase:SpaceVector = new SpaceVector(0, 0, 1);
			this._transformTo.setComponent(1, 1, Math.cos(VMath.Angle(xBase, this.xAxis)));
			this._transformTo.setComponent(1, 2, Math.cos(VMath.Angle(xBase, this.yAxis)));
			this._transformTo.setComponent(1, 3, Math.cos(VMath.Angle(xBase, this.zAxis)));
			this._transformTo.setComponent(2, 1, Math.cos(VMath.Angle(yBase, this.xAxis)));
			this._transformTo.setComponent(2, 2, Math.cos(VMath.Angle(yBase, this.yAxis)));
			this._transformTo.setComponent(2, 3, Math.cos(VMath.Angle(yBase, this.zAxis)));
			this._transformTo.setComponent(3, 1, Math.cos(VMath.Angle(zBase, this.xAxis)));
			this._transformTo.setComponent(3, 2, Math.cos(VMath.Angle(zBase, this.yAxis)));
			this._transformTo.setComponent(3, 3, Math.cos(VMath.Angle(zBase, this.zAxis)));
			
			this._transformFrom.setComponent(1, 1, Math.cos(VMath.Angle(this.xAxis, xBase)));
			this._transformFrom.setComponent(1, 2, Math.cos(VMath.Angle(this.xAxis, yBase)));
			this._transformFrom.setComponent(1, 3, Math.cos(VMath.Angle(this.xAxis, zBase)));
			this._transformFrom.setComponent(2, 1, Math.cos(VMath.Angle(this.yAxis, xBase)));
			this._transformFrom.setComponent(2, 2, Math.cos(VMath.Angle(this.yAxis, yBase)));
			this._transformFrom.setComponent(2, 3, Math.cos(VMath.Angle(this.yAxis, zBase)));
			this._transformFrom.setComponent(3, 1, Math.cos(VMath.Angle(this.zAxis, xBase)));
			this._transformFrom.setComponent(3, 2, Math.cos(VMath.Angle(this.zAxis, yBase)));
			this._transformFrom.setComponent(3, 3, Math.cos(VMath.Angle(this.zAxis, zBase)));
		}
		private function TensorMultiply(t:Tensor, v:SpaceVector):SpaceVector
		{
			var resultant:SpaceVector = new SpaceVector();
			var t11:Number = t.getComponent(1, 1);
			var t12:Number = t.getComponent(1, 2);
			var t13:Number = t.getComponent(1, 3);
			var t21:Number = t.getComponent(2, 1);
			var t22:Number = t.getComponent(2, 2);
			var t23:Number = t.getComponent(2, 3);
			var t31:Number = t.getComponent(3, 1);
			var t32:Number = t.getComponent(3, 2);
			var t33:Number = t.getComponent(3, 3);
			resultant.x = t11 * v.x + t12 * v.y + t13 * v.z;
			resultant.y = t21 * v.x + t22 * v.y + t23 * v.z;
			resultant.z = t31 * v.x + t32 * v.y + t33 * v.z;
			return resultant;
		}
	}
	
}
