﻿package MohreCircles.ddd
{
	
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class DirCos
	{
		private var x:Number;
		private var y:Number;
		private var z:Number;
		public function get X():Number
		{
			return this.x;
		}
		public function get Y():Number
		{
			return this.y;
		}
		public function get Z():Number
		{
			return this.z;
		}
		
		
		public function DirCos(v:SpaceVector)
		{
			this.x = (VMath.Dot(v.TransformToBase(), SpaceVector.X) / (VMath.Magnitude(v.TransformToBase()) * VMath.Magnitude(SpaceVector.X)));
			this.y = (VMath.Dot(v.TransformToBase(), SpaceVector.Y) / (VMath.Magnitude(v.TransformToBase()) * VMath.Magnitude(SpaceVector.Y)));
			this.z = (VMath.Dot(v.TransformToBase(), SpaceVector.Z) / (VMath.Magnitude(v.TransformToBase()) * VMath.Magnitude(SpaceVector.Z)));
			//trace("**-------------------**");
			//trace("DirCos for ", v);
			//trace(x, y, z);
			
		}
		
	}
	
}
