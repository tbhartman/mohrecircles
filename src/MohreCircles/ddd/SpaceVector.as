﻿package MohreCircles.ddd
{
	
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class SpaceVector
	{
		public static const X:SpaceVector = new SpaceVector(1, 0, 0);
		public static const Y:SpaceVector = new SpaceVector(0, 1, 0);
		public static const Z:SpaceVector = new SpaceVector(0, 0, 1);
		
		private var _x:Number = new Number();
		public function set x(setValue:Number):void
		{
			this._x = setValue;
		}
		public function get x():Number
		{
			return this._x;
		}
		private var _y:Number = new Number();
		public function set y(setValue:Number):void
		{
			this._y = setValue;
		}
		public function get y():Number
		{
			return this._y;
		}
		private var _z:Number = new Number();
		public function set z(setValue:Number):void
		{
			this._z = setValue;
		}
		public function get z():Number
		{
			return this._z;
		}

		//public function set r(setValue:Number):void
		//{
			//this._z = setValue;
		//}
		public function get r():Number
		{
			return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2));
		}
		//public function set phi(setValue:Number):void
		//{
			//this._z = setValue;
		//}
		public function get phi():Number
		{
			return Math.atan2(this.y, this.x);
		}
		//public function set theta(setValue:Number):void
		//{
			//this._z = setValue;
		//}
		public function get theta():Number
		{
			return Math.atan2(Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2)), this.z);
		}
		
		public function get length():Number
		{
			return this.r;
		}
		
		public const VIEWER_BASE:String = "viewer_base";
		public const COORDINATE_SYSTEM:String = "coordinate_system";
		
		private var _coordinateSystemType:String = "viewer_base";
		public function get coordinateSystemType():String
		{
			return this._coordinateSystemType;
		}
		private var _coordinateSystem:CoordinateSystem;
		public function get coordinateSystem():CoordinateSystem
		{
			return this._coordinateSystem;
		}
		public function set coordinateSystem(setValue:CoordinateSystem):void
		{
			if (setValue == null)
			{
				this._coordinateSystemType = this.VIEWER_BASE;
			}
			else
			{
				this._coordinateSystem =  setValue;
				this._coordinateSystemType = this.COORDINATE_SYSTEM;
			}
		}
		
		public function SpaceVector(x:Number = 0, y:Number = 0, z:Number = 0, cs:CoordinateSystem = null)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.coordinateSystem = cs;
		}
		public function TransformToBase():SpaceVector
		{
			if (this.coordinateSystemType == this.COORDINATE_SYSTEM)
			{
				return this.coordinateSystem.TransformToBase(this);
			}
			else
			{
				return this;
			}
		}
		public function TransformToCS(cs:CoordinateSystem):SpaceVector
		{
			var thisToBase:SpaceVector = this.TransformToBase();
			return cs.TransformFromBase(thisToBase);
		}
		public function Rotate(phi:Number, theta:Number):void
		{
			var x:Number = this.r * Math.sin(this.theta + theta) * Math.cos(this.phi + phi);
			var y:Number = this.r * Math.sin(this.theta + theta) * Math.sin(this.phi + phi);
			var z:Number = this.r * Math.cos(this.theta + theta);
			this.x = x;
			this.y = y;
			this.z = z;
		}
		public function toString(spherical:Boolean = false):String
		{
			if (spherical)
			{
				return ("{" + this.r + "," + this.phi + "," + this.theta + "}");
			}
			else
			{
				return ("{" + this.x + "," + this.y + "," + this.z + "}");
			}
		}
		
	}
	
}
