﻿package MohreCircles.ddd
{
	import MohreCircles.MathEx;
	
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class VMath
	{
		//sample
		private var spaceVector:SpaceVector = new SpaceVector();
		
		/*public function VMath()
		{
			
		}*/
		public static function Magnitude(v:SpaceVector):Number
		{
			return Math.sqrt((v.x * v.x) + (v.y * v.y) + (v.z * v.z));
		}
		public static function ScalarQuotient(v:SpaceVector, n:Number):SpaceVector
		{
			return new SpaceVector(v.x / n, v.y / n, v.z / n);
		}
		public static function Cross(a:SpaceVector, b:SpaceVector):SpaceVector
		{
			var c:SpaceVector = new SpaceVector();
			c.x = a.y * b.z - b.y * a.z;
			c.y = b.x * a.z - a.x * b.z;
			c.z = a.x * b.y - b.x * a.y;
			return c;
		}
		public static function Dot(a:SpaceVector, b:SpaceVector):Number
		{
			var c:Number = new Number();
			c = a.x * b.x + a.y * b.y + a.z * b.z;
			return c;
		}
		public static function Add(...params):SpaceVector
		{
			var resultant:SpaceVector = new SpaceVector();
			for (var i:int = 0; i < params.length; i++)
			{
				if (params[i] is SpaceVector)
				{
					resultant.x += params[i].x;
					resultant.y += params[i].y;
					resultant.z += params[i].z;
				}
				else
				{
					throw new Error("Not a SpaceVector");
				}
			}
			return resultant;
		}
		public static function Subtract(v1:SpaceVector, v2:SpaceVector):SpaceVector
		{
			return VMath.Add(v1, VMath.ScalarProduct( -1, v2));
		}
		public static function ScalarProduct(a:Number, b:SpaceVector):SpaceVector
		{
			var resultant:SpaceVector = new SpaceVector();
			resultant.x = a * b.x;
			resultant.y = a * b.y;
			resultant.z = a * b.z;
			return resultant;
		}
		public static function Unity(a:SpaceVector):SpaceVector
		{
			var resultant:SpaceVector = new SpaceVector();
			resultant.x = a.x / VMath.Magnitude(a);
			resultant.y = a.y / VMath.Magnitude(a);
			resultant.z = a.z / VMath.Magnitude(a);
			//trace(VMath.Magnitude(resultant));
			return resultant;
		}
		public static function Angle(a:SpaceVector, b:SpaceVector):Number
		{
			return Math.acos((VMath.Dot(a, b)) / (VMath.Magnitude(a) * VMath.Magnitude(b)));
		}
		public static function IsOrtho(a:SpaceVector, b:SpaceVector):Boolean
		{
			//Are these vectors orthogonal?  they are if the dot product is approx zero
			if (a.x.toString() != NaN.toString() && a.y.toString() != NaN.toString() && a.z.toString() != NaN.toString() &&
				b.x.toString() != NaN.toString() && b.y.toString() != NaN.toString() && b.z.toString() != NaN.toString() )
			{
				return MathEx.IsApproxEqual(VMath.Dot(a, b), 0, 6);
			}
			else
			{
				return false;
			}
		}
	}
	
}
