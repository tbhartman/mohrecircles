﻿package MohreCircles.ddd
{
	import flash.display.Bitmap;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.errors.InvalidSWFError;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.system.System;
	import MohreCircles.graphics.Coordinate;
	import MohreCircles.graphics.XYGrid;
	import MohreCircles.MathEx;
	import MohreCircles.PushButton;
	import MohreCircles.PushButtonEvent;
	import flash.text.*;
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class Viewer extends Sprite
	{
		[Embed(source='../../../lib/CALIBRI.ttf', fontName = "Calibri", unicodeRange="U+0030-U+0039,U+0041-U+005A,U+0061-U+007A,U+003A,U+002E,U+003D,U+0028,U+0029,U+002D,U+00AD", mimeType='application/x-font')]
		public static var Calibri:Class;
		Font.registerFont(Calibri);
		[Embed(source='../../../lib/CALIBRII.ttf', fontName = "CalibriI", unicodeRange="U+0030-U+0039,U+0041-U+005A,U+0061-U+007A,U+003A,U+002E,U+003D,U+0028,U+0029,U+002D,U+00AD", fontStyle = "Italic", mimeType='application/x-font')]
		public static var CalibriI:Class;
		Font.registerFont(CalibriI);
		[Embed(source='../../../lib/CALIBRIB.ttf', fontName = "CalibriB", unicodeRange="U+0030-U+0039,U+0041-U+005A,U+0061-U+007A,U+003A,U+002E,U+003D,U+0028,U+0029,U+002D,U+00AD", fontWeight = "Bold", mimeType='application/x-font')]
		public static var CalibriB:Class;
		Font.registerFont(CalibriB);
		[Embed(source='../../../lib/CALIBRIz.ttf', fontName = "CalibriBI", unicodeRange="U+0030-U+0039,U+0041-U+005A,U+0061-U+007A,U+003A,U+002E,U+003D,U+0028,U+0029,U+002D,U+00AD", fontWeight = "Bold", fontStyle = "Italic", mimeType='application/x-font')]
		public static var CalibriBI:Class;
		Font.registerFont(CalibriBI);
		[Embed(source='../../../lib/symbol.ttf', fontName = "Symbol", unicodeRange="U+0030-U+0039,U+0041-U+005A,U+0061-U+007A,U+003A,U+002E,U+003D,U+0028,U+0029,U+002D,U+00AD", mimeType='application/x-font')]
		public static var Symbol:Class;
		Font.registerFont(Symbol);
		[Embed(source='../../../lib/times.ttf', fontName = "TimesNewRoman", unicodeRange="U+0030-U+0039,U+0041-U+005A,U+0061-U+007A,U+003A,U+002E,U+003D,U+0028,U+0029,U+002D,U+00AD", mimeType='application/x-font')]
		public static var TimesNewRoman:Class;
		Font.registerFont(TimesNewRoman);
		[Embed(source='../../../lib/timesbd.ttf', fontName = "TimesNewRomanB", unicodeRange="U+0030-U+0039,U+0041-U+005A,U+0061-U+007A,U+003A,U+002E,U+003D,U+0028,U+0029,U+002D,U+00AD", fontWeight = "Bold", mimeType='application/x-font')]
		public static var TimesNewRomanB:Class;
		Font.registerFont(TimesNewRomanB);
		[Embed(source='../../../lib/timesbi.ttf', fontName = "TimesNewRomanBI", unicodeRange="U+0030-U+0039,U+0041-U+005A,U+0061-U+007A,U+003A,U+002E,U+003D,U+0028,U+0029,U+002D,U+00AD", fontWeight = "Bold", fontStyle = "Italic", mimeType='application/x-font')]
		public static var TimesNewRomanBI:Class;
		Font.registerFont(TimesNewRomanBI);
		
		private var moveLastX:Number = 0;
		private var moveLastY:Number = 0;
		
		private var displayType2D:Boolean;
		public function set DisplayType2D(b:Boolean):void
		{
			this.displayType2D = b;
		}
		public function get DisplayType2D():Boolean
		{
			return this.displayType2D;
		}
		//Images for buttons
		/*
		[Embed(source = '../../../lib/RotBodyDown.gif')]
		private var rotBodyDown:Class;
		private var rotBodyDownImage:Bitmap = new rotBodyDown();
		[Embed(source='../../../lib/RotBodyUp.gif')]
		private var rotBodyUp:Class;
		private var rotBodyUpImage:Bitmap = new rotBodyUp();
		
		[Embed(source = '../../../lib/RotRefDown.gif')]
		private var rotRefDown:Class;
		private var rotRefDownImage:Bitmap = new rotRefDown();
		[Embed(source='../../../lib/RotRefUp.gif')]
		private var rotRefUp:Class;
		private var rotRefUpImage:Bitmap = new rotRefUp();
		
		[Embed(source = '../../../lib/RotEyeDown.gif')]
		private var rotEyeDown:Class;
		private var rotEyeDownImage:Bitmap = new rotEyeDown();
		[Embed(source='../../../lib/RotEyeUp.gif')]
		private var rotEyeUp:Class;
		private var rotEyeUpImage:Bitmap = new rotEyeUp();
		
		[Embed(source = '../../../lib/ShowRefAxesDown.gif')]
		private var showRefAxesDown:Class;
		private var showRefAxesDownImage:Bitmap = new showRefAxesDown();
		[Embed(source='../../../lib/showRefAxesUp.gif')]
		private var showRefAxesUp:Class;
		private var showRefAxesUpImage:Bitmap = new showRefAxesUp();
		
		[Embed(source = '../../../lib/ShowBodyAxesDown.gif')]
		private var showBodyAxesDown:Class;
		private var showBodyAxesDownImage:Bitmap = new showBodyAxesDown();
		[Embed(source='../../../lib/showBodyAxesUp.gif')]
		private var showBodyAxesUp:Class;
		private var showBodyAxesUpImage:Bitmap = new showBodyAxesUp();
		
		[Embed(source = '../../../lib/ShowForceDown.gif')]
		private var showForcesDown:Class;
		private var showForcesDownImage:Bitmap = new showForcesDown();
		[Embed(source='../../../lib/ShowForceUp.gif')]
		private var showForcesUp:Class;
		private var showForcesUpImage:Bitmap = new showForcesUp();
		
		[Embed(source='../../../lib/ShowResultantDown.gif')]
		private var showResultantsDown:Class;
		private var showResultantsDownImage:Bitmap = new showResultantsDown();
		[Embed(source='../../../lib/ShowResultantUp.gif')]
		private var showResultantsUp:Class;
		private var showResultantsUpImage:Bitmap = new showResultantsUp();
		
		[Embed(source = '../../../lib/ShowTransDown.gif')]
		private var showTransDown:Class;
		private var showTransDownImage:Bitmap = new showTransDown();
		[Embed(source='../../../lib/ShowTransUp.gif')]
		private var showTransUp:Class;
		private var showTransUpImage:Bitmap = new showTransUp();
		
		[Embed(source = '../../../lib/SnapPrinDown.gif')]
		private var snapPrinDown:Class;
		private var snapPrinDownImage:Bitmap = new snapPrinDown();
		[Embed(source='../../../lib/SnapPrinUp.gif')]
		private var snapPrinUp:Class;
		private var snapPrinUpImage:Bitmap = new snapPrinUp();
		[Embed(source = '../../../lib/SnapMaxShearDown.gif')]
		private var snapMaxShearDown:Class;
		private var snapMaxShearDownImage:Bitmap = new snapMaxShearDown();
		[Embed(source='../../../lib/SnapMaxShearUp.gif')]
		private var snapMaxShearUp:Class;
		private var snapMaxShearUpImage:Bitmap = new snapMaxShearUp();
		
		
		[Embed(source = '../../../lib/SnapOrthoDown.gif')]
		private var snapOrthoDown:Class;
		private var snapOrthoDownImage:Bitmap = new snapOrthoDown();
		[Embed(source='../../../lib/SnapOrthoUp.gif')]
		private var snapOrthoUp:Class;
		private var snapOrthoUpImage:Bitmap = new snapOrthoUp();
		
		[Embed(source = '../../../lib/SnapRefDown.gif')]
		private var snapRefDown:Class;
		private var snapRefDownImage:Bitmap = new snapRefDown();
		[Embed(source='../../../lib/SnapRefUp.gif')]
		private var snapRefUp:Class;
		private var snapRefUpImage:Bitmap = new snapRefUp();
		
		[Embed(source = '../../../lib/SnapXRefAxisDown.gif')]
		private var snapXAxisDown:Class;
		private var snapXAxisDownImage:Bitmap = new snapXAxisDown();
		[Embed(source='../../../lib/SnapXRefAxisUp.gif')]
		private var snapXAxisUp:Class;
		private var snapXAxisUpImage:Bitmap = new snapXAxisUp();
		[Embed(source = '../../../lib/SnapYRefAxisDown.gif')]
		private var snapYAxisDown:Class;
		private var snapYAxisDownImage:Bitmap = new snapYAxisDown();
		[Embed(source='../../../lib/SnapYRefAxisUp.gif')]
		private var snapYAxisUp:Class;
		private var snapYAxisUpImage:Bitmap = new snapYAxisUp();
		[Embed(source = '../../../lib/SnapZRefAxisDown.gif')]
		private var snapZAxisDown:Class;
		private var snapZAxisDownImage:Bitmap = new snapZAxisDown();
		[Embed(source='../../../lib/SnapZRefAxisUp.gif')]
		private var snapZAxisUp:Class;
		private var snapZAxisUpImage:Bitmap = new snapZAxisUp();
		[Embed(source = '../../../lib/SnapxAxisDown.gif')]
		private var snapxAxisDown:Class;
		private var snapxAxisDownImage:Bitmap = new snapxAxisDown();
		[Embed(source='../../../lib/SnapxAxisUp.gif')]
		private var snapxAxisUp:Class;
		private var snapxAxisUpImage:Bitmap = new snapxAxisUp();
		[Embed(source = '../../../lib/SnapyAxisDown.gif')]
		private var snapyAxisDown:Class;
		private var snapyAxisDownImage:Bitmap = new snapyAxisDown();
		[Embed(source='../../../lib/SnapyAxisUp.gif')]
		private var snapyAxisUp:Class;
		private var snapyAxisUpImage:Bitmap = new snapyAxisUp();
		[Embed(source = '../../../lib/SnapzAxisDown.gif')]
		private var snapzAxisDown:Class;
		private var snapzAxisDownImage:Bitmap = new snapzAxisDown();
		[Embed(source='../../../lib/SnapzAxisUp.gif')]
		private var snapzAxisUp:Class;
		private var snapzAxisUpImage:Bitmap = new snapzAxisUp();*/
		
		
		private var _aHeight:int = new int();
		private var _aWidth:int = new int();
		
		private var body:Array = new Array(0);
		private var particle:Array = new Array(0);
		private var quad:Array = new Array(0);
		private var forceArrow:Array = new Array(0);
		private var resultantArrow:Array = new Array(0);
		private var EigenArrow:Array = new Array(0);
		private var refArrow:Array = new Array(0);
		private var bodyArrow:Array = new Array(0);
		
		public var EigenVectors:Array = new Array(3);
		
		public var show3dGenStatus:Boolean = false;
		
		private var butShowBaseAxes:PushButton = new PushButton();
		private var butShowBodyAxes:PushButton = new PushButton();
		private var butShowEigenVectors:PushButton = new PushButton();
		private var butShowForces:PushButton = new PushButton();
		private var butShowResultants:PushButton = new PushButton();
		private var butBodyIsTransparent:PushButton = new PushButton();
		
		public var butGoToPrincipal:PushButton = new PushButton();
		public var butGoToMaxShear:PushButton = new PushButton();
		public var butGoToRef:PushButton = new PushButton();
		private var butGoToXAxis:PushButton = new PushButton();
		private var butGoToYAxis:PushButton = new PushButton();
		private var butGoToZAxis:PushButton = new PushButton();
		private var butGoToxAxis:PushButton = new PushButton();
		private var butGoToyAxis:PushButton = new PushButton();
		private var butGoTozAxis:PushButton = new PushButton();
		private var butGoToOrtho:PushButton = new PushButton();
		
		public var butRotNormal:PushButton;
		public var butRotBody:PushButton;
		public var butRotBody2d:PushButton;
		
		
		//public var rotateType:String = "rotate_view";
		public static const ROTATE_VIEW:String = "rotate_view";
		public static const ROTATE_BODY_3D:String = "rotate_body_3d";
		public static const ROTATE_BODY_2D:String = "rotate_body_2d";

		public function get ShowBaseAxes():Boolean
		{
			return this.butShowBaseAxes.Status;
		}
		public function set ShowBaseAxes(b:Boolean):void
		{
			this.butShowBaseAxes.Status = b;
		}
		public function get ShowBodyAxes():Boolean
		{
			return this.butShowBodyAxes.Status;
		}
		public function set ShowBodyAxes(b:Boolean):void
		{
			this.butShowBodyAxes.Status = b;
		}
		public function get ShowEigenVectors():Boolean
		{
			return this.butShowEigenVectors.Status;
		}
		public function set ShowEigenVectors(b:Boolean):void
		{
			this.butShowEigenVectors.Status = b;
		}
		public function get ShowForces():Boolean
		{
			return this.butShowForces.Status;
		}
		public function set ShowForces(b:Boolean):void
		{
			this.butShowForces.Status = b;
		}
		public function get ShowResultants():Boolean
		{
			return this.butShowResultants.Status;
		}
		public function set ShowResultants(b:Boolean):void
		{
			this.butShowResultants.Status = b;
		}
		public function get BodyIsTransparent():Boolean
		{
			return this.butBodyIsTransparent.Status;
		}
		public function set BodyIsTransparent(b:Boolean):void
		{
			this.butBodyIsTransparent.Status = b;
		}
		
		private var _eyePosition:SpaceVector;
		public function set eyePosition(v:SpaceVector):void
		{
			this._eyePosition = v;
			this.Redraw();
		}
		public function get eyePosition():SpaceVector
		{
			return this._eyePosition;
		}
		private var _eyePositionCS:CoordinateSystem;
		private var _eyeView:SpaceVector;
		public function set eyeView(v:SpaceVector):void
		{
			var norm:Number = VMath.Magnitude(v);
			this._eyeView = VMath.ScalarProduct(1 / norm, v);
		}
		public function get eyeView():SpaceVector
		{
			return this._eyeView;
		}
		private var _viewX:SpaceVector;
		public function get viewX():SpaceVector
		{
			return this._viewX;
		}
		public function set viewX(setValue:SpaceVector):void
		{
			this._viewX = setValue;
		}
		//ivate var viewY:SpaceVector;
		public function get viewY():SpaceVector
		{
			var result:SpaceVector = new SpaceVector(0, 0, 0, _eyePositionCS);
			var v:SpaceVector = VMath.Cross(this.viewX, this.eyeView);
			result.x = v.x;
			result.y = v.y;
			result.z = v.z;
			return result;
		}
		
		private var grid:XYGrid = new XYGrid();
		private var mouseDownRect:Sprite = new Sprite();
		
		private var mouseDownStart:Coordinate = new Coordinate();
		private var mouseDownShiftKey:Boolean = true;
		
		private var plotArea:Sprite = new Sprite();
		private var plotAreaChildren:Array = new Array(0);
		private var subPlotArea:Sprite = new Sprite();
		private var subPlotAreaOver:Sprite = new Sprite();
		private var text2dAngle:TextField = new TextField();
		//Samples
		private var spaceVector:SpaceVector = new SpaceVector();
		private var textFormat:TextFormat;
		
		//Overlay sprites
		private var ShowBaseAxesOverlay:Sprite = new Sprite();
		private var ShowBodyAxesOverlay:Sprite = new Sprite();
		private var ShowEigenVectorsOverlay:Sprite = new Sprite();
		private var ShowForcesOverlay:Sprite = new Sprite();
		private var ShowResultantsOverlay:Sprite = new Sprite();
		private var ShowTransOverlay:Sprite = new Sprite();
		private var GoToRefOverlay:Sprite = new Sprite();
		private var GoToMaxShearOverlay:Sprite = new Sprite();
		private var GoToPrinOverlay:Sprite = new Sprite();
		private var GoToXOverlay:Sprite = new Sprite();
		private var GoToYOverlay:Sprite = new Sprite();
		private var GoToZOverlay:Sprite = new Sprite();
		private var GoToxOverlay:Sprite = new Sprite();
		private var GoToyOverlay:Sprite = new Sprite();
		private var GoTozOverlay:Sprite = new Sprite();
		private var GoToOrthoOverlay:Sprite = new Sprite();
		private var RotBaseOverlay:Sprite = new Sprite();
		private var RotBodyOverlay:Sprite = new Sprite();
		private var RotBody2dOverlay:Sprite = new Sprite();
		
		public function Viewer(x:int,y:int,width:int,height:int,type2d:Boolean = false):void
		{
			this.DisplayType2D = type2d;
			
			this._eyePositionCS = new CoordinateSystem(new SpaceVector(-1/Math.sqrt(2), 1/Math.sqrt(2), 0), VMath.Unity(new SpaceVector(-1, -1, Math.sqrt(4))));
			//This eyeposition vector and view vector cant really be messed with...maybe its a problem with the view rotation function
			this.eyePosition = new SpaceVector(0, 0, 1, this._eyePositionCS);
			this.eyeView = new SpaceVector(0, 0, -1, this._eyePositionCS);
			this.viewX = new SpaceVector(1, 0, 0, this._eyePositionCS);
			
			this.x = x;
			this.y = y;
			this.aHeight = height;
			this.aWidth = width;
			
			this.grid.gridOn = false;
			this.grid.axesOn = false;
			this.grid.labelsOn = false;
			this.grid.hashOn = false;
			var size:Number = 3.7;
			if (width > height)
			{
				this.grid.Rescale( -size * width/height, size * width/height, 1, -size, size, 1, false);
			}
			else
			{
				this.grid.Rescale( -size, size, 1, -size * width/height, size * width/height, 1, false);
			}
			
			this.subPlotArea.x = 15;
			this.subPlotArea.y = 47;
			this.addChild(subPlotArea);
			this.subPlotAreaOver.x = 15;
			this.subPlotAreaOver.y = 47;
			this.addChild(subPlotAreaOver);
				var form:TextFormat = new TextFormat();
				form.font = "CalibriB";
				form.size = 14;
				form.bold = true;
				
				text2dAngle.defaultTextFormat = form;
				text2dAngle.x = 147;
				text2dAngle.y = 195;
				text2dAngle.autoSize = TextFieldAutoSize.CENTER;
				text2dAngle.selectable = false;
				text2dAngle.embedFonts = true;
					
			this.addChild(text2dAngle);
			//this.text2dAngle.visible = false;
			this.plotArea.x = 15;
			this.plotArea.y = 47;
			this.addChild(plotArea);
			
			this.addChild(mouseDownRect);
			this.mouseDownRect.graphics.lineStyle(1, 0, 0);
			this.mouseDownRect.graphics.beginFill(0, 0.);
			this.mouseDownRect.graphics.drawRect(0, 0, this.aWidth, this.aHeight);
			this.mouseDownRect.addEventListener(MouseEvent.MOUSE_DOWN, MouseDownHandler);
			
			//Button overlay sprites
			{
				var txtFormSmall:TextFormat = new TextFormat();
				txtFormSmall.font = "CalibriBI";
				txtFormSmall.size = "12";
				txtFormSmall.italic = true;
				txtFormSmall.bold = true;
				var txtFormVerySmall:TextFormat = new TextFormat();
				txtFormVerySmall.font = "CalibriI";
				txtFormVerySmall.size = "8";
				txtFormVerySmall.italic = true;
				
				//Rot Base
				{
					var txtR1:TextField = new TextField();
					RotBaseOverlay.addChild(txtR1);
					txtR1.defaultTextFormat = txtFormSmall;
					txtR1.text = "N";
					txtR1.embedFonts = true;
					txtR1.autoSize = TextFieldAutoSize.LEFT;
					txtR1.selectable = false;
					txtR1.x = 0;
					txtR1.y = -2;
					
					RotBaseOverlay.graphics.lineStyle(1.5, 0x000000, 1);
					RotBaseOverlay.graphics.drawCircle(17, 17, 7);
					RotBaseOverlay.graphics.moveTo( 7, 18);
					RotBaseOverlay.graphics.lineTo(10, 15);
					RotBaseOverlay.graphics.lineTo(13, 18);
				}
				//Rot Body2d
				{
					var txtR3:TextField = new TextField();
					RotBody2dOverlay.addChild(txtR3);
					txtR3.defaultTextFormat = txtFormSmall;
					txtR3.text = "b";
					txtR3.embedFonts = true;
					txtR3.autoSize = TextFieldAutoSize.LEFT;
					txtR3.selectable = false;
					txtR3.x = 0;
					txtR3.y = -2;
					
					RotBody2dOverlay.graphics.lineStyle(1.5, 0x000000, 1);
					RotBody2dOverlay.graphics.drawCircle(17, 17, 7);
					RotBody2dOverlay.graphics.moveTo( 7, 18);
					RotBody2dOverlay.graphics.lineTo(10, 15);
					RotBody2dOverlay.graphics.lineTo(13, 18);
					RotBody2dOverlay.graphics.lineStyle(0, 0x000000, 0.7);
					RotBody2dOverlay.graphics.beginFill(0, 1);
					RotBody2dOverlay.graphics.drawCircle(17, 17, 1.5);
					RotBody2dOverlay.graphics.endFill();
				}
				//Rot Body
				{
					var txtR2:TextField = new TextField();
					RotBodyOverlay.addChild(txtR2);
					txtR2.defaultTextFormat = txtFormSmall;
					txtR2.text = "B";
					txtR2.embedFonts = true;
					txtR2.autoSize = TextFieldAutoSize.LEFT;
					txtR2.selectable = false;
					txtR2.x = 0;
					txtR2.y = -2;
					
					RotBodyOverlay.graphics.lineStyle(1.5, 0x000000, 1);
					RotBodyOverlay.graphics.drawCircle(17, 17, 7);
					RotBodyOverlay.graphics.moveTo( 7, 18);
					RotBodyOverlay.graphics.lineTo(10, 15);
					RotBodyOverlay.graphics.lineTo(13, 18);
				}
				//Show Base Axes
				{
					var txt1:TextField = new TextField();
					ShowBaseAxesOverlay.addChild(txt1);
					txt1.defaultTextFormat = txtFormSmall;
					txt1.text = "N";
					txt1.embedFonts = true;
					txt1.autoSize = TextFieldAutoSize.LEFT;
					txt1.selectable = false;
					txt1.x = 0;
					txt1.y = -2;
					
					ShowBaseAxesOverlay.graphics.lineStyle(3, 0x000099, 1);
					ShowBaseAxesOverlay.graphics.moveTo(15, 18);
					ShowBaseAxesOverlay.graphics.lineTo(15, 5);
					ShowBaseAxesOverlay.graphics.lineStyle(3, 0x009900, 1);
					ShowBaseAxesOverlay.graphics.moveTo(15, 18);
					ShowBaseAxesOverlay.graphics.lineTo(25, 24);
					ShowBaseAxesOverlay.graphics.lineStyle(3, 0x990000, 1);
					ShowBaseAxesOverlay.graphics.moveTo(15, 18);
					ShowBaseAxesOverlay.graphics.lineTo(5, 24);
				}
				//Show Body Axes
				{
					var txt2:TextField = new TextField();
					ShowBodyAxesOverlay.addChild(txt2);
					txt2.defaultTextFormat = txtFormSmall;
					txt2.text = "B";
					txt2.embedFonts = true;
					txt2.autoSize = TextFieldAutoSize.LEFT;
					txt2.selectable = false;
					txt2.x = 0;
					txt2.y = -2;
					
					ShowBodyAxesOverlay.graphics.lineStyle(3, 0x000099, 1);
					ShowBodyAxesOverlay.graphics.moveTo(15, 15);
					ShowBodyAxesOverlay.graphics.lineTo(20, 5);
					ShowBodyAxesOverlay.graphics.lineStyle(3, 0x009900, 1);
					ShowBodyAxesOverlay.graphics.moveTo(15, 15);
					ShowBodyAxesOverlay.graphics.lineTo(22, 24);
					ShowBodyAxesOverlay.graphics.lineStyle(3, 0x990000, 1);
					ShowBodyAxesOverlay.graphics.moveTo(15, 15);
					ShowBodyAxesOverlay.graphics.lineTo(5, 18);
				}
				//Show EigenVectors
				{
					var txtEV:TextField = new TextField();
					ShowEigenVectorsOverlay.addChild(txtEV);
					txtEV.defaultTextFormat = txtFormSmall;
					txtEV.text = "n";
					txtEV.embedFonts = true;
					txtEV.autoSize = TextFieldAutoSize.LEFT;
					txtEV.selectable = false;
					txtEV.x = 0;
					txtEV.y = -2;
					var txtEVsup:TextField = new TextField();
					ShowEigenVectorsOverlay.addChild(txtEVsup);
					txtEVsup.defaultTextFormat = txtFormVerySmall;
					txtEVsup.text = "(i)";
					txtEVsup.embedFonts = true;
					txtEVsup.autoSize = TextFieldAutoSize.LEFT;
					txtEVsup.selectable = false;
					txtEVsup.x = 6;
					txtEVsup.y = -2;
					
					ShowEigenVectorsOverlay.graphics.lineStyle(3, 0x000099, 1);
					ShowEigenVectorsOverlay.graphics.moveTo(15, 15);
					ShowEigenVectorsOverlay.graphics.lineTo(20, 5);
					ShowEigenVectorsOverlay.graphics.lineStyle(3, 0x009900, 1);
					ShowEigenVectorsOverlay.graphics.moveTo(15, 15);
					ShowEigenVectorsOverlay.graphics.lineTo(22, 24);
					ShowEigenVectorsOverlay.graphics.lineStyle(3, 0x990000, 1);
					ShowEigenVectorsOverlay.graphics.moveTo(15, 15);
					ShowEigenVectorsOverlay.graphics.lineTo(5, 18);
				}
				//Show Forces
				{
					var txt3:TextField = new TextField();
					ShowForcesOverlay.addChild(txt3);
					txt3.defaultTextFormat = txtFormSmall;
					txt3.text = "F";
					txt3.embedFonts = true;
					txt3.autoSize = TextFieldAutoSize.LEFT;
					txt3.selectable = false;
					txt3.x = 0;
					txt3.y = -2;
					
					ShowForcesOverlay.graphics.lineStyle(3, 0, 1);
					ShowForcesOverlay.graphics.moveTo(10, 25);
					ShowForcesOverlay.graphics.lineTo(20, 5);
					ShowForcesOverlay.graphics.lineTo(14, 8);
					ShowForcesOverlay.graphics.moveTo(20, 5);
					ShowForcesOverlay.graphics.lineTo(21.5, 11.5);
				}
				//Show Resultants
				{
					var txt4:TextField = new TextField();
					ShowResultantsOverlay.addChild(txt4);
					txt4.defaultTextFormat = txtFormSmall;
					txt4.text = "R";
					txt4.embedFonts = true;
					txt4.autoSize = TextFieldAutoSize.LEFT;
					txt4.selectable = false;
					txt4.x = 0;
					txt4.y = -2;
					
					ShowResultantsOverlay.graphics.lineStyle(3, 0x770000, 1);
					ShowResultantsOverlay.graphics.moveTo(10, 25);
					ShowResultantsOverlay.graphics.lineTo(20, 5);
					ShowResultantsOverlay.graphics.lineTo(14, 8);
					ShowResultantsOverlay.graphics.moveTo(20, 5);
					ShowResultantsOverlay.graphics.lineTo(21.5, 11.5);
				}
				//Show Transparent
				{
					ShowTransOverlay.graphics.lineStyle(2, 0x888888, 1);
					ShowTransOverlay.graphics.moveTo(12, 18);
					ShowTransOverlay.graphics.lineTo(12, 5);
					ShowTransOverlay.graphics.moveTo(12, 18);
					ShowTransOverlay.graphics.lineTo(25, 18);
					ShowTransOverlay.graphics.moveTo(12, 18);
					ShowTransOverlay.graphics.lineTo(5, 25);
					ShowTransOverlay.graphics.lineStyle(2, 0, 1);
					ShowTransOverlay.graphics.moveTo(5, 25);
					ShowTransOverlay.graphics.lineTo(5, 12);
					ShowTransOverlay.graphics.lineTo(12, 5);
					ShowTransOverlay.graphics.lineTo(25, 5);
					ShowTransOverlay.graphics.lineTo(25, 18);
					ShowTransOverlay.graphics.lineTo(18, 25);
					ShowTransOverlay.graphics.lineTo(5, 25);
					ShowTransOverlay.graphics.moveTo(18, 12);
					ShowTransOverlay.graphics.lineTo(5, 12);
					ShowTransOverlay.graphics.moveTo(18, 12);
					ShowTransOverlay.graphics.lineTo(18, 25);
					ShowTransOverlay.graphics.moveTo(18, 12);
					ShowTransOverlay.graphics.lineTo(25, 5);
				}
				//Go To Ref
				{
					var txt5x:TextField = new TextField();
					GoToRefOverlay.addChild(txt5x);
					txt5x.defaultTextFormat = txtFormSmall;
					txt5x.text = "X";
					txt5x.embedFonts = true;
					txt5x.autoSize = TextFieldAutoSize.LEFT;
					txt5x.selectable = false;
					txt5x.x = 0;
					txt5x.y = -2;
					var txt5y:TextField = new TextField();
					GoToRefOverlay.addChild(txt5y);
					txt5y.defaultTextFormat = txtFormSmall;
					txt5y.text = "Y";
					txt5y.embedFonts = true;
					txt5y.autoSize = TextFieldAutoSize.LEFT;
					txt5y.selectable = false;
					txt5y.x = 8;
					txt5y.y = 6;
					var txt5z:TextField = new TextField();
					GoToRefOverlay.addChild(txt5z);
					txt5z.defaultTextFormat = txtFormSmall;
					txt5z.text = "Z";
					txt5z.embedFonts = true;
					txt5z.autoSize = TextFieldAutoSize.LEFT;
					txt5z.selectable = false;
					txt5z.x = 16;
					txt5z.y = 14;
				}
				//Go To Max Shear
				{
					var txtForm6:TextFormat = new TextFormat();
					txtForm6.font = "Symbol";
					txtForm6.size = 24;
					txtForm6.bold = true;
					txtForm6.italic = true;
					txtForm6.leading = 0;
					
					var txtForm6b:TextFormat = new TextFormat();
					txtForm6b.font = "CalibriBI";
					txtForm6b.size = 8;
					txtForm6b.bold = true;
					txtForm6b.italic = true;
					txtForm6b.leftMargin = 0;
					txtForm6b.leading = 0;
					
					var txt6a:TextField = new TextField();
					GoToMaxShearOverlay.addChild(txt6a);
					txt6a.defaultTextFormat = txtForm6;
					txt6a.text = "t";
					txt6a.embedFonts = true;
					txt6a.autoSize = TextFieldAutoSize.CENTER;
					txt6a.selectable = false;
					//txt6a.background = false;
					//txt6a.multiline = false;
					txt6a.x = 0;
					txt6a.y = -6;
					var txt6b:TextField = new TextField();
					GoToMaxShearOverlay.addChild(txt6b);
					txt6b.defaultTextFormat = txtForm6b;
					txt6b.text = "max";
					txt6b.embedFonts = true;
					txt6b.autoSize = TextFieldAutoSize.LEFT;
					txt6b.selectable = false;
					txt6b.x = 11;
					txt6b.y = 14;
				}
				//Go To Max Prin
				{
					var txtForm7:TextFormat = new TextFormat();
					txtForm7.font = "Symbol";
					txtForm7.size = 24;
					txtForm7.bold = true;
					txtForm7.italic = true;
					txtForm7.leading = 0;
					
					var txtForm7b:TextFormat = new TextFormat();
					txtForm7b.font = "CalibriBI";
					txtForm7b.size = 10;
					txtForm7b.bold = true;
					txtForm7b.italic = true;
					txtForm7b.leftMargin = 0;
					txtForm7b.leading = 0;
					
					var txt7a:TextField = new TextField();
					GoToPrinOverlay.addChild(txt7a);
					txt7a.defaultTextFormat = txtForm7;
					txt7a.text = "s";
					txt7a.embedFonts = true;
					txt7a.autoSize = TextFieldAutoSize.CENTER;
					txt7a.selectable = false;
					txt7a.x = 0;
					txt7a.y = -6;
					var txt7b:TextField = new TextField();
					GoToPrinOverlay.addChild(txt7b);
					txt7b.defaultTextFormat = txtForm7b;
					txt7b.text = "P";
					txt7b.embedFonts = true;
					txt7b.autoSize = TextFieldAutoSize.LEFT;
					txt7b.selectable = false;
					txt7b.x = 14;
					txt7b.y = 14;
				}
				
				var txtGoTo:TextFormat = new TextFormat();
				txtGoTo.font = "CalibriB";
				txtGoTo.size = 9
				txtGoTo.bold = true;
				txtGoTo.leading = 0;
				var txtGoToLetter:TextFormat = new TextFormat();
				txtGoToLetter.font = "TimesNewRomanBI";
				txtGoToLetter.size = 20;
				txtGoToLetter.bold = true;
				txtGoToLetter.italic = true;
				txtGoToLetter.leading = 0;
				
				//Go To Base X
				{
					var txt8a:TextField = new TextField();
					GoToXOverlay.addChild(txt8a);
					txt8a.defaultTextFormat = txtGoTo;
					txt8a.text = "Go To:";
					txt8a.embedFonts = true;
					txt8a.autoSize = TextFieldAutoSize.LEFT;
					txt8a.selectable = false;
					txt8a.x = 0;
					txt8a.y = -1;
					var txt8b:TextField = new TextField();
					GoToXOverlay.addChild(txt8b);
					txt8b.defaultTextFormat = txtGoToLetter;
					txt8b.text = "X ";
					txt8b.embedFonts = true;
					txt8b.autoSize = TextFieldAutoSize.LEFT;
					txt8b.selectable = false;
					txt8b.x = 5;
					txt8b.y = 6.5;
				}
				//Go To Base Y
				{
					var txt9a:TextField = new TextField();
					GoToYOverlay.addChild(txt9a);
					txt9a.defaultTextFormat = txtGoTo;
					txt9a.text = "Go To:";
					txt9a.embedFonts = true;
					txt9a.autoSize = TextFieldAutoSize.LEFT;
					txt9a.selectable = false;
					txt9a.x = 0;
					txt9a.y = -1;
					var txt9b:TextField = new TextField();
					GoToYOverlay.addChild(txt9b);
					txt9b.defaultTextFormat = txtGoToLetter;
					txt9b.text = "Y ";
					txt9b.embedFonts = true;
					txt9b.autoSize = TextFieldAutoSize.LEFT;
					txt9b.selectable = false;
					txt9b.x = 5;
					txt9b.y = 6.5;
				}
				//Go To Base Z
				{
					var txt10a:TextField = new TextField();
					GoToZOverlay.addChild(txt10a);
					txt10a.defaultTextFormat = txtGoTo;
					txt10a.text = "Go To:";
					txt10a.embedFonts = true;
					txt10a.autoSize = TextFieldAutoSize.LEFT;
					txt10a.selectable = false;
					txt10a.x = 0;
					txt10a.y = -1;
					var txt10b:TextField = new TextField();
					GoToZOverlay.addChild(txt10b);
					txt10b.defaultTextFormat = txtGoToLetter;
					txt10b.text = "Z ";
					txt10b.embedFonts = true;
					txt10b.autoSize = TextFieldAutoSize.LEFT;
					txt10b.selectable = false;
					txt10b.x = 5;
					txt10b.y = 6.5;
				}
				//Go To Body x
				{
					var txt11a:TextField = new TextField();
					GoToxOverlay.addChild(txt11a);
					txt11a.defaultTextFormat = txtGoTo;
					txt11a.text = "Go To:";
					txt11a.embedFonts = true;
					txt11a.autoSize = TextFieldAutoSize.LEFT;
					txt11a.selectable = false;
					txt11a.x = 0;
					txt11a.y = -1;
					var txt11b:TextField = new TextField();
					GoToxOverlay.addChild(txt11b);
					txt11b.defaultTextFormat = txtGoToLetter;
					txt11b.text = "x ";
					txt11b.embedFonts = true;
					txt11b.autoSize = TextFieldAutoSize.LEFT;
					txt11b.selectable = false;
					txt11b.x = 7;
					txt11b.y = 4;
				}
				//Go To Body y
				{
					var txt12a:TextField = new TextField();
					GoToyOverlay.addChild(txt12a);
					txt12a.defaultTextFormat = txtGoTo;
					txt12a.text = "Go To:";
					txt12a.embedFonts = true;
					txt12a.autoSize = TextFieldAutoSize.LEFT;
					txt12a.selectable = false;
					txt12a.x = 0;
					txt12a.y = -1;
					var txt12b:TextField = new TextField();
					GoToyOverlay.addChild(txt12b);
					txt12b.defaultTextFormat = txtGoToLetter;
					txt12b.text = "y ";
					txt12b.embedFonts = true;
					txt12b.autoSize = TextFieldAutoSize.LEFT;
					txt12b.selectable = false;
					txt12b.x = 7;
					txt12b.y = 4;
				}
				//Go To Body z
				{
					var txt13a:TextField = new TextField();
					GoTozOverlay.addChild(txt13a);
					txt13a.defaultTextFormat = txtGoTo;
					txt13a.text = "Go To:";
					txt13a.embedFonts = true;
					txt13a.autoSize = TextFieldAutoSize.LEFT;
					txt13a.selectable = false;
					txt13a.x = 0;
					txt13a.y = -1;
					var txt13b:TextField = new TextField();
					GoTozOverlay.addChild(txt13b);
					txt13b.defaultTextFormat = txtGoToLetter;
					txt13b.text = "z ";
					txt13b.embedFonts = true;
					txt13b.autoSize = TextFieldAutoSize.LEFT;
					txt13b.selectable = false;
					txt13b.x = 7;
					txt13b.y = 4;
				}
				//Go To Ortho
				{
					var txt134a:TextField = new TextField();
					GoToOrthoOverlay.addChild(txt134a);
					txt134a.defaultTextFormat = txtGoTo;
					txt134a.text = "Go To:";
					txt134a.embedFonts = true;
					txt134a.autoSize = TextFieldAutoSize.LEFT;
					txt134a.selectable = false;
					txt134a.x = 0;
					txt134a.y = -1;
					
					GoToOrthoOverlay.graphics.lineStyle(2, 0, 1);
					GoToOrthoOverlay.graphics.moveTo(15, 20);
					GoToOrthoOverlay.graphics.lineTo(15, 27);
					GoToOrthoOverlay.graphics.lineTo(21, 23);
					GoToOrthoOverlay.graphics.lineTo(21, 16);
					GoToOrthoOverlay.graphics.moveTo(15, 20);
					GoToOrthoOverlay.graphics.lineTo(9, 16);
					GoToOrthoOverlay.graphics.lineTo(9, 23);
					GoToOrthoOverlay.graphics.lineTo(15, 27);
					GoToOrthoOverlay.graphics.moveTo(15, 20);
					GoToOrthoOverlay.graphics.lineTo(21, 16);
					GoToOrthoOverlay.graphics.lineTo(15, 12);
					GoToOrthoOverlay.graphics.lineTo(9, 16);
				}
			}
			
			if (!this.DisplayType2D)
			{
				this.butShowBaseAxes = new PushButton(215,5,30,30,true,PushButton.SWITCH_WHEN_PRESSED);
				//this.butShowBaseAxes.PictureDown = showRefAxesDownImage;
				//this.butShowBaseAxes.PictureUp = showRefAxesUpImage;
				//this.butShowBaseAxes.PictureEnabled = true;
				this.butShowBaseAxes.Overlay = ShowBaseAxesOverlay;
				this.addChild(this.butShowBaseAxes);
				this.butShowBodyAxes = new PushButton(245,5,30,30,true,PushButton.SWITCH_WHEN_PRESSED);
				//this.butShowBodyAxes.PictureDown = showBodyAxesDownImage;
				//this.butShowBodyAxes.PictureUp = showBodyAxesUpImage;
				//this.butShowBodyAxes.PictureEnabled = true;
				this.butShowBodyAxes.Overlay = ShowBodyAxesOverlay;
				this.addChild(this.butShowBodyAxes);
				this.butShowEigenVectors = new PushButton(275,5,30,30,false,PushButton.SWITCH_WHEN_PRESSED);
				//this.butShowBodyAxes.PictureDown = showBodyAxesDownImage;
				//this.butShowBodyAxes.PictureUp = showBodyAxesUpImage;
				//this.butShowBodyAxes.PictureEnabled = true;
				this.butShowEigenVectors.Overlay = ShowEigenVectorsOverlay;
				this.addChild(this.butShowEigenVectors);
				this.butShowForces = new PushButton(305,5,30,30,true,PushButton.SWITCH_WHEN_PRESSED);
				//this.butShowForces.PictureDown = showForcesDownImage;
				//this.butShowForces.PictureUp = showForcesUpImage;
				//this.butShowForces.PictureEnabled = true;
				this.butShowForces.Overlay = ShowForcesOverlay;
				this.addChild(this.butShowForces);
				this.butShowResultants = new PushButton(335,5,30,30,false,PushButton.SWITCH_WHEN_PRESSED);
				//this.butShowResultants.PictureDown = showResultantsDownImage;
				//this.butShowResultants.PictureUp = showResultantsUpImage;
				//this.butShowResultants.PictureEnabled = true;
				this.butShowResultants.Overlay = ShowResultantsOverlay;
				this.addChild(this.butShowResultants);
				this.butBodyIsTransparent = new PushButton(365, 5, 30, 30, false,PushButton.SWITCH_WHEN_PRESSED);
				//this.butBodyIsTransparent.PictureDown = showTransDownImage;
				//this.butBodyIsTransparent.PictureUp = showTransUpImage;
				//this.butBodyIsTransparent.PictureEnabled = true;
				this.butBodyIsTransparent.Overlay = this.ShowTransOverlay;
				this.addChild(this.butBodyIsTransparent);
				//trace(this.butShowBaseAxes.Status);
			}
			else
			{
				this.butBodyIsTransparent = new PushButton(365, 5, 30, 30, true,PushButton.SWITCH_WHEN_PRESSED);
				//this.butBodyIsTransparent.PictureDown = showTransDownImage;
				//this.butBodyIsTransparent.PictureUp = showTransUpImage;
				//this.butBodyIsTransparent.PictureEnabled = true;
				this.butBodyIsTransparent.Overlay = this.ShowTransOverlay;
				//this.addChild(this.butBodyIsTransparent);
				
				this.butShowBaseAxes = new PushButton(245,5,30,30,true,PushButton.SWITCH_WHEN_PRESSED);
				//this.butShowBaseAxes.PictureDown = showRefAxesDownImage;
				//this.butShowBaseAxes.PictureUp = showRefAxesUpImage;
				//this.butShowBaseAxes.PictureEnabled = true;
				this.butShowBaseAxes.Overlay = ShowBaseAxesOverlay;
				this.addChild(this.butShowBaseAxes);
				this.butShowBodyAxes = new PushButton(275,5,30,30,true,PushButton.SWITCH_WHEN_PRESSED);
				//this.butShowBodyAxes.PictureDown = showBodyAxesDownImage;
				//this.butShowBodyAxes.PictureUp = showBodyAxesUpImage;
				//this.butShowBodyAxes.PictureEnabled = true;
				this.butShowBodyAxes.Overlay = ShowBodyAxesOverlay;
				this.addChild(this.butShowBodyAxes);
				this.butShowEigenVectors = new PushButton(305,5,30,30,false,PushButton.SWITCH_WHEN_PRESSED);
				//this.butShowBodyAxes.PictureDown = showBodyAxesDownImage;
				//this.butShowBodyAxes.PictureUp = showBodyAxesUpImage;
				//this.butShowBodyAxes.PictureEnabled = true;
				this.butShowEigenVectors.Overlay = ShowEigenVectorsOverlay;
				this.addChild(this.butShowEigenVectors);
				this.butShowForces = new PushButton(335,5,30,30,true,PushButton.SWITCH_WHEN_PRESSED);
				//this.butShowForces.PictureDown = showForcesDownImage;
				//this.butShowForces.PictureUp = showForcesUpImage;
				//this.butShowForces.PictureEnabled = true;
				this.butShowForces.Overlay = ShowForcesOverlay;
				this.addChild(this.butShowForces);
				this.butShowResultants = new PushButton(365,5,30,30,false,PushButton.SWITCH_WHEN_PRESSED);
				//this.butShowResultants.PictureDown = showResultantsDownImage;
				//this.butShowResultants.PictureUp = showResultantsUpImage;
				//this.butShowResultants.PictureEnabled = true;
				this.butShowResultants.Overlay = ShowResultantsOverlay;
				this.addChild(this.butShowResultants);
			}
			
			this.butShowBaseAxes.addEventListener(PushButtonEvent.VALUE_CHANGE, ButRedraw);
			this.butShowBodyAxes.addEventListener(PushButtonEvent.VALUE_CHANGE, ButRedraw);
			this.butShowEigenVectors.addEventListener(PushButtonEvent.VALUE_CHANGE, ButRedraw);
			this.butShowForces.addEventListener(PushButtonEvent.VALUE_CHANGE, ButRedraw);
			this.butShowResultants.addEventListener(PushButtonEvent.VALUE_CHANGE, ButRedraw);
			this.butBodyIsTransparent.addEventListener(PushButtonEvent.VALUE_CHANGE, ButRedraw);
			
			this.butGoToPrincipal = new PushButton(305,390,30,30,false,PushButton.SWITCH_UNTIL_RELEASED_OR_LOCKED);
			//this.butGoToPrincipal.PictureDown = snapPrinDownImage;
			//this.butGoToPrincipal.PictureUp = snapPrinUpImage;
			//this.butGoToPrincipal.PictureEnabled = true;
			this.butGoToPrincipal.Overlay = this.GoToPrinOverlay;
			this.addChild(this.butGoToPrincipal);
			this.butGoToMaxShear = new PushButton(335,390,30,30,false,PushButton.SWITCH_UNTIL_RELEASED_OR_LOCKED);
			//this.butGoToMaxShear.PictureDown = snapMaxShearDownImage;
			//this.butGoToMaxShear.PictureUp = snapMaxShearUpImage;
			//this.butGoToMaxShear.PictureEnabled = true;
			this.butGoToMaxShear.Overlay = GoToMaxShearOverlay;
			this.addChild(this.butGoToMaxShear);
			this.butGoToRef = new PushButton(365,390,30,30,false,PushButton.SWITCH_UNTIL_RELEASED);
			//this.butGoToRef.PictureDown = snapRefDownImage;
			//this.butGoToRef.PictureUp = snapRefUpImage;
			//this.butGoToRef.PictureEnabled = true;
			this.butGoToRef.Overlay = this.GoToRefOverlay;
			this.addChild(this.butGoToRef);
			
			if (!this.DisplayType2D)
			{
				this.butRotNormal = new PushButton(5, 5, 30, 30, true, PushButton.SWITCH_WHEN_PRESSED);
				//this.butRotNormal.PictureDown = rotRefDownImage;
				//this.butRotNormal.PictureUp = rotRefUpImage;
				//this.butRotNormal.PictureEnabled = true;
				this.butRotNormal.Overlay = this.RotBaseOverlay;
				this.addChild(this.butRotNormal);
				this.butRotBody = new PushButton(35, 5, 30, 30, false, PushButton.SWITCH_WHEN_PRESSED);
				//this.butRotBody.PictureDown = rotBodyDownImage;
				//this.butRotBody.PictureUp = rotBodyUpImage;
				//this.butRotBody.PictureEnabled = true;
				this.butRotBody.Overlay = this.RotBodyOverlay;
				this.addChild(this.butRotBody);
				this.butRotBody2d = new PushButton(65, 5, 30, 30, false, PushButton.SWITCH_WHEN_PRESSED);
				//this.butRotBody2d.PictureDown = rotEyeDownImage;
				//this.butRotBody2d.PictureUp = rotEyeUpImage;
				//this.butRotBody2d.PictureEnabled = true;
				this.butRotBody2d.Overlay = this.RotBody2dOverlay;
				this.addChild(this.butRotBody2d);
			
			
				this.butRotNormal.addEventListener(PushButtonEvent.VALUE_CHANGE, RotateNormPushButtonHandler);
				this.butRotBody.addEventListener(PushButtonEvent.VALUE_CHANGE, Rotate3dPushButtonHandler);
				this.butRotBody2d.addEventListener(PushButtonEvent.VALUE_CHANGE, Rotate2dPushButtonHandler);
				
				this.butGoToXAxis = new PushButton(5, 390, 30, 30, false, PushButton.SWITCH_UNTIL_RELEASED);
				//this.butGoToXAxis.PictureDown = snapXAxisDownImage;
				//this.butGoToXAxis.PictureUp = snapXAxisUpImage;
				//this.butGoToXAxis.PictureEnabled = true;
				this.butGoToXAxis.Overlay = this.GoToXOverlay;
				this.butGoToXAxis.addEventListener(PushButtonEvent.VALUE_CHANGE, ButGoToXAxisHandler);
				this.addChild(this.butGoToXAxis);
				this.butGoToYAxis = new PushButton(35, 390, 30, 30, false, PushButton.SWITCH_UNTIL_RELEASED);
				//this.butGoToYAxis.PictureDown = snapYAxisDownImage;
				//this.butGoToYAxis.PictureUp = snapYAxisUpImage;
				//this.butGoToYAxis.PictureEnabled = true;
				this.butGoToYAxis.Overlay = this.GoToYOverlay;
				this.butGoToYAxis.addEventListener(PushButtonEvent.VALUE_CHANGE, ButGoToYAxisHandler);
				this.addChild(this.butGoToYAxis);
				this.butGoToZAxis = new PushButton(65, 390, 30, 30, false, PushButton.SWITCH_UNTIL_RELEASED);
				//this.butGoToZAxis.PictureDown = snapZAxisDownImage;
				//this.butGoToZAxis.PictureUp = snapZAxisUpImage;
				//this.butGoToZAxis.PictureEnabled = true;
				this.butGoToZAxis.Overlay = this.GoToZOverlay;
				this.butGoToZAxis.addEventListener(PushButtonEvent.VALUE_CHANGE, ButGoToZAxisHandler);
				this.addChild(this.butGoToZAxis);
				
				this.butGoToxAxis = new PushButton(95, 390, 30, 30, false, PushButton.SWITCH_UNTIL_RELEASED_OR_LOCKED);
				//this.butGoToxAxis.PictureDown = snapxAxisDownImage;
				//this.butGoToxAxis.PictureUp = snapxAxisUpImage;
				//this.butGoToxAxis.PictureEnabled = true;
				this.butGoToxAxis.Overlay = this.GoToxOverlay;
				this.butGoToxAxis.addEventListener(PushButtonEvent.VALUE_CHANGE, ButGoToxAxisHandler);
				this.addChild(this.butGoToxAxis);
				this.butGoToyAxis = new PushButton(125, 390, 30, 30, false, PushButton.SWITCH_UNTIL_RELEASED_OR_LOCKED);
				//this.butGoToyAxis.PictureDown = snapyAxisDownImage;
				//this.butGoToyAxis.PictureUp = snapyAxisUpImage;
				//this.butGoToyAxis.PictureEnabled = true;
				this.butGoToyAxis.Overlay = this.GoToyOverlay;
				this.butGoToyAxis.addEventListener(PushButtonEvent.VALUE_CHANGE, ButGoToyAxisHandler);
				this.addChild(this.butGoToyAxis);
				this.butGoTozAxis = new PushButton(155, 390, 30, 30, false, PushButton.SWITCH_UNTIL_RELEASED_OR_LOCKED);
				//this.butGoTozAxis.PictureDown = snapzAxisDownImage;
				//this.butGoTozAxis.PictureUp = snapzAxisUpImage;
				//this.butGoTozAxis.PictureEnabled = true;
				this.butGoTozAxis.Overlay = this.GoTozOverlay;
				this.butGoTozAxis.addEventListener(PushButtonEvent.VALUE_CHANGE, ButGoTozAxisHandler);
				this.addChild(this.butGoTozAxis);
				
				this.butGoToOrtho = new PushButton(190, 390, 30, 30, false, PushButton.SWITCH_UNTIL_RELEASED);
				//this.butGoToOrtho.PictureDown = snapOrthoDownImage;
				//this.butGoToOrtho.PictureUp = snapOrthoUpImage;
				//this.butGoToOrtho.PictureEnabled = true;
				this.butGoToOrtho.Overlay = this.GoToOrthoOverlay;
				this.butGoToOrtho.addEventListener(PushButtonEvent.VALUE_CHANGE, ButGoToOrthoHandler);
				this.addChild(this.butGoToOrtho);
				
			}
			else
			{
				this.GoToZAxis();
				
			}
		}
		public function set RotateType(type:String):void
		{
			this.butRotNormal.Enabled = true;
			switch(type)
			{
				case Viewer.ROTATE_VIEW:
					this.butRotNormal.Status = true;
					this.butRotNormal.Enabled = false;
					this.butRotBody.Status = false;
					this.butRotBody2d.Status = false;
					break;
				case Viewer.ROTATE_BODY_3D:
					this.butRotNormal.Status = false;
					this.butRotBody.Status = true;
					this.butRotBody2d.Status = false;
					break;
				case Viewer.ROTATE_BODY_2D:
					this.butRotNormal.Status = false;
					this.butRotBody.Status = false;
					this.butRotBody2d.Status = true;
					break;
				default:
					break;
			}
		}
		public function get RotateType():String
		{
			if (this.DisplayType2D)
			{
				return Viewer.ROTATE_BODY_2D;
			}
			if (this.butRotNormal.Status)
			{
				return Viewer.ROTATE_VIEW;
			}
			else if (this.butRotBody.Status)
			{
				return Viewer.ROTATE_BODY_3D;
			}
			else
			{
				return Viewer.ROTATE_BODY_2D;
			}
		}

		private function ButGoToOrthoHandler(e:PushButtonEvent):void
		{
			this._eyePositionCS.SetAxes(new SpaceVector(-1/Math.sqrt(2), 1/Math.sqrt(2), 0), VMath.Unity(new SpaceVector(-1, -1, Math.sqrt(4))));
			this.butGoToxAxis.Status = false;
			this.butGoToyAxis.Status = false;
			this.butGoTozAxis.Status = false;
			this.Redraw();
		}
		private function ButGoToXAxisHandler(e:PushButtonEvent):void
		{
			this._eyePositionCS.SetAxes(new SpaceVector(0, 1, 0), new SpaceVector(0, 0, 1));
			this.butGoToxAxis.Status = false;
			this.butGoToyAxis.Status = false;
			this.butGoTozAxis.Status = false;
			this.Redraw();
		}
		private function ButGoToYAxisHandler(e:PushButtonEvent):void
		{
			this._eyePositionCS.SetAxes(new SpaceVector(-1, 0, 0), new SpaceVector(0, 0, 1));
			this.butGoToxAxis.Status = false;
			this.butGoToyAxis.Status = false;
			this.butGoTozAxis.Status = false;
			this.Redraw();
		}
		private function ButGoToZAxisHandler(e:PushButtonEvent):void
		{
			this.GoToZAxis();
			this.butGoToxAxis.Status = false;
			this.butGoToyAxis.Status = false;
			this.butGoTozAxis.Status = false;
			this.Redraw();
		}
		private function GoToZAxis():void
		{
			this._eyePositionCS.SetAxes(new SpaceVector(1, 0, 0), new SpaceVector(0, 1, 0));
		}
		private function ButGoToxAxisHandler(e:PushButtonEvent):void
		{
			if (e.locked)
			{
				this.RotateType = Viewer.ROTATE_BODY_2D;
				this.butGoToMaxShear.Status = false;
				this.butGoToPrincipal.Status = false;
			}
			//this.butGoToxAxis.Status = false;
			this.butGoToyAxis.Status = false;
			this.butGoTozAxis.Status = false;
			this.Redraw();
		}
		private function GoToxAxis():void
		{
			this._eyePositionCS.SetAxes(new SpaceVector(0, 1, 0,this.quad[0].Point[0].coordinateSystem).TransformToBase(), new SpaceVector(0, 0, 1, this.quad[0].Point[0].coordinateSystem).TransformToBase());
		}
		private function ButGoToyAxisHandler(e:PushButtonEvent):void
		{
			if (e.locked)
			{
				this.RotateType = Viewer.ROTATE_BODY_2D;
				this.butGoToMaxShear.Status = false;
				this.butGoToPrincipal.Status = false;
			}
			this.butGoToxAxis.Status = false;
			//this.butGoToyAxis.Status = false;
			this.butGoTozAxis.Status = false;
			this.Redraw();
		}
		private function GoToyAxis():void
		{
			this._eyePositionCS.SetAxes(new SpaceVector(-1, 0, 0,this.quad[0].Point[0].coordinateSystem).TransformToBase(), new SpaceVector(0, 0, 1, this.quad[0].Point[0].coordinateSystem).TransformToBase());
		}
		private function ButGoTozAxisHandler(e:PushButtonEvent):void
		{
			if (e.locked)
			{
				this.RotateType = Viewer.ROTATE_BODY_2D;
				this.butGoToMaxShear.Status = false;
				this.butGoToPrincipal.Status = false;
			}
			this.butGoToxAxis.Status = false;
			this.butGoToyAxis.Status = false;
			//this.butGoTozAxis.Status = false;
			this.Redraw();
		}
		private function GoTozAxis():void
		{
			this._eyePositionCS.SetAxes(new SpaceVector(1, 0, 0,this.quad[0].Point[0].coordinateSystem).TransformToBase(), new SpaceVector(0, 1, 0, this.quad[0].Point[0].coordinateSystem).TransformToBase());
		}
		
		private function RotateNormPushButtonHandler(e:PushButtonEvent):void
		{
			if (this.butRotNormal.Status)
			{
				this.RotateType = Viewer.ROTATE_VIEW;
				this.butRotBody2d.Status = false;
				this.butRotBody.Status = false;
				this.butRotNormal.Enabled = false;
				this.butGoToxAxis.Status = false;
				this.butGoToyAxis.Status = false;
				this.butGoTozAxis.Status = false;
			}
		}
		private function Rotate3dPushButtonHandler(e:PushButtonEvent):void
		{
			if (e.value)
			{
				this.RotateType = Viewer.ROTATE_BODY_3D;
				this.butRotNormal.Status = false;
				this.butRotBody2d.Status = false;
				this.butRotNormal.Enabled = true;
				this.butGoToMaxShear.Status = false;
				this.butGoToPrincipal.Status = false;
			}
			else
			{
				this.RotateType = Viewer.ROTATE_VIEW;
				this.butRotNormal.Status = true;
				this.butGoToMaxShear.Status = false;
				this.butGoToPrincipal.Status = false;
			}
		}
		private function Rotate2dPushButtonHandler(e:PushButtonEvent):void
		{
			if (e.value)
			{
				this.RotateType = Viewer.ROTATE_BODY_2D;
				this.butRotNormal.Status = false;
				this.butRotBody.Status = false;
				this.butRotNormal.Enabled = true;
			}
			else
			{
				this.RotateType = Viewer.ROTATE_VIEW;
				this.butRotNormal.Status = true;
			}
		}
		private function ButRedraw(e:PushButtonEvent):void
		{
			this.Redraw();
		}
		private function MouseDownHandler(e:MouseEvent):void
		{
			//trace("down:", e.localX);
			//deleted 17 Nov 08 -tbh- BUG Context menu not appearing after running swf
			//cleared 17 Nov 08 -tbh- BUG Viewer disappears when rotating, then right clicking
			this.addEventListener(MouseEvent.MOUSE_MOVE, MouseMoveHandler);
			this.addEventListener(MouseEvent.MOUSE_UP, MouseUpHandler);
			this.addEventListener(MouseEvent.MOUSE_OUT, MouseOutHandler);
			this.mouseDownStart.x = e.stageX;
			this.mouseDownStart.y = e.stageY;
			this.mouseDownShiftKey = e.shiftKey;
			//this.RotateView(new SpaceVector(0, 1, 0), Math.PI / -4);
			//this.Redraw();
			//trace(e.buttonDown);
		}
		private function MouseMoveHandler(e:MouseEvent):void
		{
			if (moveLastX != 0 && moveLastY != 0)
			{
				var diffX:Number = e.stageX - moveLastX;
				var diffY:Number = e.stageY - moveLastY;
				//trace(diffX, diffY );
				if (diffX == 0 && diffY == 0)
				{
					this.MouseUpHandler(e);
					return;
				}
			}
			moveLastX = e.stageX;
			moveLastY = e.stageY;
			var deltaX:Number = e.stageX - this.mouseDownStart.x;
			var deltaY:Number = e.stageY - this.mouseDownStart.y;
			if (true)//!this.mouseDownShiftKey)
			{
				//rotation
				/**
				 * with viewX and viewY defined as a cartesian grid on the screen,
				 * a mouse movement in the X or Y direction always causes a delta phi
				 * about an axis defined as (delta X + delta Y) cross (z-axis).  this
				 * should rotate the eyeCS along that resultant vector.  the amount of
				 * rotation really just depends on some proportionality constant multiplied
				 * against the unit vector in the z direction (which is the eyeView direction)
				 * that proportionality constant
				 * should be set up in such a way that when you are zoomed in real close it doesnt
				 * make the rotation go wild.
				 *
				 * the coordinate system should accept rotation parameters as a vector and
				 * magnitude with the vector defining direction and the magnitude as a rotation
				 * in radians about that vector
				 */
				switch(this.RotateType)
				{
					case Viewer.ROTATE_VIEW:
						var rotScale:Number = MathEx.Deg2Rad(5.0) / 5; // radians per pixel
						var rotAngle:Number = Math.sqrt(deltaX * deltaX + deltaY * deltaY) * rotScale;

						var deltaVector:SpaceVector = new SpaceVector(deltaX, -deltaY, 0, this._eyePositionCS);
						var rotDir:SpaceVector = VMath.Cross(VMath.ScalarProduct( -1, this.eyeView), deltaVector);
						rotDir.coordinateSystem = this._eyePositionCS;
						
						this.RotateView(rotDir,rotAngle);
						break;
					case Viewer.ROTATE_BODY_2D:
						//Rotates about eyePosition vector
						//cos^(-1) of dotP/mag
						var xOrigin:Number = this.CoordinateSystem2Parent(this._eyePosition).x;
						var yOrigin:Number = this.CoordinateSystem2Parent(this._eyePosition).y;
						var x1:Number = this.mouseDownStart.x - this.x - this.grid.x - this.plotArea.x;
						var y1:Number = this.mouseDownStart.y - this.y - this.grid.y - this.plotArea.y;
						var x2:Number = e.localX - this.grid.x - this.plotArea.x;
						var y2:Number = e.localY - this.grid.y - this.plotArea.y;
						x1 -= xOrigin;
						y1 -= yOrigin;
						x2 -= xOrigin;
						y2 -= yOrigin;
						y1 = -y1;
						y2 = -y2;
						var oneSv:SpaceVector = new SpaceVector(x1, y1, 0);
						var twoSv:SpaceVector = new SpaceVector(x2, y2, 0);
						var dotP:Number = VMath.Dot(oneSv,twoSv);
						var mag:Number = (VMath.Magnitude(oneSv) * VMath.Magnitude(twoSv));
						var cos:Number = Math.atan2(y1, x1);
						var cross:SpaceVector = VMath.Cross(oneSv, twoSv);
						var angle:Number = Math.acos(dotP / mag);
						if ( VMath.Dot(cross, this._eyePosition) > 0)
						{
							angle = -angle;
						}
						var rotDirection:SpaceVector = this._eyePosition;
						this.quad[0].Point[0].coordinateSystem.Rotate(rotDirection, angle);
						var tmp:Coordinate = this.grid.Parent2Grid(x1, y1);
						//trace(x1,y1,x2,y2);
						this.dispatchEvent(new Event(Event.RENDER));
						break;
					case Viewer.ROTATE_BODY_3D:
						var rotScale3d:Number = MathEx.Deg2Rad(5.0) / 5; // radians per pixel
						var rotAngle3d:Number = Math.sqrt(deltaX * deltaX + deltaY * deltaY) * rotScale3d;

						var deltaVector3d:SpaceVector = new SpaceVector(deltaX, -deltaY, 0, this._eyePositionCS);
						var rotDir3d:SpaceVector = VMath.Cross(VMath.ScalarProduct( -1, this.eyeView), deltaVector3d);
						rotDir3d.coordinateSystem = this._eyePositionCS;
						
						this.quad[0].Point[0].coordinateSystem.Rotate(rotDir3d,-rotAngle3d);
						this.dispatchEvent(new Event(Event.RENDER));
						break;
					default:
						break;
				}
			}
			else
			{
				//Translation Routine
				//(none currently needed)
			}
			
			this.mouseDownStart.x = e.stageX;
			this.mouseDownStart.y = e.stageY;
			
			this.Redraw();
		}
		public function RotateView(v:SpaceVector,rad:Number):void
		{
			this._eyePositionCS.Rotate(v,rad);
		}
		private function MouseOutHandler(e:MouseEvent):void
		{
			this.MouseUpHandler(e);
		}
		private function MouseUpHandler(e:MouseEvent):void
		{
			this.removeEventListener(MouseEvent.MOUSE_MOVE, MouseMoveHandler);
			this.removeEventListener(MouseEvent.MOUSE_UP, MouseUpHandler);
			this.removeEventListener(MouseEvent.MOUSE_OUT, MouseOutHandler);
			moveLastX = 0;
			moveLastY = 0;
			//this.MakeDefaultRotateType();
		}
		private function MakeDefaultRotateType():void
		{
			this.RotateType = Viewer.ROTATE_VIEW;
			this.butRotBody.Status = false;
			this.butRotBody2d.Status = false;
			this.butRotNormal.Status = true;
		}
		public function Redraw():void
		{
			var i:int;
			//trace(VMath.Norm(VMath.Cross(this.eyePosition, this.eyeView)));
			this.plotArea.graphics.clear();
			var childrenLength:int = this.plotAreaChildren.length
			for (var j:int =childrenLength - 1; j >= 0; j--)
			{
				if (this.plotArea.contains(this.plotAreaChildren[j]))
				{
					this.plotArea.removeChild(this.plotAreaChildren[j]);
				}
				if (this.subPlotArea.contains(this.plotAreaChildren[j]))
				{
					this.subPlotArea.removeChild(this.plotAreaChildren[j]);
				}
				this.plotAreaChildren.pop();
			}
			this.subPlotArea.graphics.clear();
			this.subPlotAreaOver.graphics.clear();
			if (this.butGoToxAxis.Status)
			{
				this.GoToxAxis();
			}
			else if (this.butGoToyAxis.Status)
			{
				this.GoToyAxis();
			}
			else if (this.butGoTozAxis.Status)
			{
				this.GoTozAxis();
			}
			
			//EyePosition vector in base coords:
			var EyePosBase:SpaceVector = eyePosition.TransformToBase();
			//EyePosition vector in body coords: (based on CS of 0th quad's 0th point)
			if (quad.length > 0)
			{
				var EyePosBody:SpaceVector = eyePosition.TransformToCS(quad[0].Point[0].coordinateSystem);
			}
			/* Order of draw will be:
			 * 	- Rear ref. axes
			 * 		- Rear body axes.
			 * 			- Rear forces
			 * 				- Rear cube faces
			 * 				- Front cube faces
			 * 	- Front ref. axes
			 * 		- Front body axes.
			 * 			- Front forces
			 * */
			
			//Add arc for 2d
			if (this.DisplayType2D && this.bodyArrow.length > 0)
			{
				if (!this.contains(text2dAngle))
				{
					this.text2dAngle.visible = true;
				}
				var center:Coordinate = this.grid.Grid2Parent(0, 0);
				var XAxis:Coordinate = this.grid.Grid2Parent(1, 0);
				var sp:SpaceVector = new SpaceVector(1, 0, 0);
				sp.coordinateSystem = bodyArrow[0].Point[0].coordinateSystem;
				var xAxis:Coordinate = this.grid.Grid2Parent(sp.TransformToBase().x, sp.TransformToBase().y);
				
				
				this.subPlotArea.graphics.lineStyle(2, 0x777777, 1);
				this.subPlotArea.graphics.drawCircle(center.x, center.y, grid.GridLength2Pixels(.7, 1, 0).x);
				this.subPlotAreaOver.graphics.lineStyle(2, 0, 0);
				this.subPlotAreaOver.graphics.beginFill(0xffffff, 1);
				this.subPlotAreaOver.graphics.drawCircle(center.x, center.y, grid.GridLength2Pixels(1, 1, 0).x);
				this.subPlotAreaOver.graphics.moveTo(center.x, center.y);
				this.subPlotAreaOver.graphics.lineTo(center.x + grid.GridLength2Pixels(1.5, 1, 0).x, center.y);
				var angle:Number = VMath.Angle(new SpaceVector(1, 0, 0), sp.TransformToBase());
				if (xAxis.y > center.y)
				{
					angle = 2 * Math.PI - angle;
				}
				this.text2dAngle.text = this.Number2DisplayText(angle*180/Math.PI);
				var mag:Number = this.grid.GridLength2Pixels(1, 1, 0).x;
				var coord14:Coordinate = new Coordinate(center.x + mag*Math.cos(angle/4), center.y - mag*Math.sin(angle/4));
				var coord12:Coordinate = new Coordinate(center.x + mag*Math.cos(angle/2), center.y - mag*Math.sin(angle/2));
				var coord34:Coordinate = new Coordinate(center.x + mag*Math.cos(3*angle/4), center.y - mag*Math.sin(3*angle/4));
				//this.plotArea.graphics.curveTo(coord.x,coord.y, center.x + 3 * (xAxis.x - center.x)/4, center.y + 3*(xAxis.y - center.y)/4);
				this.subPlotAreaOver.graphics.lineTo(coord14.x, coord14.y);
				this.subPlotAreaOver.graphics.lineTo(coord12.x, coord12.y);
				this.subPlotAreaOver.graphics.lineTo(coord34.x, coord34.y);
				this.subPlotAreaOver.graphics.lineTo(xAxis.x, xAxis.y);
				this.subPlotAreaOver.graphics.endFill();
				//trace(coord12.x, coord12.y);
				
				mag = 0.8;
				var mag2:Number = 0.6;
				this.subPlotAreaOver.graphics.lineStyle(2, 0x777777, 1);
				this.subPlotAreaOver.graphics.moveTo(center.x + mag2*(-center.x + XAxis.x), center.y + mag2*(-center.y + XAxis.y));
				this.subPlotAreaOver.graphics.lineTo(center.x + mag*(-center.x + XAxis.x), center.y + mag*(-center.y + XAxis.y));
				this.subPlotAreaOver.graphics.moveTo(center.x + mag2*(xAxis.x - center.x), center.y + mag2*(xAxis.y - center.y));
				this.subPlotAreaOver.graphics.lineTo(center.x + mag*(xAxis.x - center.x), center.y + mag*(xAxis.y - center.y));
				
				this.subPlotAreaOver.graphics.lineStyle(1, 0, 1);
				this.subPlotAreaOver.graphics.drawCircle(202, 156, 2);
			}
			
			var dotProduct:Number;
			var isViableIn3dGen:Boolean;
			//Rear ref. axes
			if (this.ShowBaseAxes)
			{
				for (i = 0; i < this.refArrow.length; i++)
				{
					dotProduct = VMath.Dot(refArrow[i].Point[0].TransformToBase(), EyePosBase);
					if ( dotProduct < 0)
					{
						this.DrawArrow(this.refArrow[i]);
					}
				}
			}
			
			//Rear EigenVectors
			if (this.ShowEigenVectors)
			{
				for (i = 0; i < this.EigenArrow.length; i++)
				{
					dotProduct = VMath.Dot(EigenArrow[i].Point[0].TransformToBase(), EyePosBase);
					if ( dotProduct < 0)
					{
						this.DrawArrow(this.EigenArrow[i]);
					}
				}
			}
			if (this.ShowEigenVectors && this.ShowBodyAxes && this.show3dGenStatus)
			{
				this.DrawEigenAngle(true);
			}
			
			//Rear body axes.
			if (this.ShowBodyAxes)
			{
				for (i = 0; i < this.bodyArrow.length; i++)
				{
					dotProduct = VMath.Dot(bodyArrow[i].Point[0].TransformToBase(), EyePosBase);
					isViableIn3dGen = true;
					if (this.show3dGenStatus)
					{
						isViableIn3dGen = MathEx.IsApproxEqual(VMath.Angle(this.bodyArrow[i].Origin, new SpaceVector(1, 0, 0)), 0, 4);
					}
					//trace(dotProduct);
					if ( dotProduct < 0 && isViableIn3dGen)
					{
						this.DrawArrow(this.bodyArrow[i]);
					}
				}
			}
			
			//Rear forces
			if (this.ShowForces)
			{
				//trace(this.ShowForces);
				for (i = 0; i < this.forceArrow.length; i++)
				{
					dotProduct = VMath.Dot(forceArrow[i].Point[0].TransformToBase(), EyePosBase);
					isViableIn3dGen = true;
					if (this.show3dGenStatus)
					{
						isViableIn3dGen = MathEx.IsApproxEqual(VMath.Angle(this.forceArrow[i].Origin, new SpaceVector(1, 0, 0)), 0, 4);
					}
					if ( dotProduct < 0 && isViableIn3dGen)
					{
						this.DrawArrow(this.forceArrow[i],true);
					}
				}
			}
			if (this.ShowResultants)
			{
				for (i = 0; i < this.resultantArrow.length; i++)
				{
					dotProduct = VMath.Dot(resultantArrow[i].Point[0].TransformToBase(), EyePosBase);
					isViableIn3dGen = true;
					if (this.show3dGenStatus)
					{
						isViableIn3dGen = MathEx.IsApproxEqual(VMath.Angle(this.resultantArrow[i].Origin, new SpaceVector(1, 0, 0)), 0, 4);
					}
					if ( dotProduct < 0 && isViableIn3dGen)
					{
						this.DrawArrow(this.resultantArrow[i],true);
					}
				}
			}
			
			//Rear cube faces
			for (i = 0; i < this.quad.length; i++)
			{
				dotProduct = VMath.Dot(quad[i].outwardNormal.TransformToBase(), EyePosBase);
				if ( dotProduct < 0)
				{
					this.DrawQuadrilateral(this.quad[i],i);
				}
			}
			//Front cube faces
			for (i = 0; i < this.quad.length; i++)
			{
				dotProduct = VMath.Dot(quad[i].outwardNormal.TransformToBase(), EyePosBase);
				if ( dotProduct >= 0)
				{
					this.DrawQuadrilateral(this.quad[i],3+i);
				}
			}
			
			//Front ref. axes
			if (this.ShowBaseAxes)
			{
				for (i = 0; i < this.refArrow.length; i++)
				{
					dotProduct = VMath.Dot(refArrow[i].Point[0].TransformToBase(), EyePosBase);
					if ( dotProduct >= 0)
					{
						this.DrawArrow(this.refArrow[i]);
					}
				}
			}
			
			//Front EigenVectors
			if (this.ShowEigenVectors)
			{
				for (i = 0; i < this.EigenArrow.length; i++)
				{
					dotProduct = VMath.Dot(EigenArrow[i].Point[0].TransformToBase(), EyePosBase);
					if ( dotProduct >= 0)
					{
						this.DrawArrow(this.EigenArrow[i]);
					}
				}
			}
			if (this.ShowEigenVectors && this.ShowBodyAxes && this.show3dGenStatus)
			{
				this.DrawEigenAngle(false);
			}
			
			//Front body axes.
			if (this.ShowBodyAxes)
			{
				for (i = 0; i < this.bodyArrow.length; i++)
				{
					dotProduct = VMath.Dot(bodyArrow[i].Point[0].TransformToBase(), EyePosBase);
					isViableIn3dGen = true;
					if (this.show3dGenStatus)
					{
						isViableIn3dGen = MathEx.IsApproxEqual(VMath.Angle(this.bodyArrow[i].Origin, new SpaceVector(1, 0, 0)), 0, 4);
					}
					if ( dotProduct >= 0 && isViableIn3dGen)
					{
						this.DrawArrow(this.bodyArrow[i]);
					}
				}
			}
			
			//Front forces
			if (this.ShowForces)
			{
				for (i = 0; i < this.forceArrow.length; i++)
				{
					dotProduct = VMath.Dot(forceArrow[i].Point[0].TransformToBase(), EyePosBase);
					if (this.show3dGenStatus)
					{
						isViableIn3dGen = MathEx.IsApproxEqual(VMath.Angle(this.forceArrow[i].Origin, new SpaceVector(1, 0, 0)), 0, 4);
					}
					if ( dotProduct >= 0 && isViableIn3dGen)
					{
						this.DrawArrow(this.forceArrow[i],true);
					}
				}
			}
			if (this.ShowResultants)
			{
				for (i = 0; i < this.resultantArrow.length; i++)
				{
					dotProduct = VMath.Dot(resultantArrow[i].Point[0].TransformToBase(), EyePosBase);
					if (this.show3dGenStatus)
					{
						isViableIn3dGen = MathEx.IsApproxEqual(VMath.Angle(this.resultantArrow[i].Origin, new SpaceVector(1, 0, 0)), 0, 4);
					}
					if ( dotProduct >= 0 && isViableIn3dGen)
					{
						this.DrawArrow(this.resultantArrow[i],true);
					}
				}
			}
			
			
			//Particles
			for (i = 0; i < this.particle.length; i++)
			{
				this.DrawParticle(this.particle[i]);
			}
			
		}
		private function DrawEigenAngle(back:Boolean):void
		{
			if (this.EigenVectors[2] == null)
			{
				return;
			}
			for (var i:int = 2; i >= 0; i--)
			{
				var cs:CoordinateSystem = new CoordinateSystem(this.EigenVectors[2], this.EigenVectors[1]);
				var cs2:CoordinateSystem = new CoordinateSystem(this.EigenVectors[2], this.EigenVectors[1]);
				var sp:SpaceVector;
				var sp2:SpaceVector;
				var rot:SpaceVector;
				var bodyCs:CoordinateSystem = this.quad[0].Point[0].coordinateSystem;
				var fv:SpaceVector = new SpaceVector(2.5, 0, 0, bodyCs);
				var pos:Coordinate;
				switch (i)
				{
					case 2:
						sp = new SpaceVector(2.5, 0, 0, cs);
						sp2 = new SpaceVector(2.5, 0, 0, cs2);
						this.plotArea.graphics.lineStyle(1, 0xff0000, 1);
						break;
					case 1:
						sp = new SpaceVector(0, 2.5, 0, cs);
						sp2 = new SpaceVector(0, 2.5, 0, cs2);
						this.plotArea.graphics.lineStyle(1, 0x00ff00, 1);
						break;
					case 0:
						sp = new SpaceVector(0, 0, 2.5, cs);
						sp2 = new SpaceVector(0, 0, 2.5, cs2);
						this.plotArea.graphics.lineStyle(1, 0x0000ff, 1);
						break;
					default:
						return;
				}
				rot = VMath.Cross(sp.TransformToBase(), fv.TransformToBase());
				//rot.coordinateSystem = cs;
				var angle:Number = VMath.Angle(fv.TransformToBase(), sp2.TransformToBase());
				var anglePrev:Number;
				pos = this.CoordinateSystem2Parent(sp2.TransformToBase());
				this.plotArea.graphics.moveTo(pos.x, pos.y);
				var dot:Number;
				var angleOrig:Number = VMath.Angle(fv.TransformToBase(), sp.TransformToBase());
				if (!MathEx.IsApproxEqual(VMath.Magnitude(VMath.Cross(fv.TransformToBase(), sp.TransformToBase())), 0, 5))
				{
					do
					{
						anglePrev = angle;
						cs2.Rotate(rot, -0.1);
						angle = VMath.Angle(sp2.TransformToBase(), sp.TransformToBase());
						pos = this.CoordinateSystem2Parent(sp2.TransformToBase());
						dot = VMath.Dot(sp2.TransformToBase(), this.eyePosition.TransformToBase());
						if (angle > angleOrig)
						{
							dot = VMath.Dot(fv.TransformToBase(), this.eyePosition.TransformToBase());
							pos = this.CoordinateSystem2Parent(fv.TransformToBase());
							if (back && dot < 0)
							{
								this.plotArea.graphics.lineTo(pos.x, pos.y);
							}
							else if (!back && dot >= 0)
							{
								this.plotArea.graphics.lineTo(pos.x, pos.y);
							}
							break;
						}
						if (back)
						{
							if (dot >= 0)
							{
								this.plotArea.graphics.moveTo(pos.x, pos.y);
							}
							else
							{
								this.plotArea.graphics.lineTo(pos.x, pos.y);
							}
						}
						else if (!back)
						{
							if (dot < 0)
							{
								this.plotArea.graphics.moveTo(pos.x, pos.y);
							}
							else
							{
								this.plotArea.graphics.lineTo(pos.x, pos.y);
							}
						}
					} while (angle < angleOrig)
					
				}
			}
		}
		private function DrawParticle(p:Particle):void
		{
			var coord:Coordinate = this.CoordinateSystem2Parent(p.position);
			this.plotArea.graphics.lineStyle(1, 0, 0);
			this.plotArea.graphics.beginFill(p.color);
			this.plotArea.graphics.drawCircle(coord.x, coord.y, p.size);
		}
		private function DrawQuadrilateral(q:Quadrilateral,qNum:uint):void
		{
			this.plotArea.graphics.lineStyle(1, 0);
			var coord:Coordinate = this.CoordinateSystem2Parent(q.Point[3]);
			this.plotArea.graphics.moveTo(coord.x, coord.y);
			if (this.BodyIsTransparent)
			{
				if (this.DisplayType2D)
				{
					this.plotArea.graphics.beginFill(0xFFFFFF,0);
				}
				else
				{
					this.plotArea.graphics.beginFill(0xFFFFFF,0.5);
				}
			}
			else
			{
				this.plotArea.graphics.beginFill(0xFFFFFF);
			}
			for (var i:int = 0; i < 4; i++)
			{
				coord = this.CoordinateSystem2Parent(q.Point[i]);
				this.plotArea.graphics.lineTo(coord.x,coord.y)
			}
			this.plotArea.graphics.endFill();
			
			//Add event handler for snap

			
		}
		private function DrawArrow(a:Arrow, drawHead:Boolean = false):void
		{
			//cleared 17 Nov 08 -tbh- BUG okay, now the normal stress arrows are screwed up with the heads
			// they don't flip when stress is compressive
			this.plotArea.graphics.lineStyle(a.Thickness, a.Color,a.Alpha);
			var coord:Coordinate = this.CoordinateSystem2Parent(a.Point[0]);
			this.plotArea.graphics.moveTo(coord.x, coord.y);
			coord = this.CoordinateSystem2Parent(a.Point[1]);
			this.plotArea.graphics.lineTo(coord.x, coord.y);
			if (a.LabelText != "")
			{
				var tfForm:TextFormat = new TextFormat();
				tfForm.font = "TimesNewRomanB";
				tfForm.bold = true;
				tfForm.color = a.Color;
				tfForm.size = 8;
				tfForm.indent = 0;
				tfForm.leftMargin = 0;
				tfForm.rightMargin = 0;
				
				var tf:TextField = new TextField();
				tf.defaultTextFormat = tfForm;
				tf.embedFonts = true;
				tf.text = a.LabelText;
				tf.autoSize = TextFieldAutoSize.CENTER;
				tf.selectable = false;
				//tf.border = true;
				//tf.borderColor = 0;
				
				var svUnity:SpaceVector = VMath.Unity(a.Point[1]);
				svUnity.coordinateSystem = a.Point[1].coordinateSystem;
				var svAdd:SpaceVector = VMath.Add(VMath.ScalarProduct(0.2, svUnity), a.Point[1]);
				svAdd.coordinateSystem = a.Point[1].coordinateSystem;
				coord = this.CoordinateSystem2Parent(svAdd);
				tf.x = coord.x - tf.width / 2;
				tf.y = coord.y - tf.height / 2;
				
				var index:int = this.plotAreaChildren.length
				this.plotAreaChildren[index] = tf;
				if (VMath.Dot(a.Point[1].TransformToBase(), this._eyePosition.TransformToBase()) >= 0)
				{
					this.plotArea.addChild(this.plotAreaChildren[index]);
				}
				else
				{
					this.subPlotArea.addChild(this.plotAreaChildren[index]);
				}
				
			}
			if (drawHead)
			{
				//this.plotArea.graphics.lineStyle(a.Thickness/2,a.Color, a.Alpha);
				//this.plotArea.graphics.beginFill(a.Color, a.Alpha);
				if (a.Invertable)
				{
					//this.plotArea.graphics.beginFill(0, 1);
					coord = this.CoordinateSystem2Parent(a.HeadPoints[0]);
					this.plotArea.graphics.moveTo(coord.x, coord.y);
					coord = this.CoordinateSystem2Parent(a.HeadPoints[1]);
					this.plotArea.graphics.lineTo(coord.x, coord.y);
					coord = this.CoordinateSystem2Parent(a.HeadPoints[2]);
					this.plotArea.graphics.lineTo(coord.x, coord.y);
					coord = this.CoordinateSystem2Parent(a.HeadPoints[0]);
					this.plotArea.graphics.lineTo(coord.x, coord.y);
					//this.plotArea.graphics.endFill();
					
				}
				else
				{
					//trace("hey");
					//normal stress
					var arrowLength:Number = 5;
					var arrowWidth:Number = 3;
					var A:Coordinate;
					var O:Coordinate;
					if (a.Flip)
					{
						O = this.CoordinateSystem2Parent(a.EndPoint);
						A = this.CoordinateSystem2Parent(a.Origin);
					}
					else
					{
						A = this.CoordinateSystem2Parent(a.EndPoint);
						O = this.CoordinateSystem2Parent(a.Origin);
					}
					//trace(!MathEx.IsApproxEqual(A.x, O.x, 5), ":",!MathEx.IsApproxEqual(A.y, O.y, 5));
					if (!MathEx.IsApproxEqual(A.x, O.x, 5) || !MathEx.IsApproxEqual(A.y, O.y, 5))
					{
						//trace("yo");
						var magA:Number = Math.sqrt(Math.pow((A.x - O.x), 2) + Math.pow((A.y - O.y), 2));
						if (magA <= 4 * arrowLength)
						{
							arrowWidth *= magA /(4*arrowLength);
							arrowLength = magA /4;
						}
						var B:Coordinate = new Coordinate((magA-arrowLength) * (A.x - O.x)/magA , (magA-arrowLength) * (A.y - O.y)/magA);
						var magB:Number = Math.sqrt(Math.pow(B.x, 2) + Math.pow(B.y, 2));
						var D:Coordinate = new Coordinate((B.x - arrowWidth * B.y/magB) + O.x, (B.y + arrowWidth * B.x/magB) + O.y);
						var C:Coordinate = new Coordinate((B.x + arrowWidth * B.y/magB) + O.x, (B.y - arrowWidth * B.x/magB) + O.y);
						
						this.plotArea.graphics.moveTo(A.x, A.y);
						this.plotArea.graphics.lineTo(C.x, C.y);
						this.plotArea.graphics.moveTo(A.x, A.y);
						this.plotArea.graphics.lineTo(D.x, D.y);
						//this.plotArea.graphics.lineTo(A.x, A.y);
					}
					//this.plotArea.graphics.endFill();
				}
			}
			//trace(a.Point[0]);
		}
		private function Grid2CoordinateSystem(x:Number, y:Number):SpaceVector
		{
			var resultant:SpaceVector = VMath.Add(VMath.ScalarProduct(x, this.viewX.TransformToBase()), VMath.ScalarProduct(y, this.viewY.TransformToBase()));
			return resultant;
		}
		private function CoordinateSystem2Grid(v:SpaceVector):SpaceVector
		{
			var resultant:SpaceVector = new SpaceVector();
			resultant.x = VMath.Dot(this.eyePosition.TransformToBase(),this.viewX.TransformToBase()) + VMath.Dot(v, this.viewX.TransformToBase());
			resultant.y = VMath.Dot(this.eyePosition.TransformToBase(),this.viewY.TransformToBase()) + VMath.Dot(v, this.viewY.TransformToBase());
			return resultant;
		}
		public function CoordinateSystem2Parent(v:SpaceVector):Coordinate
		{
			var vector:SpaceVector = this.CoordinateSystem2Grid(v.TransformToBase());
			return grid.Grid2Parent(vector.x, vector.y);
			
		}
		public function AddBody(b:Body):void
		{
			var newBody:Array = new Array(body.length + 1);
			for (var i:int = 0; i < body.length; i++)
			{
				newBody[i] = body[i];
			}
			newBody[newBody.length] = b;
			body = newBody;
		}
		public function AddParticle(p:Particle):void
		{
			var newPoint:Array = new Array(particle.length);
			for (var i:int = 0; i < particle.length; i++)
			{
				newPoint[i] = particle[i];
			}
			newPoint[newPoint.length] = p;
			particle = newPoint;
		}
		public function AddQuadrilateral(q:Quadrilateral):void
		{
			var newQuad:Array = new Array(quad.length);
			for (var i:int = 0; i < quad.length; i++)
			{
				newQuad[i] = quad[i];
			}
			newQuad[newQuad.length] = q;
			quad = newQuad;
		}
		public function AddForceArrow(a:Arrow):void
		{
			var newForceArrow:Array = new Array(forceArrow.length);
			for (var i:int = 0; i < forceArrow.length; i++)
			{
				newForceArrow[i] = forceArrow[i];
			}
			newForceArrow[newForceArrow.length] = a;
			forceArrow = newForceArrow;
		}
		public function AddResultantArrow(a:Arrow):void
		{
			var newResArrow:Array = new Array(resultantArrow.length);
			for (var i:int = 0; i < resultantArrow.length; i++)
			{
				newResArrow[i] = resultantArrow[i];
			}
			newResArrow[newResArrow.length] = a;
			resultantArrow = newResArrow;
		}
		public function AddRefArrow(a:Arrow):void
		{
			var newRefArrow:Array = new Array(refArrow.length);
			for (var i:int = 0; i < refArrow.length; i++)
			{
				newRefArrow[i] = refArrow[i];
			}
			newRefArrow[newRefArrow.length] = a;
			refArrow = newRefArrow;
		}
		public function AddEigenArrow(a:Arrow):void
		{
			var newEigenArrow:Array = new Array(EigenArrow.length);
			for (var i:int = 0; i < EigenArrow.length; i++)
			{
				newEigenArrow[i] = EigenArrow[i];
			}
			newEigenArrow[newEigenArrow.length] = a;
			EigenArrow = newEigenArrow;
		}
		public function AddBodyArrow(a:Arrow):void
		{
			var newBodyArrow:Array = new Array(bodyArrow.length);
			for (var i:int = 0; i < bodyArrow.length; i++)
			{
				newBodyArrow[i] = bodyArrow[i];
			}
			newBodyArrow[newBodyArrow.length] = a;
			bodyArrow = newBodyArrow;
		}
		public function set aHeight(setValue:int):void
		{
			this._aHeight = setValue;
		}
		public function get aHeight():int
		{
			return this._aHeight;
		}
		public function set aWidth(setValue:int):void
		{
			this._aWidth = setValue;
		}
		public function get aWidth():int
		{
			return this._aWidth;
		}
		public function DrawBorder(thickness:uint = 2, color:uint = 0, alpha:Number = 1):void
		{
			this.graphics.lineStyle(thickness, color, alpha);
			this.graphics.drawRect(thickness / 2, thickness / 2, this.aWidth - thickness, this.aHeight - thickness);
		}
		private function Number2DisplayText(n:Number):String
		{
			var txt:String;
			if (n < 0)
			{
				n = MathEx.RoundDec(n, 3);
				txt = String(n.toFixed(5)).substr(0,6);
			}
			else
			{
				n = MathEx.RoundDec(n, 3);
				txt = String(n.toFixed(6)).substr(0,5);
			}
			//if ( n == 0)
			//{
				//txt = "0";
			//}
			return txt;
		}
		
	}
	
}
