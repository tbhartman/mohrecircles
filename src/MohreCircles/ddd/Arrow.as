﻿package MohreCircles.ddd
{
	import flash.geom.Point;
	
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class Arrow extends Surface
	{
		private var flipped:Boolean = false;
		private var origin:SpaceVector = new SpaceVector;
		public function get Origin():SpaceVector
		{
			return origin;
		}
		private var unitDirection:SpaceVector;
		private var invertable:Boolean;
		public function get Invertable():Boolean
		{
			return this.invertable;
		}
		private var points:Array = new Array(2);
		private var headPoints:Array = new Array(3);
		public function get HeadPoints():Array
		{
			return this.headPoints;
		}
		public function get Point():Array
		{
			return this.points;
		}
		private var color:uint;
		public function get Color():uint
		{
			return this.color;
		}
		private var thickness:uint;
		public function get Thickness():uint
		{
			return this.thickness;
		}
		private var alpha:Number;
		public function get Alpha():Number
		{
			return this.alpha;
		}
		public function set Alpha(a:Number):void
		{
			this.alpha = a;
		}
		private var labelText:String;
		public function get LabelText():String
		{
			return this.labelText;
		}
		public function set LabelText(t:String):void
		{
			this.labelText = t;
		}
		
		
		
		public function Arrow(point1:SpaceVector,point2:SpaceVector,invertable:Boolean=true,color:uint=0x0,thickness:uint=1,alpha:Number = 1,text:String = ""):void
		{
			this.origin = point1;
			this.points[0] = point1;
			this.points[1] = point2;
			this.color = color;
			this.thickness = thickness;
			this.invertable = invertable;
			this.alpha = alpha;
			this.LabelText = text;
			this.headPoints[0] = this.points[1];
			this.headPoints[1] = this.points[1];
			this.headPoints[2] = this.points[1];
			
			this.SetDirection();
		}
		public function Reset(newOrigin:SpaceVector,relSP:SpaceVector):void
		{
			this.origin = newOrigin;
			this.points[0] = newOrigin;
			this.EndPoint = relSP;
			this.SetDirection();
		}
		private function SetDirection():void
		{
			var direction:SpaceVector = VMath.Subtract(this.points[1], this.points[0]);
			var tmpV:SpaceVector = VMath.ScalarQuotient(direction, VMath.Magnitude(direction));
			this.unitDirection = new SpaceVector(tmpV.x, tmpV.y, tmpV.z, this.origin.coordinateSystem);
			
		}
		public function get EndPoint():SpaceVector
		{
			if (this.points[1] == this.origin)
			{
				return this.points[0];
			}
			else
			{
				return this.points[1];
			}
		}
		public function set EndPoint(rel2orig:SpaceVector):void
		{
			var tmp:SpaceVector = VMath.Add(this.origin, rel2orig);
			tmp.coordinateSystem = this.origin.coordinateSystem;
			this.points[1] = tmp;
		}
		public function set Flip(b:Boolean):void
		{
			this.flipped = b;
		}
		public function get Flip():Boolean
		{
			return this.flipped;
		}
		public function get Vector():SpaceVector
		{
			var tmp:SpaceVector = VMath.Add(this.points[1], VMath.ScalarProduct( -1, this.points[0]));
			tmp.coordinateSystem = this.origin.coordinateSystem;
			return tmp;
		}
		public function SetMagnitude(m:Number):void
		{
			//trace(this.unitDirection);
			var resultant:SpaceVector;
			var tmpV:SpaceVector;
			if (this.invertable)
			{
				resultant = VMath.ScalarProduct(m, this.unitDirection);
				tmpV = VMath.Add(this.origin, resultant);
				this.points[1] = new SpaceVector(tmpV.x, tmpV.y, tmpV.z, this.origin.coordinateSystem);
			}
			else
			{
				resultant = VMath.ScalarProduct(Math.abs(m), this.unitDirection);
				tmpV = VMath.Add(this.origin, resultant);
				//if (m < 0)
				//{
					//this.points[0] = new SpaceVector(tmpV.x, tmpV.y, tmpV.z, this.origin.coordinateSystem);
					//this.points[1] = this.origin;
				//}
				//else
				//{
					//this.points[0] = this.origin;
					//this.points[1] = new SpaceVector(tmpV.x, tmpV.y, tmpV.z, this.origin.coordinateSystem);
				//}
				this.Flip = false;
				this.points[0] = this.origin;
				this.points[1] = new SpaceVector(tmpV.x, tmpV.y, tmpV.z, this.origin.coordinateSystem);
				if (m < 0)
				{
					this.Flip = true;
				}
			}
			//now make arrowhead
			this.MakeHead();
		}
		private function MakeHead():void
		{
			var arrowHeadLength:Number = 0.1; //coord system length
			var arrowHeadWidth:Number = 0.06; //coord system length
			if (this.invertable)
			{
				//shear stress
				
				//find relative vector
				var relVec:SpaceVector = VMath.Subtract(this.points[1], this.points[0]);
				relVec = VMath.Subtract(relVec, VMath.ScalarProduct(arrowHeadLength / VMath.Magnitude(relVec), relVec));
				this.headPoints[0] = VMath.Add(this.points[0], relVec);
				this.headPoints[1] = this.points[1];
				this.headPoints[2] = VMath.Add(this.headPoints[0], VMath.ScalarProduct(arrowHeadWidth / VMath.Magnitude(this.origin), this.origin));
				
				if (VMath.Magnitude(this.Vector) <= arrowHeadLength)
				{
					relVec = VMath.ScalarProduct(VMath.Magnitude(this.Vector) / (arrowHeadLength*7), relVec);
					this.headPoints[0] = VMath.Add(this.points[0], relVec);
					this.headPoints[1] = this.points[1];
					this.headPoints[2] = VMath.Add(this.headPoints[0], VMath.ScalarProduct(arrowHeadWidth / VMath.Magnitude(this.origin), this.origin));
				}
				if (VMath.Magnitude(this.Vector) <= arrowHeadLength/3)
				{
					this.headPoints[0] = this.points[1];
					this.headPoints[1] = this.points[1];
					this.headPoints[2] = this.points[1];
				}
			}
			else
			{
				//normal stress
				
			}
			this.headPoints[0].coordinateSystem = this.origin.coordinateSystem;
			this.headPoints[1].coordinateSystem = this.origin.coordinateSystem;
			this.headPoints[2].coordinateSystem = this.origin.coordinateSystem;
			
		}
		
	}
	
}
