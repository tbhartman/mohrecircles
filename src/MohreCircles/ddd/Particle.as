﻿package MohreCircles.ddd
{
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class Particle
	{
		private var _position:SpaceVector;
		public function set position(setValue:SpaceVector):void
		{
			this._position = setValue;
		}
		public function get position():SpaceVector
		{
			return this._position;
		}
		private var _color:uint;
		public function set color(setValue:uint):void
		{
			this._color = setValue;
		}
		public function get color():uint
		{
			return this._color;
		}
		private var _size:uint;
		public function set size(setValue:uint):void
		{
			this._size = setValue;
		}
		public function get size():uint
		{
			return this._size;
		}
		
		public function Particle(v:SpaceVector, color:uint = 0, size:uint = 2):void
		{
			this.position = v;
			this.color = color;
			this.size = size;
		}
		
	}
	
}
