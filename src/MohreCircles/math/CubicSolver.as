﻿package MohreCircles.math
{
	
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class CubicSolver
	{
		/**
		 * Finds the roots of a cubic equation for all real coefficients.
		 * @param	a
		 * @param	b
		 * @param	c
		 * @param	d
		 * @return
		 */
		public function CubicSolver()
		{
			
		}
		public static function Roots(a:Number,b:Number,c:Number,d:Number,e:Number = 0.000000000001):Array
		{
			var prime_root_plus:Number = ( -2 * b + Math.sqrt(4 * Math.pow(b, 2) - 12 * a * c)) / (6 * a);
			var prime_root_minus:Number = ( -2 * b - Math.sqrt(4 * Math.pow(b, 2) - 12 * a * c)) / (6 * a);
			var inflection:Number = -(b / (3 * a));
			
			var guess1:Number = prime_root_minus + (prime_root_minus - inflection);
			var guess2:Number = inflection;
			var guess3:Number = prime_root_plus - (inflection - prime_root_plus);
			
			var zero1:Number = CubicSolver.SolverIterater(guess1, a, b, c, d, e);
			var zero2:Number = CubicSolver.SolverIterater(guess2, a, b, c, d, e);
			var zero3:Number = CubicSolver.SolverIterater(guess3, a, b, c, d, e);
			
			//trace(zero1,zero2, zero3);
			//trace("*");
			//trace(a, b, c, d);
			//trace(zero1, zero2, zero3);
			return new Array(zero1, zero2, zero3);
			
		}
		
		public static function SolverIterater(guess:Number, a:Number, b:Number, c:Number, d:Number, req_diff:Number):Number
		{
			var Xn:Number = guess;
			var Yn1:Number = new Number(); //y-value of Xn-1
			var Dn1:Number = new Number(); //slope of y at Xn-1
			var Xn1:Number = new Number(); //Xn-1
			var Xn2:Number = new Number(); //Xn-2
			var diff:Number = new Number();
			var count:int = 0;
			
			do
			{
				if (count > 50)
				{
					break;
				}
				Xn2 = Xn1;
				Xn1 = Xn;
				Dn1 = 3 * a * Math.pow(Xn1, 2) + 2 * b * Xn1 + c;
				Yn1 = a * Math.pow(Xn1, 3) + b * Math.pow(Xn1, 2) + c * Xn1 + d;
				//break if guess is right on
				if (Math.abs(Yn1) <= req_diff)
				{
					//trace(Yn1);
					break;
				}
				Xn = Xn1 - (Yn1 / Dn1);
				count++;
			} while (Math.abs(Xn - Xn1) > req_diff);
			
			return Xn;
		}
		
	}
	
}
