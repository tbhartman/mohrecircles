﻿package MohreCircles.math
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class Tensor extends EventDispatcher
	{
		private var components:Array = new Array(9);
		
		private var changeEvent:TensorEvent;
		
		//Sample variables
		private var number:Number = new Number();
		
		public function Tensor()
		{
			for (var i:int = 0; i < this.components.length; i++)
			{
				this.components[i] = new Number();
			}
		}
		
		public function getComponent(i:int, j:int):Number
		{
			return this.components[Tensor.IJ2Num(i, j)];
		}
		public function setComponent(i:int, j:int, value:Number):void
		{
			var oldValue:Number = this.components[Tensor.IJ2Num(i, j)];
			if (oldValue != value)
			{
				this.components[Tensor.IJ2Num(i, j)] = value;
				changeEvent = new TensorEvent(TensorEvent.VALUE_CHANGE);
				changeEvent.i = i;
				changeEvent.j = j;
				changeEvent.value = value;
				this.dispatchEvent(changeEvent);
			}
		}
		public static function Num2I(n:Number):int
		{
			return Math.floor(n / 3) + 1;
		}
		public static function Num2J(n:Number):int
		{
			return (n % 3) + 1;
		}
		public static function IJ2Num(i:int, j:int):int
		{
			var result:int = (i - 1) * 3 + (j - 1);
			return result;
		}
		public override function toString():String
		{
			var s1:String = "{" + this.getComponent(1, 1) + "," + this.getComponent(1, 2) + "," + this.getComponent(1, 3) + "}";
			var s2:String = "{" + this.getComponent(2, 1) + "," + this.getComponent(2, 2) + "," + this.getComponent(2, 3) + "}";
			var s3:String = "{" + this.getComponent(3, 1) + "," + this.getComponent(3, 2) + "," + this.getComponent(3, 3) + "}";
			return ("{" + s1 + s2 + s3 + "}");
		}
		public function GetMaxAbsDiagonal():Number
		{
			return Math.max(Math.abs(this.getComponent(1, 1)), Math.abs(this.getComponent(2, 2)), Math.abs(this.getComponent(3, 3)));
		}
		public function GetMaxAbsOffDiagonal():Number
		{
			return Math.max(Math.abs(this.getComponent(1, 2)), Math.abs(this.getComponent(1, 3)), Math.abs(this.getComponent(2, 1))
				,Math.abs(this.getComponent(2, 3)), Math.abs(this.getComponent(3, 1)), Math.abs(this.getComponent(3, 2)));
		}
		public function GetMaxAbsComponent():Number
		{
			return Math.max(Math.abs(this.GetMaxAbsDiagonal()), Math.abs(this.GetMaxAbsOffDiagonal()));
		}
	}
	
}
