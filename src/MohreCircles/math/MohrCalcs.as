﻿package MohreCircles.math
{
	import MohreCircles.ddd.DirCos;
	import MohreCircles.ddd.SpaceVector;
	import MohreCircles.ddd.VMath;
	import MohreCircles.MathEx;
	
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class MohrCalcs
	{
		private var inputTensor:Tensor = new Tensor();
		
		public var sigma:Array = new Array();
		public var eigenVectors:Array = new Array();
		public var radii:Array = new Array(3);
		public var centers:Array = new Array(3);
		
		private var alphaTensor:Tensor = new Tensor();
		public function get AlphaTensor():Tensor
		{
			return this.alphaTensor;
		}
		
		public function MohrCalcs()
		{
			
		}
		public function setTensor(t:Tensor):void
		{
			this.inputTensor = t;
			this.Update();
		}
		public function Update():void
		{
			//Calculation of Principal Stresses
			var xx:Number = inputTensor.getComponent(1, 1);
			var xy:Number = inputTensor.getComponent(1, 2);
			var xz:Number = inputTensor.getComponent(1, 3);
			var yy:Number = inputTensor.getComponent(2, 2);
			var yz:Number = inputTensor.getComponent(2, 3);
			var zz:Number = inputTensor.getComponent(3, 3);
			
			
			var I1:Number = xx + yy + zz;
			var I2:Number = (xx * yy) + (xx * zz) + (yy * zz) - Math.pow(xy, 2) - Math.pow(xz, 2) - Math.pow(yz, 2);
			var I3:Number = xx * yy * zz - xx * Math.pow(yz, 2) - Math.pow(xz, 2) * yy - Math.pow(xy, 2) * zz + 2 * xy * xz * yz;
			
			this.sigma = CubicSolver.Roots(1, -I1, I2, -I3);
			this.GetEigenVectors();
			//trace("--");
			//trace(I1, I2, I3);
			//trace(sigma[0], sigma[1], sigma[2]);
			//trace("*-- Input Tensor --*");
			//trace("|", xx, xy, xz, "|");
			//trace("|", xy, yy, yz, "|");
			//trace("|", xz, yz, zz, "|");
			//trace("Sigma 0: ", this.sigma[0]);
			//trace("Sigma 1: ", this.sigma[1]);
			//trace("Sigma 2: ", this.sigma[2]);
			
			for (var i:int = 0; i < radii.length; i++)
			{
				switch (i)
				{
					case 0: //Radius of Circle with Principal Stresses 1,2
						this.radii[i] = (this.sigma[1] - this.sigma[0]) / 2;
						break;
					case 1: //Radius of Circle with Principal Stresses 1,3
						this.radii[i] = (this.sigma[2] - this.sigma[0]) / 2;
						break;
					case 2: //Radius of Circle with Principal Stresses 2,3
						this.radii[i] = (this.sigma[2] - this.sigma[1]) / 2;
						break;
					default:
						break;
				}
				
			}
			
			for (var j:int = 0; j < centers.length; j++)
			{
				switch (j)
				{
					case 0: //Center of Circle with Principal Stresses 1,2
						this.centers[j] = this.sigma[0] + this.radii[0];
						break;
					case 1: //Center of Circle with Principal Stresses 1,3
						this.centers[j] = this.sigma[0] + this.radii[1];
						break;
					case 2: //Center of Circle with Principal Stresses 2,3
						this.centers[j] = this.sigma[1] + this.radii[2];
						break;
					default:
						break;
				}
			}
			
		}
		public function Transformation(xAxis:SpaceVector, yAxis:SpaceVector, zAxis:SpaceVector):Tensor
		{
			var xx:Number = inputTensor.getComponent(1, 1);
			var xy:Number = inputTensor.getComponent(1, 2);
			var xz:Number = inputTensor.getComponent(1, 3);
			var yy:Number = inputTensor.getComponent(2, 2);
			var yz:Number = inputTensor.getComponent(2, 3);
			var zz:Number = inputTensor.getComponent(3, 3);
			
			var dirCos:DirCos = new DirCos(xAxis);
			var L1:Number = dirCos.X;
			var M1:Number = dirCos.Y;
			var N1:Number = dirCos.Z;
			dirCos = new DirCos(yAxis);
			var L2:Number = dirCos.X;
			var M2:Number = dirCos.Y;
			var N2:Number = dirCos.Z;
			dirCos = new DirCos(zAxis);
			var L3:Number = dirCos.X;
			var M3:Number = dirCos.Y;
			var N3:Number = dirCos.Z;
			
			this.alphaTensor.setComponent(1, 1, L1);
			this.alphaTensor.setComponent(1, 2, M1);
			this.alphaTensor.setComponent(1, 3, N1);
			this.alphaTensor.setComponent(2, 1, L2);
			this.alphaTensor.setComponent(2, 2, M2);
			this.alphaTensor.setComponent(2, 3, N2);
			this.alphaTensor.setComponent(3, 1, L3);
			this.alphaTensor.setComponent(3, 2, M3);
			this.alphaTensor.setComponent(3, 3, N3);
			//trace("**------------**");
			//trace("|", L1, M1, N1, "|");
			//trace("|", L2, M2, N2, "|");
			//trace("|", L3, M3, N3, "|");
			//trace(" ");
			
			var trans:Tensor = new Tensor();
			trans.setComponent(1, 1, (L1 * L1 * xx) + (M1 * M1 * yy) + (N1 * N1 * zz)
				+ ((L1 * M1 + L1 * M1) * xy) + ((L1 * N1 + L1 * N1) * xz) + ((M1 * N1 + M1 * N1) * yz));
			trans.setComponent(1, 2, (L1 * L2 * xx) + (M1 * M2 * yy) + (N1 * N2 * zz)
				+ ((L1 * M2 + L2 * M1) * xy) + ((L1 * N2 + L2 * N1) * xz) + ((M1 * N2 + M2 * N1) * yz));
			trans.setComponent(1, 3, (L1 * L3 * xx) + (M1 * M3 * yy) + (N1 * N3 * zz)
				+ ((L1 * M3 + L3 * M1) * xy) + ((L1 * N3 + L3 * N1) * xz) + ((M1 * N3 + M3 * N1) * yz));
			trans.setComponent(2, 1, trans.getComponent(1, 2));
			trans.setComponent(2, 2, (L2 * L2 * xx) + (M2 * M2 * yy) + (N2 * N2 * zz)
				+ ((L2 * M2 + L2 * M2) * xy) + ((L2 * N2 + L2 * N2) * xz) + ((M2 * N2 + M2 * N2) * yz));
			trans.setComponent(2, 3, (L2 * L3 * xx) + (M2 * M3 * yy) + (N2 * N3 * zz)
				+ ((L2 * M3 + L3 * M2) * xy) + ((L2 * N3 + L3 * N2) * xz) + ((M2 * N3 + M3 * N2) * yz));
			trans.setComponent(3, 1, trans.getComponent(1, 3));
			trans.setComponent(3, 2, trans.getComponent(2, 3));
			trans.setComponent(3, 3, (L3 * L3 * xx) + (M3 * M3 * yy) + (N3 * N3 * zz)
				+ ((L3 * M3 + L3 * M3) * xy) + ((L3 * N3 + L3 * N3) * xz) + ((M3 * N3 + M3 * N3) * yz));
				
			return trans;
		}
		private function GetEigenVectors():void
		{
			var t11:Number = inputTensor.getComponent(1, 1);
			var t12:Number = inputTensor.getComponent(1, 2);
			var t13:Number = inputTensor.getComponent(1, 3);
			var t21:Number = inputTensor.getComponent(2, 1);
			var t22:Number = inputTensor.getComponent(2, 2);
			var t23:Number = inputTensor.getComponent(2, 3);
			var t31:Number = inputTensor.getComponent(3, 1);
			var t32:Number = inputTensor.getComponent(3, 2);
			var t33:Number = inputTensor.getComponent(3, 3);
			
			var t11_lambda:Number;
			var t22_lambda:Number;
			var t33_lambda:Number;
			
			var v1:Number;
			var v2:Number;
			var v3:Number;
			
			var d:Number;
			var q:Number;
			var r:Number;
			var a:Number;
			var b:Number;
			var c:Number;
			
			//trace("*--*");
			if ((t12 == 0 && t13 == 0) || (t12 == 0 && t23 == 0) || (t13 == 0 && t23 == 0))
			{
				if ((t12 == 0 && t13 == 0) && (t12 == 0 && t23 == 0))// && (t13 == 0 && t23 == 0))
				{	//three base axes are eigenvectors
					//trace("all three");
					this.eigenVectors[2] = new SpaceVector(1, 0, 0);
					this.eigenVectors[1] = new SpaceVector(0, 1, 0);
					this.eigenVectors[0] = new SpaceVector(0, 0, 1);
				}
				else
				{	//one or more eigenvectors is in base axis direction
					
					var aug11:Number;
					var aug12:Number;
					var aug21:Number;
					var aug22:Number;
					var sig:Number;
					var axis_stress:Number;
					
					if (t12 == 0 && t13 == 0)
					{
						//trace("Xaxis");
						//X-axis is an eigenvector
						/* X-axis is an eigenvector...must find eigenvectors corresponding
						 * to eigenvalues not in x-axis direction
						 */
						axis_stress = inputTensor.getComponent(1, 1);
						if (axis_stress != this.sigma[2])
						{
							sig = this.sigma[2];
						}
						else if (axis_stress != this.sigma[1])
						{
							sig = this.sigma[1];
						}
						else
						{
							sig = this.sigma[3];
						}
						aug11 = t22 - sig;
						aug12 = t23;
						aug21 = t32;
						aug22 = t33 - sig;
					}
					else if (t12 == 0 && t23 == 0)
					{
						//trace("Yaxis");
						axis_stress = inputTensor.getComponent(2, 2);
						if (axis_stress != this.sigma[2])
						{
							sig = this.sigma[2];
						}
						else if (axis_stress != this.sigma[1])
						{
							sig = this.sigma[1];
						}
						else
						{
							sig = this.sigma[3];
						}
						aug11 = t11 - this.sigma[2];
						aug12 = t13;
						aug21 = t31;
						aug22 = t33 - this.sigma[2];
					}
					else //if (t13 == 0 && t23 == 0)
					{
						//trace("Zaxis");
						axis_stress = inputTensor.getComponent(3, 3);
						if (axis_stress != this.sigma[2])
						{
							sig = this.sigma[2];
						}
						else if (axis_stress != this.sigma[1])
						{
							sig = this.sigma[1];
						}
						else
						{
							sig = this.sigma[3];
						}
						aug11 = t11 - this.sigma[2];
						aug12 = t12;
						aug21 = t21;
						aug22 = t22 - this.sigma[2];
					}
					// a*x^2 + b*x + c = 0
					// Singularity when aug11 == 0
					if (aug11 == 0)
					{
						aug11 += 0.00001;
					}
					a = 1 + Math.pow(aug12 / aug11, 2);
					b = aug22 - (aug21 * aug12) / aug11;
					c = -1;
					
					v2 = ( -b + Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / (2 * a);
					if (Math.abs(v2) > 1)
					{
						v2 = ( -b - Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / (2 * a);
					}
					v1 = -aug12 / aug11 * v2;
					//trace("*--*");
					//trace("0:", aug11, aug12);
					//trace("0:", aug21, aug22);
					//trace("v1: ", v1);
					//trace("v2: ", v2);
					
					
					if (t12 == 0 && t13 == 0)
					{
						this.eigenVectors[2] = new SpaceVector(1, 0, 0);
						this.eigenVectors[1] = new SpaceVector(0, v1, v2);
						this.eigenVectors[0] = VMath.Cross(this.eigenVectors[2], this.eigenVectors[1]);
					}
					else if (t12 == 0 && t23 == 0)
					{
						this.eigenVectors[2] = new SpaceVector(v1, 0, v2);
						this.eigenVectors[1] = new SpaceVector(0, 1, 0);
						this.eigenVectors[0] = VMath.Cross(this.eigenVectors[2], this.eigenVectors[1]);
					}
					else //if (t13 == 0 && t23 == 0)
					{
						this.eigenVectors[2] = new SpaceVector(v1, v2, 0);
						this.eigenVectors[0] = new SpaceVector(0, 0, 1);
						this.eigenVectors[1] = VMath.Cross(this.eigenVectors[0], this.eigenVectors[2]);
					}
				}
			}
			else
			{	// no base axes are eigenvectors
				//trace("none");
				for (var i:int = 0; i < 3; i++)
				{
					//subtract eigenvalue
					t11_lambda = t11 - this.sigma[i];
					t22_lambda = t22 - this.sigma[i];
					t33_lambda = t33 - this.sigma[i];
					
					if (t11_lambda == 0)
					{
						t11_lambda += 0.00001;
					}
					if (t22_lambda == 0)
					{
						t22_lambda += 0.00001;
					}
					if (t33_lambda == 0)
					{
						t33_lambda += 0.00001;
					}
					
					//coefficients to find components
					d = ( -t23*t11_lambda + (t21 * t13))/(t22_lambda*t11_lambda - (t21 * t12));
					q = -(t12 * d) / t11_lambda;
					r = -t13 / t11_lambda;
					a = 1 + Math.pow(d, 2) + (Math.pow(q, 2) + q * r + Math.pow(r, 2));
					b = (t33_lambda + (t32 * d) - ((t31 * t13)/t11_lambda) - ((t31*t12*d)/t11_lambda))/t33_lambda;
					c = -1;
					//components
					//v3 = ( -b + Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / (2 * a);
					//if ( Math.abs(v3) > 1)
					//{
						v3 = ( -b - Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / (2 * a);
					//}
					v2 = d * v3;
					//trace(d);
					v1 = ( -(t12 / t11_lambda) * (v2))-((t13/t11_lambda) * v3);
					
					
					this.eigenVectors[i] = new SpaceVector(v1, v2, v3);
				}
				
			}
			//trace("*--2*");
			//trace(this.eigenVectors[2]);
			//trace(this.eigenVectors[1]);
			//trace(this.eigenVectors[0]);
			
			//Now check for eigenvectors match with eigenvalues
			
			/* now, i'm having problems with a crazy eigenvector. i'm going to check if
			 * two vectors are perpendicular.  if they are, i'll just calculate the third
			 * one
			 */
			if (VMath.IsOrtho(this.eigenVectors[2],this.eigenVectors[1]))
			{
				//trace(1);
				this.eigenVectors[0] = VMath.Cross(this.eigenVectors[2], this.eigenVectors[1]);
			}
			else if (VMath.IsOrtho(this.eigenVectors[0],this.eigenVectors[2]))
			{
				//trace(2);
				this.eigenVectors[1] = VMath.Cross(this.eigenVectors[0], this.eigenVectors[2]);
			}
			else if (VMath.IsOrtho(this.eigenVectors[1],this.eigenVectors[0]))
			{
				//trace(3);
				this.eigenVectors[2] = VMath.Cross(this.eigenVectors[1], this.eigenVectors[0]);
			}
			var okay:Boolean = false;
			var escapeCount:int = 0;
			var precisionRequired:int = 5;
			do
			{
				escapeCount++;
				if (escapeCount > 10)
				{
					okay = true;
					continue;
				}
				var oldEV0:SpaceVector = this.eigenVectors[0];
				var oldEV1:SpaceVector = this.eigenVectors[1];
				var oldEV2:SpaceVector = this.eigenVectors[2];
				
				var check01:Number = (t11 - this.sigma[0]) * oldEV0.x + t12 * oldEV0.y + t13 * oldEV0.z;
				var check02:Number = t21 * oldEV0.x + (t22 - this.sigma[0]) * oldEV0.y + t23 * oldEV0.z;
				var check03:Number = t31 * oldEV0.x + t32 * oldEV0.y + (t33 - this.sigma[0]) * oldEV0.z;
				var check11:Number = (t11 - this.sigma[1]) * oldEV1.x + t12 * oldEV1.y + t13 * oldEV1.z;
				var check12:Number = t21 * oldEV1.x + (t22 - this.sigma[1]) * oldEV1.y + t23 * oldEV1.z;
				var check13:Number = t31 * oldEV1.x + t32 * oldEV1.y + (t33 - this.sigma[1]) * oldEV1.z;
				var check21:Number = (t11 - this.sigma[2]) * oldEV2.x + t12 * oldEV2.y + t13 * oldEV2.z;
				var check22:Number = t21 * oldEV2.x + (t22 - this.sigma[2]) * oldEV2.y + t23 * oldEV2.z;
				var check23:Number = t31 * oldEV2.x + t32 * oldEV2.y + (t33 - this.sigma[2]) * oldEV2.z;
				//trace("---");
				//trace("0:", check01);
				//trace("1:", check02);
				//trace("2:", check03);
				//trace("0:", check11);
				//trace("1:", check12);
				//trace("2:", check13);
				//trace("0:", check21);
				//trace("1:", check22);
				//trace("2:", check23);
				
				if (MathEx.RoundDec(check01,precisionRequired) != 0 ||
					MathEx.RoundDec(check02,precisionRequired) != 0 ||
					MathEx.RoundDec(check03,precisionRequired) != 0 )
				{

					this.eigenVectors[2] = oldEV0;
					this.eigenVectors[1] = oldEV2;
					this.eigenVectors[0] = oldEV1;
					continue;
				}
				//At this point eigenvector 0 should be good!
				if (MathEx.RoundDec(check11,precisionRequired) != 0 ||
					MathEx.RoundDec(check12,precisionRequired) != 0 ||
					MathEx.RoundDec(check13,precisionRequired) != 0 )
				{
					//1 and 2 need to flip
					this.eigenVectors[1] = oldEV2;
					this.eigenVectors[2] = oldEV1;
					continue;
				}
				okay = true;
				
			} while (!okay)
			//trace("*--*");
			//trace(this.eigenVectors[2]);
			//trace(this.eigenVectors[1]);
			//trace(this.eigenVectors[0]);
			
		}
		//private function GetEigenVectors():void
		//{
			//var t11:Number = inputTensor.getComponent(1, 1);
			//var t12:Number = inputTensor.getComponent(1, 2);
			//var t13:Number = inputTensor.getComponent(1, 3);
			//var t21:Number = inputTensor.getComponent(2, 1);
			//var t22:Number = inputTensor.getComponent(2, 2);
			//var t23:Number = inputTensor.getComponent(2, 3);
			//var t31:Number = inputTensor.getComponent(3, 1);
			//var t32:Number = inputTensor.getComponent(3, 2);
			//var t33:Number = inputTensor.getComponent(3, 3);
			//
			//var vectors:Array = new Array(3);
			//vectors[0] = new SpaceVector(1, 0, 0);
			//vectors[1] = new SpaceVector(0, 1, 0);
			//vectors[2] = new SpaceVector(0, 0, 1);
			//
			//for (var i:int = 0; i < 3; i++)
			//{
				//
				//var row:Array = new Array(3);
				//
				//do
				//{
					//for (var j:int = 0; j < 3; j++)
					//{
						//row[i] = inputTensor.getComponent(i, 1) * vectors[i].x + inputTensor.getComponent(i, 2) * vectors[i].y + inputTensor.getComponent(i, 3) * vectors[i].z;
						//
						//
						//if(row[i]
					//
					//}
					//
				//} while (
				//
			//}
			//
			//var offset:Number = 0.00001;
			//var round:int = 6;
			//if (t11 == MathEx.RoundDec(this.sigma[0],round) ||
				//t11 == MathEx.RoundDec(this.sigma[1],round) ||
				//t11 == MathEx.RoundDec(this.sigma[2],round))
				//{
					//t12 += offset;
					//t21 += offset;
				//}
			//if (t22 == MathEx.RoundDec(this.sigma[0],round) ||
				//t22 == MathEx.RoundDec(this.sigma[1],round) ||
				//t22 == MathEx.RoundDec(this.sigma[2],round))
				//{
					//t12 += offset;
					//t21 += offset;
				//}
			//if (t33 == MathEx.RoundDec(this.sigma[0],round) ||
				//t33 == MathEx.RoundDec(this.sigma[1],round) ||
				//t33 == MathEx.RoundDec(this.sigma[2],round))
				//{
					//t13 += offset;
					//t31 += offset;
				//}
			//
			//for (var i:int = 0; i < 3; i++)
			//{
				//subtract eigenvalue
				//t11 -= this.sigma[i];
				//t22 -= this.sigma[i];
				//t33 -= this.sigma[i];
				//
				//
				//coefficients to find components
				//d = ( -t23*t11 + (t21 * t13))/(t22*t11 - (t21 * t12));
				//q = -(t12 * d) / t11;
				//r = -t13 / t11;
				//a = 1 + Math.pow(d, 2) + (Math.pow(q, 2) + q * r + Math.pow(r, 2));
				//b = (t33 + (t32 * d) - ((t31 * t13)/t11) - ((t31*t12*d)/t11))/t33;
				//c = -1;
				//components
				//v3 = ( -b + Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / (2 * a);
				//if (i == 2)
				//{
					//trace(d, q, r, a, b, c);
					//trace(i, ": ",d, t11,t12,t13,t21,t22,t23);
				//}
				//if ( Math.abs(v3) > 1)
				//{
					//v3 = ( -b - Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / (2 * a);
				//}
				//trace("v3:", v3);
				//v2 = d * v3;
				//v1 = ( -(t12 / t11) * (v2))-((t13/t11) * v3);
				//
				//
				//this.eigenVectors[i] = new SpaceVector(v1, v2, v3);
				//trace(i, ": ", this.eigenVectors[i]);
				//t11 += this.sigma[i];
				//t22 += this.sigma[i];
				//t33 += this.sigma[i];
			//}
			//trace("*--*");
			//trace(this.eigenVectors[2]);
			//trace(this.eigenVectors[1]);
			//trace(this.eigenVectors[0]);
			//
		//}
		
	}
	
}
