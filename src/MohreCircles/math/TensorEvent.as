﻿package MohreCircles.math
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class TensorEvent extends Event
	{
		public var i:int = new int();
		public var j:int = new int();
		public var value:Number = new Number();
		
		public static const VALUE_CHANGE:String = "value_change";
		
		public function TensorEvent(type:String)
		{
			super(type);
		}
		
	}
	
}
