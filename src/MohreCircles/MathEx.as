﻿package MohreCircles
{
	
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class MathEx
	{
		
		public function MathEx()
		{
			

		}
		/**
		 *
		 * @param	n
		 * @param	r
		 * @return
		 */
		public static function round(n:Number, r:Number):Number
		{
			return Math.round(n / r) * r;
		}
		public static function RoundDec(n:Number, r:Number):Number
		{
			var exp:int = Math.pow(10, r);
			var powered:Number = n * exp;
			var rounded:int = Math.round(powered);
			var depowered:Number = rounded / exp;
			//trace(n,exp,powered,rounded,depowered);
			return depowered;
		}
		public static function sign(n:Number):Number
		{
			if (n < 0)
			{
				return -1;
			}
			else if (n > 0)
			{
				return 1;
			}
			else
			{
				return 1;
			}
		}
		public static function Rad2Deg(rad:Number):Number
		{
			return rad * (180 / Math.PI);
		}
		public static function Deg2Rad(deg:Number):Number
		{
			return deg * (Math.PI / 180);
		}
		/**
		 * Are these numbers approximately equal?
		 * @param	a - number to compare
		 * @param	b - number to compare to
		 * @param	c - decimal places to compare ((-) is decades)
		 * @return	boolean
		 */
		public static function IsApproxEqual(a:Number, b:Number, prec:int):Boolean
		{
			if (prec >= 0)
			{
				return MathEx.RoundDec(a - b, prec) == 0;
			}
			else
			{
				return MathEx.round(a - b, Math.pow(10, -1 * prec)) == 0;
			}
		}
	}
	
}
