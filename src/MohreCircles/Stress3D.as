﻿package MohreCircles
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.SpreadMethod;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.net.URLRequest;
	import flash.text.AntiAliasType;
	import flash.text.Font;
	import flash.text.FontStyle;
	import flash.text.FontType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import MohreCircles.ddd.Arrow;
	import MohreCircles.ddd.CoordinateSystem;
	import MohreCircles.ddd.Particle;
	import MohreCircles.ddd.Quadrilateral;
	import MohreCircles.ddd.SpaceVector;
	import MohreCircles.ddd.Viewer;
	import MohreCircles.ddd.VMath;
	import MohreCircles.math.*;
	import MohreCircles.graphics.*;
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class Stress3D extends Sprite
	{
		private var exitButton:Sprite = new Sprite();
		public function get ExitButton():Sprite
		{
			return exitButton;
		}
		private var backgroundSprite:Sprite = new Sprite();
		private var displayType2D:Boolean;
		public function set DisplayType2D(b:Boolean):void
		{
			this.displayType2D = b;
		}
		public function get DisplayType2D():Boolean
		{
			return this.displayType2D;
		}
		
		private var header:Header = new Header();
		
		private var outputBodyStressTensor:Tensor = new Tensor();
		
		private var inputStressTensor:Tensor = new Tensor();
		private var inputArea:Sprite = new Sprite();
		private var inputStressTensorDisplay:InputArea = new InputArea();
		
		//private var inputAreaRot:Sprite = new Sprite();
		//private var inputRotDisplay:InputAreaRot = new InputAreaRot();
		//private var inputRotArray:Array = new Array(3);
		
		private var mohrCircle:MohrCircle = new MohrCircle();
		private var viewer:Viewer;
		
		//Normal stress arrows
		private var SigXXArrow:Arrow;
		private var SigXXArrow1:Arrow;
		private var SigYYArrow:Arrow;
		private var SigYYArrow1:Arrow;
		private var SigZZArrow:Arrow;
		private var SigZZArrow1:Arrow;
		//Shear stress arrows
		private var SigXYArrow:Arrow;
		private var SigXYArrow1:Arrow;
		private var SigYXArrow:Arrow;
		private var SigYXArrow1:Arrow;
		private var SigXZArrow:Arrow;
		private var SigXZArrow1:Arrow;
		private var SigZXArrow:Arrow;
		private var SigZXArrow1:Arrow;
		private var SigYZArrow:Arrow;
		private var SigYZArrow1:Arrow;
		private var SigZYArrow:Arrow;
		private var SigZYArrow1:Arrow;
		
		private var ResXArrow:Arrow;
		private var ResXArrow1:Arrow;
		private var ResYArrow:Arrow;
		private var ResYArrow1:Arrow;
		private var ResZArrow:Arrow;
		private var ResZArrow1:Arrow;

		private var baseXAxis:SpaceVector = new SpaceVector(1, 0, 0);
		private var baseYAxis:SpaceVector = new SpaceVector(0, 1, 0);
		private var baseZAxis:SpaceVector = new SpaceVector(0, 0, 1);

		private var bodyCs:CoordinateSystem;
		private var bodyXAxis:SpaceVector;
		private var bodyYAxis:SpaceVector;
		private var bodyZAxis:SpaceVector;
		
		//private var outputPrincipalDisplay:Sprite = new Sprite();
		//private var outputOtherDisplay:Sprite = new Sprite();
		//private var iconDisplay:Sprite = new Sprite();
		
		//private var imageLoader:Loader;
		//private var urlString:String;
		//private var imageRequest:URLRequest;
		
		private var show3dGenStatus:Boolean = false;
		
		private var eigenArrows:Array = new Array(3);
		
		public function Stress3D(type2d:Boolean = false)
		{
			this.DisplayType2D = type2d;
			
			this.addChild(backgroundSprite);
			backgroundSprite.graphics.beginFill(0xffffff, 1);
			backgroundSprite.graphics.drawRect(0, 0, 800, 525);
			backgroundSprite.graphics.endFill();
			
			
			//inputRotArray[0] = new Number(0);
			//inputRotArray[1] = new Number(0);
			//inputRotArray[2] = new Number(0);
			bodyCs = new CoordinateSystem(baseXAxis, baseYAxis);
			bodyXAxis = new SpaceVector(1, 0, 0, bodyCs);
			bodyYAxis = new SpaceVector(0, 1, 0, bodyCs);
			bodyZAxis = new SpaceVector(0, 0, 1, bodyCs);
			
			this.header = new Header(this.DisplayType2D);
			this.addChild(header);
			
			this.addChild(inputArea);
			inputArea.y = 525 - 75;
			this.inputStressTensorDisplay = new InputArea(this.DisplayType2D);
			inputArea.addChild(inputStressTensorDisplay);
			inputArea.graphics.lineStyle(2, 0x000000);
			inputArea.graphics.drawRect(1, 1, 400 - 2, 75 - 2);
			
			//this.addChild(inputAreaRot);
			//inputAreaRot.x = 400;
			//inputAreaRot.y = 500 - 75;
			//inputAreaRot.addChild(inputRotDisplay);
			//inputAreaRot.graphics.lineStyle(2, 0x000000);
			//inputAreaRot.graphics.drawRect(1, 1, 400 - 2, 75 - 2);
			
			
			//this.outputStressDisplay = new TensorDisplay(this.outputBodyStressTensor,"t");
			//this.addChild(this.outputStressDisplay);
			//this.outputStressDisplay.x = 3;
			//this.outputStressDisplay.y = 3;
			
			this.mohrCircle = new MohrCircle(this.DisplayType2D);
			this.addChild(mohrCircle);
			mohrCircle.y = 100;
			mohrCircle.setWidth = 400;
			mohrCircle.setHeight = 350;
			mohrCircle.graphics.lineStyle(2, 0x000000);
			mohrCircle.graphics.drawRect(1, 1, 400 - 2, 350 - 2);
			if (!this.DisplayType2D)
			{
				this.mohrCircle.ButShow3dGen.addEventListener(PushButtonEvent.VALUE_CHANGE, Show3dGenValueChangeHandler);
			}
			
			this.viewer = new Viewer(400, 100, 400, 425, this.DisplayType2D);
			this.addChild(viewer);
			this.viewer.DrawBorder();
			//bodyCs.Rotate(new SpaceVector(-1, 1.2, 5), Math.PI / 4.2);
			var pt1:SpaceVector = new SpaceVector(1, 1, 1, bodyCs);
			var pt2:SpaceVector = new SpaceVector(1, 1, -1, bodyCs);
			var pt3:SpaceVector = new SpaceVector(1, -1, 1, bodyCs);
			var pt4:SpaceVector = new SpaceVector(1, -1, -1, bodyCs);
			var pt5:SpaceVector = new SpaceVector(-1, 1, 1, bodyCs);
			var pt6:SpaceVector = new SpaceVector(-1, 1, -1, bodyCs);
			var pt7:SpaceVector = new SpaceVector(-1, -1, 1, bodyCs);
			var pt8:SpaceVector = new SpaceVector(-1, -1, -1, bodyCs);
			this.viewer.AddQuadrilateral(new Quadrilateral(pt1,pt3,pt4,pt2)); //+x-face
			this.viewer.AddQuadrilateral(new Quadrilateral(pt1,pt2,pt6,pt5)); //+y-face
			this.viewer.AddQuadrilateral(new Quadrilateral(pt1,pt5,pt7,pt3)); //+z-face
			this.viewer.AddQuadrilateral(new Quadrilateral(pt5,pt6,pt8,pt7)); //-x-face
			this.viewer.AddQuadrilateral(new Quadrilateral(pt7,pt8,pt4,pt3)); //-y-face
			this.viewer.AddQuadrilateral(new Quadrilateral(pt2, pt4, pt8, pt6)); //-z-face
			//Normal Sress Arrows
			this.SigXXArrow = new Arrow(new SpaceVector(1.1, 0, 0, bodyCs), new SpaceVector(2, 0, 0, bodyCs),false, 0x0, 3);
			this.SigXXArrow1 = new Arrow(new SpaceVector(-1.1, 0, 0, bodyCs), new SpaceVector(-2, 0, 0, bodyCs),false, 0x0, 3);
			this.SigYYArrow = new Arrow(new SpaceVector(0, 1.1, 0, bodyCs), new SpaceVector(0, 2, 0, bodyCs),false, 0x0, 3);
			this.SigYYArrow1 = new Arrow(new SpaceVector(0, -1.1, 0, bodyCs), new SpaceVector(0, -2, 0, bodyCs),false, 0x0, 3);
			this.SigZZArrow = new Arrow(new SpaceVector(0, 0, 1.1, bodyCs), new SpaceVector(0, 0, 2, bodyCs),false, 0x0, 3);
			this.SigZZArrow1 = new Arrow(new SpaceVector(0, 0, -1.1, bodyCs), new SpaceVector(0, 0, -2, bodyCs),false, 0x0, 3);
			this.viewer.AddForceArrow(this.SigXXArrow); //+xx
			this.viewer.AddForceArrow(this.SigYYArrow); //+yy
			this.viewer.AddForceArrow(this.SigZZArrow); //+zz
			this.viewer.AddForceArrow(this.SigXXArrow1); //-xx
			this.viewer.AddForceArrow(this.SigYYArrow1); //-yy
			this.viewer.AddForceArrow(this.SigZZArrow1); //-zz
			//Shear Stress Arrows
			this.SigXYArrow = new Arrow(new SpaceVector(1.1, 0, 0, bodyCs), new SpaceVector(1.1, 1, 0, bodyCs), true, 0x0, 2);
			this.SigXYArrow1 = new Arrow(new SpaceVector( -1.1, 0, 0, bodyCs), new SpaceVector( -1.1, -1, 0, bodyCs), true, 0x0, 2);
			this.SigYXArrow = new Arrow(new SpaceVector(0, 1.1, 0, bodyCs), new SpaceVector(1, 1.1, 0, bodyCs), true, 0x0, 2);
			this.SigYXArrow1 = new Arrow(new SpaceVector(0, -1.1, 0, bodyCs), new SpaceVector( -1, -1.1, 0, bodyCs), true, 0x0, 2);
			this.SigXZArrow = new Arrow(new SpaceVector(1.1, 0, 0, bodyCs), new SpaceVector(1.1, 0, 1, bodyCs), true, 0x0, 2);
			this.SigXZArrow1 = new Arrow(new SpaceVector( -1.1, 0, 0, bodyCs), new SpaceVector( -1.1, 0, -1, bodyCs), true, 0x0, 2);
			this.SigZXArrow = new Arrow(new SpaceVector(0, 0, 1.1, bodyCs), new SpaceVector(1, 0, 1.1, bodyCs), true, 0x0, 2);
			this.SigZXArrow1 = new Arrow(new SpaceVector(0, 0, -1.1, bodyCs), new SpaceVector( -1, 0, -1.1, bodyCs), true, 0x0, 2);
			this.SigYZArrow = new Arrow(new SpaceVector(0, 1.1, 0, bodyCs), new SpaceVector(0, 1.1, 1, bodyCs), true, 0x0, 2);
			this.SigYZArrow1 = new Arrow(new SpaceVector(0, -1.1, 0, bodyCs), new SpaceVector(0, -1.1, -1, bodyCs), true, 0x0, 2);
			this.SigZYArrow = new Arrow(new SpaceVector(0, 0, 1.1, bodyCs), new SpaceVector(0, 1, 1.1, bodyCs), true, 0x0, 2);
			this.SigZYArrow1 = new Arrow(new SpaceVector(0, 0, -1.1, bodyCs), new SpaceVector(0, -1, -1.1, bodyCs), true, 0x0, 2);
			this.viewer.AddForceArrow(this.SigXYArrow); //+xy
			this.viewer.AddForceArrow(this.SigXZArrow); //+xz
			this.viewer.AddForceArrow(this.SigXYArrow1); //-xy
			this.viewer.AddForceArrow(this.SigXZArrow1); //-xz
			this.viewer.AddForceArrow(this.SigYXArrow); //+yx
			this.viewer.AddForceArrow(this.SigYZArrow); //+yz
			this.viewer.AddForceArrow(this.SigYXArrow1); //-yx
			this.viewer.AddForceArrow(this.SigYZArrow1); //-yz
			this.viewer.AddForceArrow(this.SigZXArrow); //+zx
			this.viewer.AddForceArrow(this.SigZYArrow); //+zy
			this.viewer.AddForceArrow(this.SigZXArrow1); //-zx
			this.viewer.AddForceArrow(this.SigZYArrow1); //-zy
			//Resultant Sress Arrows
			this.ResXArrow = new Arrow(new SpaceVector(1.1, 0, 0, bodyCs), new SpaceVector(2, 0, 0, bodyCs),false, 0x990000, 3, 0.7);
			this.ResXArrow1 = new Arrow(new SpaceVector(-1.1, 0, 0, bodyCs), new SpaceVector(-2, 0, 0, bodyCs),false, 0x0990000, 3, 0.7);
			this.ResYArrow = new Arrow(new SpaceVector(0, 1.1, 0, bodyCs), new SpaceVector(0, 2, 0, bodyCs),false, 0x990000, 3, 0.7);
			this.ResYArrow1 = new Arrow(new SpaceVector(0, -1.1, 0, bodyCs), new SpaceVector(0, -2, 0, bodyCs),false, 0x0990000, 3, 0.7);
			this.ResZArrow = new Arrow(new SpaceVector(0, 0, 1.1, bodyCs), new SpaceVector(0, 0, 2, bodyCs),false, 0x990000, 3, 0.7);
			this.ResZArrow1 = new Arrow(new SpaceVector(0, 0, -1.1, bodyCs), new SpaceVector(0, 0, -2, bodyCs),false, 0x0990000, 3, 0.7);
			this.viewer.AddResultantArrow(this.ResXArrow); //+x
			this.viewer.AddResultantArrow(this.ResYArrow); //+y
			this.viewer.AddResultantArrow(this.ResZArrow); //+z
			this.viewer.AddResultantArrow(this.ResXArrow1); //-x
			this.viewer.AddResultantArrow(this.ResYArrow1); //-y
			this.viewer.AddResultantArrow(this.ResZArrow1); //-z

			//Reference Axes
			this.viewer.AddRefArrow(new Arrow(new SpaceVector(1.8, 0, 0), new SpaceVector(3.7, 0, 0),false,0xff0000,2,0.8,"X")); //X-axis
			this.viewer.AddRefArrow(new Arrow(new SpaceVector(0, 1.8, 0), new SpaceVector(0, 3.7, 0), false, 0x00ff00, 2, 0.8, "Y")); //Y-axis
			if (!this.DisplayType2D)
			{
				this.viewer.AddRefArrow(new Arrow(new SpaceVector(0, 0, 1.8), new SpaceVector(0, 0, 3.7), false, 0x0000ff, 2, 0.8, "Z")); //Z-axis
			}
			else
			{
				this.viewer.AddRefArrow(new Arrow(new SpaceVector(0, 0, 1.8), new SpaceVector(0, 0, 3.7), false, 0x0000ff, 2, 0.8)); //Z-axis
			}
			//EigenVectors
			this.eigenArrows[2] = new Arrow(new SpaceVector(1.8, 0, 0), new SpaceVector(3.7, 0, 0), false, 0xff0000, 2, 0.8, "I");
			this.viewer.AddEigenArrow(this.eigenArrows[2]); //2
			this.eigenArrows[1] = new Arrow(new SpaceVector(0, 1.8, 0), new SpaceVector(0, 3.7, 0),false,0x00ff00,2,0.8, "II")
			this.viewer.AddEigenArrow(this.eigenArrows[1]); //1
			if (!this.DisplayType2D)
			{
				this.eigenArrows[0] = new Arrow(new SpaceVector(0, 0, 1.8), new SpaceVector(0, 0, 3.7),false,0x0000ff,2,0.8, "III")
				this.viewer.AddEigenArrow(this.eigenArrows[0]); //0
			}
			else
			{
				this.eigenArrows[0] = new Arrow(new SpaceVector(0, 0, 1.8), new SpaceVector(0, 0, 3.7),false,0x0000ff,2,0.8)
				this.viewer.AddEigenArrow(this.eigenArrows[0]); //0
			}
			//Body Axes
			this.viewer.AddBodyArrow(new Arrow(new SpaceVector(1, 0, 0, bodyCs), new SpaceVector(3, 0, 0, bodyCs),false,0xff0000,1, 0.8, "x")); //x-axis
			this.viewer.AddBodyArrow(new Arrow(new SpaceVector(0, 1, 0, bodyCs), new SpaceVector(0, 3, 0, bodyCs),false,0x00ff00,1, 0.8, "y")); //y-axis
			if (!this.DisplayType2D)
			{
				this.viewer.AddBodyArrow(new Arrow(new SpaceVector(0, 0, 1, bodyCs), new SpaceVector(0, 0, 3, bodyCs),false,0x0000ff,1, 0.8, "z")); //z-axis
			}
			else
			{
				this.viewer.AddBodyArrow(new Arrow(new SpaceVector(0, 0, 1, bodyCs), new SpaceVector(0, 0, 3, bodyCs),false,0x0000ff,1, 0.8)); //z-axis
			}
			//this.viewer.AddParticle(new Particle(new SpaceVector(1, 1, 1,bodyCs), 0, 4));
			this.viewer.Redraw();
			
			inputStressTensorDisplay.addEventListener(InputAreaEvent.VALUE_CHANGE, InputChanged);
			//inputRotDisplay.addEventListener(InputAreaEvent.VALUE_CHANGE, InputRotChanged);
			
			inputStressTensorDisplay.setComponentValue(1, 1, 10);
			inputStressTensorDisplay.setComponentValue(1, 2, 15.5);
			inputStressTensorDisplay.setComponentValue(1, 3, -7.6);
			inputStressTensorDisplay.setComponentValue(2, 2, 2.4);
			inputStressTensorDisplay.setComponentValue(2, 3, 10.1);
			inputStressTensorDisplay.setComponentValue(3, 3, 5.32);
			
			//inputRotDisplay.setComponentValue(0, 0);
			
			this.viewer.butGoToPrincipal.addEventListener(PushButtonEvent.VALUE_CHANGE, GoToPrincipalHandler);
			this.viewer.butGoToMaxShear.addEventListener(PushButtonEvent.VALUE_CHANGE, GoToMaxShearHandler);
			this.viewer.butGoToRef.addEventListener(PushButtonEvent.VALUE_CHANGE, GoToRefHandler);
			
			this.viewer.addEventListener(Event.RENDER, RenderHandler);
			
			this.addChild(exitButton);
			var size:uint = 10;
			exitButton.graphics.beginFill(0xeeeeee, 1);
			exitButton.graphics.drawRect(0, 0, size, size);
			exitButton.graphics.endFill();
			exitButton.graphics.lineStyle(2, 0, 1);
			exitButton.graphics.moveTo(1, 1);
			exitButton.graphics.lineTo(size - 1, size - 1);
			exitButton.graphics.moveTo(1, size - 1);
			exitButton.graphics.lineTo(size - 1, 1);
			exitButton.x = 800 - size;
		}
		private function Show3dGenValueChangeHandler(e:PushButtonEvent):void
		{
			this.show3dGenStatus = this.mohrCircle.ButShow3dGen.Status;
			this.viewer.show3dGenStatus = this.mohrCircle.ButShow3dGen.Status;
			this.viewer.ShowBaseAxes = !this.mohrCircle.ButShow3dGen.Status;
			this.viewer.ShowEigenVectors = this.mohrCircle.ButShow3dGen.Status;
			this.Redraw();
		}
		private function RenderHandler(e:Event):void
		{
			this.Redraw();
		}
		private function GoToPrincipalHandler(e:PushButtonEvent):void
		{
			if (!this.DisplayType2D)
			{
				if (e.locked)
				{
					this.viewer.butRotNormal.Status = true;
					this.viewer.RotateType = Viewer.ROTATE_VIEW
					this.viewer.butRotBody.Status = false;
					this.viewer.butRotBody2d.Status = false;
				}
			}
			else
			{
				
			}
			this.viewer.butGoToMaxShear.Status = false;
			this.Redraw();
		}
		private function GoToMaxShearHandler(e:PushButtonEvent):void
		{
			if (e.locked && !this.DisplayType2D)
			{
				this.viewer.butRotNormal.Status = true;
				this.viewer.RotateType = Viewer.ROTATE_VIEW
				this.viewer.butRotBody.Status = false;
				this.viewer.butRotBody2d.Status = false;
			}
			this.viewer.butGoToPrincipal.Status = false;
			this.Redraw();
		}
		private function GoToRefHandler(e:PushButtonEvent):void
		{
			this.viewer.butGoToMaxShear.Status = false;
			this.viewer.butGoToPrincipal.Status = false;
			this.Redraw();
		}
		public function InputChanged(e:InputAreaEvent):void
		{
			inputStressTensor.setComponent(e.i, e.j, e.value);
			if (e.i != e.j)
			{
				inputStressTensor.setComponent(e.j, e.i, e.value);
			}
			if (this.DisplayType2D)
			{
				inputStressTensor.setComponent(1, 3, 0);
				inputStressTensor.setComponent(2, 3, 0);
				inputStressTensor.setComponent(3, 1, 0);
				inputStressTensor.setComponent(3, 2, 0);
				inputStressTensor.setComponent(3, 3, inputStressTensor.getComponent(2,2));
			}
			//trace("**-----**");
			//trace("Spot: ", e.i, ",", e.j);
			//trace("Valu: ", e.value);
			this.Redraw();
		}
		//public function InputRotChanged(e:InputAreaEvent):void
		//{
			//inputRotArray[e.i] = e.value;
			//this.Redraw();
		//}
		public function RotateBody():void
		{
			//var baseXrot:Number = this.inputRotArray[0];
			//var baseYrot:Number = this.inputRotArray[1];
			//var baseZrot:Number = this.inputRotArray[2];
			//this.bodyCs.Rotate(this.bodyXAxis, baseXrot);
			//this.bodyCs.Rotate(this.bodyYAxis, baseYrot);
			//this.bodyCs.Rotate(this.bodyZAxis, baseZrot);
			//trace(bodyCs.xAxis);
		}
		private function GoToPrincipal():void
		{
			//cleared 15 Nov 08 -tbh- BUG disappears with only YZ stress
			if (!this.DisplayType2D)
			{
				if (this.mohrCircle.mohrMath.sigma[0] != this.mohrCircle.mohrMath.sigma[1] ||
					this.mohrCircle.mohrMath.sigma[0] != this.mohrCircle.mohrMath.sigma[2])
					{
						//trace("l");
						//trace(mohrCircle.EigenVectors[2]);
						//trace(mohrCircle.EigenVectors[1]);
						this.bodyCs.SetAxes(mohrCircle.EigenVectors[2], mohrCircle.EigenVectors[1]);
					}
				else
				{
					//trace("nope");
				}
			}
			else
			{
				if (this.mohrCircle.mohrMath.sigma[0] != this.mohrCircle.mohrMath.sigma[1] ||
					this.mohrCircle.mohrMath.sigma[0] != this.mohrCircle.mohrMath.sigma[2])
				{
					//trace("l");
					//trace(mohrCircle.EigenVectors[2]);
					//trace(mohrCircle.EigenVectors[1]);
					if (MathEx.IsApproxEqual(Math.abs(VMath.Unity(mohrCircle.EigenVectors[1]).z), 1, 5))
					{
						this.bodyCs.SetAxes(mohrCircle.EigenVectors[2], mohrCircle.EigenVectors[0]);
					}
					else if (MathEx.IsApproxEqual(Math.abs(VMath.Unity(mohrCircle.EigenVectors[0]).z), 1, 5))
					{
						this.bodyCs.SetAxes(mohrCircle.EigenVectors[2], mohrCircle.EigenVectors[1]);
					}
					else
					{
						this.bodyCs.SetAxes(mohrCircle.EigenVectors[1], mohrCircle.EigenVectors[0]);
					}
				}
			}
			//trace("*--*");
			//trace("0: ",this.mohrCircle.mohrMath.sigma[2], this.mohrCircle.EigenVectors[2]);
			//trace("1: ",this.mohrCircle.mohrMath.sigma[1], this.mohrCircle.EigenVectors[1]);
			//trace("2: ",this.mohrCircle.mohrMath.sigma[0], this.mohrCircle.EigenVectors[0]);
		}
		private function GoToMaxShear():void
		{
				this.GoToPrincipal();
				if (!this.DisplayType2D)
				{
					this.bodyCs.Rotate(new SpaceVector(0,1,0,this.bodyCs), Math.PI / 4);
					
				}
				else
				{
					this.bodyCs.Rotate(new SpaceVector(0, 0, 1,this.bodyCs), Math.PI / 4);
				}
		}
		public function Redraw(...params):void
		{
			//this.RotateBody();
			if (this.viewer.butGoToRef.Status)
			{
				this.bodyCs.SetAxes(this.baseXAxis, this.baseYAxis);
			}
			mohrCircle.Update(inputStressTensor, bodyXAxis, bodyYAxis, bodyZAxis);
			if (this.viewer.butGoToPrincipal.Status)
			{
				this.GoToPrincipal();
			}
			if (this.viewer.butGoToMaxShear.Status)
			{
				this.GoToMaxShear();
			}
			mohrCircle.Update(inputStressTensor, bodyXAxis, bodyYAxis, bodyZAxis);
			this.outputBodyStressTensor = mohrCircle.GetTransformedStress();
			//var stress:Tensor = outputBodyStressTensor;
			//trace("**------------**");
			//trace("|", stress.getComponent(1, 1), stress.getComponent(1, 2), stress.getComponent(1, 3), "|");
			//trace("|", stress.getComponent(2, 1), stress.getComponent(2, 2), stress.getComponent(2, 3), "|");
			//trace("|", stress.getComponent(3, 1), stress.getComponent(3, 2), stress.getComponent(3, 3), "|");
			//trace(" ");
			this.header.BodyStressTensor = this.outputBodyStressTensor;
			this.header.AlphaTensor = mohrCircle.AlphaTensor;
			this.header.Sigma = this.mohrCircle.EigenValues;
			if (!this.DisplayType2D)
			{
				this.header.Sigma = this.mohrCircle.EigenValues;
			}
			else
			{
				this.header.Sigma = new Array(this.mohrCircle.EigenValues[2],this.mohrCircle.EigenValues[0],this.mohrCircle.EigenValues[2]);
			}
			
			this.eigenArrows[2].Reset(VMath.ScalarProduct(1.8, VMath.Unity(this.mohrCircle.EigenVectors[2])), VMath.ScalarProduct(1.9, VMath.Unity(this.mohrCircle.EigenVectors[2])));
			this.eigenArrows[1].Reset(VMath.ScalarProduct(1.8, VMath.Unity(this.mohrCircle.EigenVectors[1])), VMath.ScalarProduct(1.9, VMath.Unity(this.mohrCircle.EigenVectors[1])));
			var sp:SpaceVector = VMath.Cross(this.mohrCircle.EigenVectors[2], this.mohrCircle.EigenVectors[1]);
			this.eigenArrows[0].Reset(VMath.ScalarProduct(1.8, VMath.Unity(sp)), VMath.ScalarProduct(1.9, VMath.Unity(sp)));
			
			if (this.DisplayType2D)
			{
				sp = VMath.Cross(VMath.ScalarProduct( -1, this.mohrCircle.EigenVectors[2]), this.mohrCircle.EigenVectors[1]);
				if (!MathEx.IsApproxEqual(sp.z, 0, 4))
				{
					sp = VMath.Cross(VMath.ScalarProduct( -1, this.mohrCircle.EigenVectors[2]), this.mohrCircle.EigenVectors[0]);
				}
				this.eigenArrows[1].Reset(VMath.ScalarProduct(1.8, VMath.Unity(sp)), VMath.ScalarProduct(1.9, VMath.Unity(sp)));
				this.eigenArrows[0].Alpha = 0;
				//sp = VMath.Cross(this.mohrCircle.EigenVectors[2], this.mohrCircle.EigenVectors[0]);
				//this.eigenArrows[0].Reset(VMath.ScalarProduct(2, VMath.Unity(sp)), VMath.ScalarProduct(2, VMath.Unity(sp)));
			}
			
			this.viewer.EigenVectors[2] = mohrCircle.EigenVectors[2];
			this.viewer.EigenVectors[1] = mohrCircle.EigenVectors[1];
			this.viewer.EigenVectors[0] = VMath.Cross(mohrCircle.EigenVectors[2], mohrCircle.EigenVectors[1]);
			
			this.header.MaxShear = Math.max(this.mohrCircle.mohrMath.radii[0], this.mohrCircle.mohrMath.radii[1], this.mohrCircle.mohrMath.radii[2]);
			this.header.RefreshOutputDisplay();
			this.UpdateViewerStress();
			this.viewer.Redraw();
			//update displayed body stresses
			//update viewer stresses
			
		}
		private function UpdateViewerStress():void
		{
			var maxStress:Number;
			if (this.inputStressTensor.getComponent(1, 1) == 0 &&
				this.inputStressTensor.getComponent(1, 2) == 0 &&
				this.inputStressTensor.getComponent(1, 3) == 0 &&
				this.inputStressTensor.getComponent(2, 1) == 0 &&
				this.inputStressTensor.getComponent(2, 2) == 0 &&
				this.inputStressTensor.getComponent(2, 3) == 0 &&
				this.inputStressTensor.getComponent(3, 1) == 0 &&
				this.inputStressTensor.getComponent(3, 2) == 0 &&
				this.inputStressTensor.getComponent(3, 3) == 0)
				{
					maxStress = 1;
					
				}
			else
			{
				maxStress = outputBodyStressTensor.GetMaxAbsComponent();
			}
				
			var stress:Tensor = outputBodyStressTensor;
			//normals
			SigXXArrow.SetMagnitude(stress.getComponent(1, 1) / maxStress);
			SigXXArrow1.SetMagnitude( stress.getComponent(1, 1) / maxStress);
			SigYYArrow.SetMagnitude(stress.getComponent(2, 2) / maxStress);
			SigYYArrow1.SetMagnitude( stress.getComponent(2, 2) / maxStress);
			SigZZArrow.SetMagnitude(stress.getComponent(3, 3) / maxStress);
			SigZZArrow1.SetMagnitude( stress.getComponent(3, 3) / maxStress);
			
			//shears
			//trace(stress.getComponent(1, 2));
			this.SigXYArrow.SetMagnitude(stress.getComponent(1, 2) / maxStress);
			this.SigXYArrow1.SetMagnitude(stress.getComponent(1, 2) / maxStress);
			this.SigYXArrow.SetMagnitude(stress.getComponent(2, 1) / maxStress);
			this.SigYXArrow1.SetMagnitude(stress.getComponent(2, 1) / maxStress);
			this.SigXZArrow.SetMagnitude(stress.getComponent(1, 3) / maxStress);
			this.SigXZArrow1.SetMagnitude(stress.getComponent(1, 3) / maxStress);
			this.SigZXArrow.SetMagnitude(stress.getComponent(3, 1) / maxStress);
			this.SigZXArrow1.SetMagnitude(stress.getComponent(3, 1) / maxStress);
			this.SigYZArrow.SetMagnitude(stress.getComponent(2, 3) / maxStress);
			this.SigYZArrow1.SetMagnitude(stress.getComponent(2, 3) / maxStress);
			this.SigZYArrow.SetMagnitude(stress.getComponent(3, 2) / maxStress);
			this.SigZYArrow1.SetMagnitude(stress.getComponent(3, 2) / maxStress);
			
			//resultants
			this.ResXArrow.Flip = false;
			this.ResXArrow1.Flip = false;
			this.ResYArrow.Flip = false;
			this.ResYArrow1.Flip = false;
			this.ResZArrow.Flip = false;
			this.ResZArrow1.Flip = false;
			
			this.ResXArrow.EndPoint  = VMath.Add(SigXXArrow.Vector,  SigXYArrow.Vector,  SigXZArrow.Vector);
			this.ResXArrow1.EndPoint = VMath.Add(SigXXArrow1.Vector, SigXYArrow1.Vector, SigXZArrow1.Vector);
			if (stress.getComponent(1, 1) <= 0)
			{
				this.ResXArrow.EndPoint  = VMath.Add(SigXXArrow.Vector,  VMath.ScalarProduct(-1,SigXYArrow.Vector), VMath.ScalarProduct(-1,SigXZArrow.Vector));
				this.ResXArrow1.EndPoint  = VMath.Add(SigXXArrow1.Vector,  VMath.ScalarProduct(-1,SigXYArrow1.Vector), VMath.ScalarProduct(-1,SigXZArrow1.Vector));
				this.ResXArrow.Flip = true;
				this.ResXArrow1.Flip = true;
			}
			else
			{
				this.ResXArrow.Flip = false;
				this.ResXArrow1.Flip = false;
			}
			this.ResYArrow.EndPoint  = VMath.Add(SigYYArrow.Vector,  SigYXArrow.Vector,  SigYZArrow.Vector);
			this.ResYArrow1.EndPoint = VMath.Add(SigYYArrow1.Vector, SigYXArrow1.Vector, SigYZArrow1.Vector);
			if (stress.getComponent(2, 2) <= 0)
			{
				this.ResYArrow.EndPoint  = VMath.Add(SigYYArrow.Vector,  VMath.ScalarProduct(-1,SigYXArrow.Vector), VMath.ScalarProduct(-1,SigYZArrow.Vector));
				this.ResYArrow1.EndPoint  = VMath.Add(SigYYArrow1.Vector,  VMath.ScalarProduct(-1,SigYXArrow1.Vector), VMath.ScalarProduct(-1,SigYZArrow1.Vector));
				this.ResYArrow.Flip = true;
				this.ResYArrow1.Flip = true;
			}
			else
			{
				this.ResYArrow.Flip = false;
				this.ResYArrow1.Flip = false;
			}
			this.ResZArrow.EndPoint  = VMath.Add(SigZZArrow.Vector,  SigZXArrow.Vector,  SigZYArrow.Vector);
			this.ResZArrow1.EndPoint = VMath.Add(SigZZArrow1.Vector, SigZXArrow1.Vector, SigZYArrow1.Vector);
			if (stress.getComponent(3, 3) <= 0)
			{
				this.ResZArrow.EndPoint  = VMath.Add(SigZZArrow.Vector,  VMath.ScalarProduct(-1,SigZXArrow.Vector), VMath.ScalarProduct(-1,SigZYArrow.Vector));
				this.ResZArrow1.EndPoint  = VMath.Add(SigZZArrow1.Vector,  VMath.ScalarProduct(-1,SigZXArrow1.Vector), VMath.ScalarProduct(-1,SigZYArrow1.Vector));
				this.ResZArrow.Flip = true;
				this.ResZArrow1.Flip = true;
			}
			else
			{
				this.ResZArrow.Flip = false;
				this.ResZArrow1.Flip = false;
			}
			
			//trace("**------------**");
			//trace("|", stress.getComponent(1, 1), stress.getComponent(1, 2), stress.getComponent(1, 3), "|");
			//trace("|", stress.getComponent(2, 1), stress.getComponent(2, 2), stress.getComponent(2, 3), "|");
			//trace("|", stress.getComponent(3, 1), stress.getComponent(3, 2), stress.getComponent(3, 3), "|");
			//trace(" ");
		}
	}
	
}
