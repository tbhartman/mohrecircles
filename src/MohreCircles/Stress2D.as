﻿package MohreCircles
{
	import flash.display.Sprite;
	import flash.events.Event;
	import MohreCircles.ddd.Particle;
	import MohreCircles.ddd.SpaceVector;
	import MohreCircles.ddd.Viewer;
	import MohreCircles.math.*;
	import MohreCircles.graphics.*;
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class Stress2D extends Sprite
	{/*
		private var inputStressTensor:Tensor = new Tensor();
		
		private var inputArea:Sprite = new Sprite();
		private var inputStressTensorDisplay:InputArea = new InputArea();
		
		
		private var mohrCircle:MohrCircle = new MohrCircle();
		private var viewer:Viewer;
		
		public function Stress2D()
		{
			this.addChild(inputArea);
			inputArea.y = 500 - 75;
			inputArea.addChild(inputStressTensorDisplay);
			inputArea.graphics.lineStyle(2, 0x000000);
			inputArea.graphics.drawRect(1, 1, 600 - 2, 75 - 2);
			
			
			this.addChild(mohrCircle);
			mohrCircle.Type = MohrCircleType.TwoD;
			mohrCircle.y = 75;
			mohrCircle.setWidth = 400;
			mohrCircle.setHeight = 350;
			//deleted 15 Nov 08 -tbh- TODO MohrCircle class should have border property
			mohrCircle.graphics.lineStyle(2, 0x000000);
			mohrCircle.graphics.drawRect(1, 1, 400 - 2, 350 - 2);
			
			this.viewer = new Viewer(400, 75, 400, 350);
			this.addChild(viewer);
			this.viewer.DrawBorder();
			this.viewer.AddParticle(new Particle(new SpaceVector(-1, -1, -1),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(1, 0, 0),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(1, 0, .25),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(1, 0, .5),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(1, 0, .75),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(1, .25, 0),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(1, .5, 0),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(1, .75, 0),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(0, 1, 0),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(0, 1, .25),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(0, 1, .5),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(0, 1, .75),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(.25, 1, 0),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(.5, 1, 0),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(.75, 1, 0),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(0, 0, 1),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(.25, 0, 1),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(.5, 0, 1),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(.75, 0, 1),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(0, .25, 1),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(0, .5, 1),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(0, .75, 1),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(1, 0, 0),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(0, 1, 0),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(0, 0, 1),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(.5, 0, 0),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(0, .5, 0),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(0, 0, .5),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(.75, 0, 0),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(0, .75, 0),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(0, 0, .75),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(.25, 0, 0),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(0, .25, 0),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(0, 0, .25),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(1, 1, 0),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(1, 0, 1),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(0, 1, 1),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(1, 1, .5),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(1, .5, 1),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(.5, 1, 1),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(1, 1, .75),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(1, .75, 1),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(.75, 1, 1),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(1, 1, .25),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(1, .25, 1),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(.25, 1, 1),0));
			this.viewer.AddParticle(new Particle(new SpaceVector(1, 1, 1),0));
			this.viewer.Redraw();
			
			inputStressTensorDisplay.addEventListener(InputAreaEvent.VALUE_CHANGE, InputChanged);
			
			inputStressTensorDisplay.setComponentValue(1, 1, 10);
			inputStressTensorDisplay.setComponentValue(1, 2, 3);
			inputStressTensorDisplay.setComponentValue(1, 3, 0);
			inputStressTensorDisplay.setComponentValue(2, 2, 1);
			inputStressTensorDisplay.setComponentValue(2, 3, 0);
			inputStressTensorDisplay.setComponentValue(3, 3, -4);

		}
		public function InputChanged(e:InputAreaEvent):void
		{
			inputStressTensor.setComponent(e.i, e.j, e.value);
			this.Redraw();
		}
		public function Redraw(...params):void
		{
			mohrCircle.Update(inputStressTensor);
		}*/
	}
	
}
