﻿package MohreCircles
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class PushButtonEvent extends Event
	{
		public var value:Boolean = new Boolean();
		public var locked:Boolean = new Boolean();
		
		public static const VALUE_CHANGE:String = "value_change";
		
		public function PushButtonEvent(type:String)
		{
			super(type);
		}
		
	}
	
}
