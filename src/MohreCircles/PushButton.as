﻿package MohreCircles
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class PushButton extends Sprite
	{
		private var blink:Sprite = new Sprite();
		private var timer:Timer;
		private var status:Boolean = false;
		public function get Status():Boolean
		{
			return this.status;
			//trace("hi");
		}
		public function set Status(b:Boolean):void
		{
			this.status = b;
			//this.DispatchValueChangedEvent();
			this.SetButGraphic();
		}
		
		private var buttonType:String;
		//private function set ButtonType(v:String):void
		//{
			//this.buttonType = v;
		//}
		public function get ButtonType():String
		{
			return this.buttonType;
		}
		public static const SWITCH_WHEN_PRESSED:String = "switched_when_pressed";
		public static const SWITCH_UNTIL_RELEASED:String = "switch_until_released";
		public static const SWITCH_UNTIL_RELEASED_OR_LOCKED:String = "switch_until_released_or_locked";
		
		private var but:Sprite = new Sprite();
		private var butWidth:uint;
		private var butHeight:uint;
		
		private var butDownSprite:Sprite = new Sprite();
		
		private var enabled:Boolean = true;
		public function set Enabled(b:Boolean):void
		{
			this.enabled = b;
		}
		public function get Enabled():Boolean
		{
			return this.enabled;
		}
		private var pictureEnabled:Boolean = false;
		public function set PictureEnabled(b:Boolean):void
		{
			this.pictureEnabled = b;
			this.SetButGraphic();
		}
		public function get PictureEnabled():Boolean
		{
			return this.pictureEnabled;
		}
		private var pictureUp:Bitmap;
		private var pictureDown:Bitmap;
		public function set PictureUp(b:Bitmap):void
		{
			this.pictureUp = b;
		}
		public function set PictureDown(b:Bitmap):void
		{
			this.pictureDown = b;
		}
		
		private var overlay:Sprite = new Sprite();
		public function set Overlay(s:Sprite):void
		{
			if (this.contains(this.overlay))
			{
				this.removeChild(this.overlay);
			}
			this.overlay = s;
			this.addChildAt(this.overlay, 1);
			this.SetButGraphic();
		}
		
		//private var txtLbl:Sprite = new Sprite();
		
		public function PushButton(x:int=0,y:int=0,width:int=0,height:int=0,def:Boolean=false,type:String = PushButton.SWITCH_WHEN_PRESSED):void
		{
			this.x = x;
			this.y = y;
			this.butWidth = width;
			this.butHeight = height;
			this.buttonType = type;
			this.addChild(this.but);
			this.addChild(this.butDownSprite);
			this.overlay.visible = false;
			this.blink.graphics.lineStyle(2, 0xff0000, 1);
			this.blink.graphics.beginFill(0xff0000, 0.2);
			this.blink.graphics.drawRect(2, 2, width - 4, height - 4);
			this.blink.graphics.endFill();
			
			this.butDownSprite.graphics.beginFill(0, 0);
			this.butDownSprite.graphics.drawRect(0, 0, this.butWidth, this.butHeight);
			this.butDownSprite.graphics.endFill();
			this.butDownSprite.addEventListener(MouseEvent.MOUSE_DOWN, MouseDownHandler);
			
			this.status = def;
			this.SetButGraphic();
			//this.SetButGraphic();
			
			
		}
		private function SetButGraphic():void
		{
			this.but.graphics.clear();
			if (this.PictureEnabled)
			{
				this.overlay.visible = false;
				if (!this.contains(this.pictureDown))
				{
					this.addChildAt(this.pictureDown,0);
				}
				if (!this.contains(this.pictureUp))
				{
					this.addChildAt(this.pictureUp,0);
				}
				if (this.Status)
				{
					this.pictureDown.visible = true;
					this.pictureUp.visible = false;
				}
				else
				{
					this.pictureDown.visible = false;
					this.pictureUp.visible = true;
				}
				//trace(this.status);
				this.but.graphics.beginFill(0xffffff, 0);
				this.but.graphics.drawRect(0, 0, this.butWidth, this.butHeight);
				this.but.graphics.endFill();
			}
			else
			{
				this.overlay.visible = true;
				var butBorderWidth:Number = 2;
				if (this.Status)
				{
					this.overlay.x = 0.5;
					this.overlay.y = 0.5;
					this.but.graphics.beginFill(0x222222);
					this.but.graphics.moveTo(0, 0);
					this.but.graphics.lineTo(0, this.butHeight);
					this.but.graphics.lineTo(this.butWidth, 0);
					this.but.graphics.lineTo(0, 0);
					this.but.graphics.endFill();
					this.but.graphics.beginFill(0xcccccc);
					this.but.graphics.moveTo(this.butWidth, this.butHeight);
					this.but.graphics.lineTo(0, this.butHeight);
					this.but.graphics.lineTo(this.butWidth, 0);
					this.but.graphics.lineTo(this.butWidth, this.butHeight);
					this.but.graphics.endFill();
					this.but.graphics.beginFill(0x999999);
					this.but.graphics.drawRect(butBorderWidth, butBorderWidth, this.butWidth-2*butBorderWidth, this.butHeight-2*butBorderWidth);
					this.but.graphics.endFill();
				}
				else
				{
					this.overlay.x = 0;
					this.overlay.y = 0;
					this.but.graphics.beginFill(0xcccccc);
					this.but.graphics.moveTo(0, 0);
					this.but.graphics.lineTo(0, this.butHeight);
					this.but.graphics.lineTo(this.butWidth, 0);
					this.but.graphics.lineTo(0, 0);
					this.but.graphics.endFill();
					this.but.graphics.beginFill(0x222222);
					this.but.graphics.moveTo(this.butWidth, this.butHeight);
					this.but.graphics.lineTo(0, this.butHeight);
					this.but.graphics.lineTo(this.butWidth, 0);
					this.but.graphics.lineTo(this.butWidth, this.butHeight);
					this.but.graphics.endFill();
					this.but.graphics.beginFill(0xb8b8b8);
					this.but.graphics.drawRect(butBorderWidth, butBorderWidth, this.butWidth-2*butBorderWidth, this.butHeight-2*butBorderWidth);
					this.but.graphics.endFill();
				}
			}
		}
		private function MouseDownHandler(e:MouseEvent):void
		{
			if (this.enabled)
			{
				this.Switch();
			}
		}
		private function MouseUpHandler(e:MouseEvent):void
		{
			switch(this.ButtonType)
			{
				case PushButton.SWITCH_WHEN_PRESSED:
					break;
				case PushButton.SWITCH_UNTIL_RELEASED:
					this.Status = false;
					this.removeEventListener(MouseEvent.MOUSE_UP, MouseUpHandler);
					this.removeEventListener(MouseEvent.MOUSE_OUT, MouseUpHandler);
					break;
				case PushButton.SWITCH_UNTIL_RELEASED_OR_LOCKED:
					this.Status = false;
					this.removeEventListener(MouseEvent.MOUSE_UP, MouseUpHandler);
					this.removeEventListener(MouseEvent.MOUSE_OUT, MouseUpHandler);
					break;
				default:
					break;
			}
			
		}
		public function Switch():void
		{
			//FIXEDBUG Top four display buttons not working properly. 11 Nov 08
			switch(this.ButtonType)
			{
				case PushButton.SWITCH_WHEN_PRESSED:
					this.Status = !this.Status;
					this.DispatchValueChangedEvent();
					break;
				case PushButton.SWITCH_UNTIL_RELEASED:
					this.Status = true;
					this.DispatchValueChangedEvent();
					this.addEventListener(MouseEvent.MOUSE_UP, MouseUpHandler);
					this.addEventListener(MouseEvent.MOUSE_OUT, MouseUpHandler);
					break;
				case PushButton.SWITCH_UNTIL_RELEASED_OR_LOCKED:
					if (this.Status)
					{
						this.Status = false;
						this.DispatchValueChangedEvent();
					}
					else
					{
						this.Status = true;
						this.DispatchValueChangedEvent();
						this.addEventListener(MouseEvent.MOUSE_UP, MouseUpHandler);
						this.addEventListener(MouseEvent.MOUSE_OUT, MouseUpHandler);
						this.timer = new Timer(400, 1);
						this.timer.addEventListener(TimerEvent.TIMER, TimerHandler);
						this.timer.start();
					}
					break;
				default:
					break;
			}
			
		}
		private function TimerHandler(e:TimerEvent):void
		{
			switch(this.ButtonType)
			{
				case PushButton.SWITCH_WHEN_PRESSED:
					break;
				case PushButton.SWITCH_UNTIL_RELEASED:
					break;
				case PushButton.SWITCH_UNTIL_RELEASED_OR_LOCKED:
					if (this.Status)
					{
						this.removeEventListener(MouseEvent.MOUSE_UP, MouseUpHandler);
						this.removeEventListener(MouseEvent.MOUSE_OUT, MouseUpHandler);
						this.timer.removeEventListener(TimerEvent.TIMER, TimerHandler);
						this.timer.stop();
						this.DispatchValueChangedEvent(true);
						this.timer = new Timer(50, 1);
						this.timer.addEventListener(TimerEvent.TIMER, BlinkHandler);
						this.timer.start();
						this.addChild(blink);
					}
					else
					{
					}
					break;
				default:
					break;
			}
		}
		private function BlinkHandler(e:TimerEvent):void
		{
			this.timer.stop();
			this.timer.removeEventListener(TimerEvent.TIMER, BlinkHandler);
			//trace("yeah");
			this.removeChild(blink);
		}
		private function DispatchValueChangedEvent(locked:Boolean = false):void
		{
			var e:PushButtonEvent = new PushButtonEvent(PushButtonEvent.VALUE_CHANGE);
			e.value = this.Status;
			e.locked = locked;
			this.dispatchEvent(e);
		}
	}
	
}
