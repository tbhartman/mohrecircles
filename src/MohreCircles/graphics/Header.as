﻿package MohreCircles.graphics
{
	import flash.display.Sprite;
	import flash.text.*;
	import MohreCircles.math.Tensor;
	import MohreCircles.MathEx;
	
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class Header extends Sprite
	{
		[Embed(source='../../../lib/TBH.ttf', fontName = "TBH", unicodeRange="U+0030-U+0039,U+0041-U+005A,U+0061-U+007A,U+003A,U+002E,U+003D,U+0028,U+0029,U+002D,U+00AD", mimeType='application/x-font')]
		public static var TBH:Class;
		Font.registerFont(TBH);
		[Embed(source='../../../lib/symbol.ttf', fontName = "Symbol", unicodeRange="U+0030-U+0039,U+0041-U+005A,U+0061-U+007A,U+003A,U+002E,U+003D,U+0028,U+0029,U+002D,U+00AD", mimeType='application/x-font')]
		public static var Symbol:Class;
		Font.registerFont(Symbol);
		[Embed(source='../../../lib/CALIBRI.ttf', fontName = "Calibri", unicodeRange="U+0030-U+0039,U+0041-U+005A,U+0061-U+007A,U+003A,U+002E,U+003D,U+0028,U+0029,U+002D,U+00AD", mimeType='application/x-font')]
		public static var Calibri:Class;
		Font.registerFont(Calibri);
		
		private var displayType2D:Boolean;
		public function set DisplayType2D(b:Boolean):void
		{
			this.displayType2D = b;
		}
		public function get DisplayType2D():Boolean
		{
			return this.displayType2D;
		}
		
		private var outputPrinSprite:Sprite = new Sprite();
		private var outputTensorSprite:Sprite = new Sprite();
		private var icon:Logo;
		private var outputStressDisplay:Array = new Array(9);
		private var outputPrinDisplay:Array = new Array(3);
		private var outputAlphaDisplay:Array = new Array(9);
		//private var outputBodyStressTensor:Array = new Array(9);
		private var outputBodyStressTensor:Tensor = new Tensor();
		private var sigma:Array = new Array(3);
		private var maxShear:Number;
		private var maxShearDisplay:TextField = new TextField();
		public function set MaxShear(n:Number):void
		{
			this.maxShear = n;
		}
		private var outputAlphaTensor:Tensor = new Tensor();
		public function set BodyStressTensor(t:Tensor):void
		{
			this.outputBodyStressTensor = t;
		}
		public function set AlphaTensor(t:Tensor):void
		{
			this.outputAlphaTensor = t;
		}
		public function set Sigma(a:Array):void
		{
			this.sigma = a;
		}
		
		//Dimensions
			//tensors
			private var sigmaX:int;
			private var sigmaHeight:int;
			private var sigmaWidth:int;
			private var alphaX:int;
			private var alphaHeight:int;
			private var alphaWidth:int;
			private var prinX:int;
			private var prinHeight:int;
			private var prinWidth:int;
			private var tauX:int;
			
			private var sigmaBracketX:int;
			private var sigmaBracketY:int;
			private var alphaBracketX:int;
			private var alphaBracketY:int;
			private var prinBracketX:int;
			private var prinBracketY:int;
			
			private var bracketSpacing:int;
			
			private var headerHeight:int = 100;
			
		
		public function Header(type2d:Boolean = false)
		{
			this.DisplayType2D = type2d;
			sigmaX = 6;
			var extraSpace:Number = sigmaX * 3;
			sigmaHeight = 64;
			sigmaWidth = 135;
			alphaX = sigmaX + extraSpace;
			alphaHeight = sigmaHeight;
			alphaWidth = sigmaWidth;
			prinX = sigmaX + extraSpace;
			prinHeight = sigmaHeight;
			prinWidth = sigmaWidth / 2.3;
			tauX = sigmaX + extraSpace;
			
			bracketSpacing = 7;
			
			//this.addChild(backgroundImage);
			this.icon = new Logo(125, 75, 1/3,true);
			this.addChild(icon);
			this.icon.x = 650;
			this.addChild(outputTensorSprite);
			
			this.InitOutputDisplay();
			
		}
		private function InitTensorDisplay():void
		{
			this.outputTensorSprite;
			
			var txtSymForm:TextFormat = new TextFormat();
			txtSymForm.size = 24;
			txtSymForm.bold = true;
			txtSymForm.font = "Symbol";
			var txtCalForm:TextFormat = new TextFormat();
			txtCalForm.size = 24;
			txtCalForm.bold = true;
			txtCalForm.font = "Calibri";
			var txtSubForm:TextFormat = new TextFormat();
			txtSubForm.size = 12;
			txtSubForm.bold = true;
			txtSubForm.font = "Calibri";
			
			var txtSigma:TextField = new TextField();
			txtSigma.x = sigmaX;
			txtSigma.selectable = false;
			txtSigma.embedFonts = true;
			txtSigma.defaultTextFormat = txtSymForm;
			txtSigma.text = "s";
			txtSigma.y = headerHeight/2-txtSigma.textHeight/2;
			this.addChild(txtSigma);
			
			var txtEq:TextField = new TextField();
			txtEq.x = txtSigma.x + txtSigma.textWidth;
			txtEq.y = txtSigma.y;
			txtEq.selectable = false;
			txtEq.embedFonts = true;
			txtEq.defaultTextFormat = txtCalForm;
			txtEq.text = "=";
			this.addChild(txtEq);
			
			var txtAlpha:TextField = new TextField();
			txtAlpha.x = txtEq.x + txtEq.textWidth + sigmaWidth + alphaX + bracketSpacing;
			txtAlpha.y = txtSigma.y;
			txtAlpha.selectable = false;
			txtAlpha.embedFonts = true;
			txtAlpha.defaultTextFormat = txtSymForm;
			txtAlpha.text = "a";
			this.addChild(txtAlpha);
			
			var txtEq2:TextField = new TextField();
			txtEq2.x = txtAlpha.x + txtAlpha.textWidth;
			txtEq2.y = txtAlpha.y;
			txtEq2.selectable = false;
			txtEq2.embedFonts = true;
			txtEq2.defaultTextFormat = txtCalForm;
			txtEq2.text = "=";
			this.addChild(txtEq2);
			
			sigmaBracketX = txtEq.x + txtEq.textWidth + bracketSpacing;
			sigmaBracketY = headerHeight/2 - sigmaHeight/2;
			this.DrawBrackets(sigmaBracketX, sigmaBracketY, sigmaWidth, sigmaHeight);
			alphaBracketX = txtEq2.x + txtEq2.textWidth + bracketSpacing;
			alphaBracketY = sigmaBracketY;
			this.DrawBrackets(alphaBracketX, alphaBracketY, alphaWidth, alphaHeight);
			
			var txtPrin:TextField = new TextField();
			txtPrin.x = txtEq2.x + txtEq2.textWidth + alphaWidth + prinX + bracketSpacing;
			txtPrin.y = txtSigma.y;
			txtPrin.selectable = false;
			txtPrin.embedFonts = true;
			txtPrin.defaultTextFormat = txtSymForm;
			txtPrin.text = "s";
			this.addChild(txtPrin);
			
			var txtPrinP:TextField = new TextField();
			txtPrinP.x = txtPrin.x + txtPrin.textWidth;
			txtPrinP.y = txtSigma.y + txtPrin.textHeight/1.5;
			txtPrinP.selectable = false;
			txtPrinP.embedFonts = true;
			txtPrinP.defaultTextFormat = txtSubForm;
			txtPrinP.text = "P";
			this.addChild(txtPrinP);
			
			var txtEq3:TextField = new TextField();
			txtEq3.x = txtPrinP.x + txtPrinP.textWidth;
			txtEq3.y = txtPrin.y;
			txtEq3.selectable = false;
			txtEq3.embedFonts = true;
			txtEq3.defaultTextFormat = txtCalForm;
			txtEq3.text = "=";
			this.addChild(txtEq3);
			
			prinBracketX = txtEq3.x + txtEq3.textWidth + bracketSpacing;
			prinBracketY = sigmaBracketY;
			this.DrawBrackets(prinBracketX, prinBracketY, prinWidth, prinHeight);
			
			var txtTau:TextField = new TextField();
			txtTau.x = txtEq3.x + txtEq3.textWidth + prinWidth + tauX + bracketSpacing;
			txtTau.y = txtSigma.y;
			txtTau.selectable = false;
			txtTau.embedFonts = true;
			txtTau.defaultTextFormat = txtSymForm;
			txtTau.text = "t";
			this.addChild(txtTau);
			
			var txtMax:TextField = new TextField();
			txtMax.x = txtTau.x + txtTau.textWidth;
			txtMax.y = txtSigma.y + txtTau.textHeight/1.5;
			txtMax.selectable = false;
			txtMax.embedFonts = true;
			txtMax.defaultTextFormat = txtSubForm;
			txtMax.text = "max";
			this.addChild(txtMax);
			
			var txtEq4:TextField = new TextField();
			txtEq4.x = txtMax.x + txtMax.textWidth;
			txtEq4.y = txtTau.y;
			txtEq4.selectable = false;
			txtEq4.embedFonts = true;
			txtEq4.defaultTextFormat = txtCalForm;
			txtEq4.text = "=";
			this.addChild(txtEq4);
			
			this.maxShearDisplay.x = txtEq4.x - txtEq4.textWidth;
			this.maxShearDisplay.y = txtEq4.y + 2*txtEq4.textHeight / 10;
		}
		private function DrawBrackets(x:int,y:int,w:int,h:int):void
		{
			var armLength:int = 3;
			var thickness:Number = 2.5;
			this.graphics.lineStyle(thickness, 0, 1);
			this.graphics.moveTo(x+armLength, y);
			this.graphics.lineTo(x, y);
			this.graphics.lineTo(x, y+h);
			this.graphics.lineTo(x+armLength, y+h);
			this.graphics.moveTo(x+w-armLength, y);
			this.graphics.lineTo(x+w, y);
			this.graphics.lineTo(x+w, y+h);
			this.graphics.lineTo(x+w-armLength, y+h);
			
		}
		private function InitOutputDisplay():void
		{
			this.InitTensorDisplay();
			var tmp:TextField = new TextField();
			var i:uint;
			var format:TextFormat = new TextFormat();
			format.bold = true;
			format.size = 12;
			format.color = 0;
			format.font = "Calibri";
			var largeFormat:TextFormat = new TextFormat();
			largeFormat.bold = true;
			largeFormat.size = 14;
			largeFormat.color = 0;
			largeFormat.font = "Calibri";
			//format.bold = true;
			for (i = 0; i < this.outputStressDisplay.length; i++)
			{
				this.outputStressDisplay[i] = new  TextField();
				this.outputStressDisplay[i].text = "0.0";
				this.outputStressDisplay[i].embedFonts = true;
				this.outputStressDisplay[i].selectable = false;
				this.outputStressDisplay[i].autoSize = TextFieldAutoSize.CENTER;
				this.outputStressDisplay[i].defaultTextFormat = format;
				this.outputTensorSprite.addChild(this.outputStressDisplay[i]);
			}
			for (i = 0; i < this.outputAlphaDisplay.length; i++)
			{
				this.outputAlphaDisplay[i] = new TextField();
				this.outputAlphaDisplay[i].text = "0.0";
				this.outputAlphaDisplay[i].embedFonts = true;
				this.outputAlphaDisplay[i].selectable = false;
				this.outputAlphaDisplay[i].autoSize = TextFieldAutoSize.CENTER;
				this.outputAlphaDisplay[i].defaultTextFormat = format;
				this.outputTensorSprite.addChild(this.outputAlphaDisplay[i]);
			}
			for (i = 0; i < this.outputPrinDisplay.length; i++)
			{
				this.outputPrinDisplay[i] = new TextField();
				this.outputPrinDisplay[i].text = "0.0";
				this.outputPrinDisplay[i].embedFonts = true;
				this.outputPrinDisplay[i].selectable = false;
				this.outputPrinDisplay[i].autoSize = TextFieldAutoSize.CENTER;
				this.outputPrinDisplay[i].defaultTextFormat = format;
				this.outputTensorSprite.addChild(this.outputPrinDisplay[i]);
			}
			
			this.maxShearDisplay.text = "0.000";
			this.maxShearDisplay.embedFonts = true;
			this.maxShearDisplay.selectable = false;
			this.maxShearDisplay.autoSize = TextFieldAutoSize.CENTER;
			this.maxShearDisplay.defaultTextFormat = largeFormat;
			this.outputTensorSprite.addChild(this.maxShearDisplay);
			
			var spaceX:Number = sigmaWidth/3;
			var spaceY:Number = sigmaHeight/3;
			var startX:Number = sigmaBracketX + spaceX/3 + 4;
			var startY:Number = sigmaBracketY;
			var a1x:uint = startX + 0 * spaceX;
			var a2x:uint = startX + 1 * spaceX;
			var a3x:uint = startX + 2 * spaceX;
			var a1y:uint = startY + 0 * spaceY;
			var a2y:uint = startY + 1 * spaceY;
			var a3y:uint = startY + 2 * spaceY;
			
			this.outputStressDisplay[0].x = a1x;
			this.outputStressDisplay[1].x = a2x;
			this.outputStressDisplay[2].x = a3x;
			this.outputStressDisplay[3].x = a1x;
			this.outputStressDisplay[4].x = a2x;
			this.outputStressDisplay[5].x = a3x;
			this.outputStressDisplay[6].x = a1x;
			this.outputStressDisplay[7].x = a2x;
			this.outputStressDisplay[8].x = a3x;
			
			this.outputStressDisplay[0].y = a1y;
			this.outputStressDisplay[1].y = a1y;
			this.outputStressDisplay[2].y = a1y;
			this.outputStressDisplay[3].y = a2y;
			this.outputStressDisplay[4].y = a2y;
			this.outputStressDisplay[5].y = a2y;
			this.outputStressDisplay[6].y = a3y;
			this.outputStressDisplay[7].y = a3y;
			this.outputStressDisplay[8].y = a3y;
			
			spaceX = alphaWidth/3;
			spaceY = alphaHeight/3;
			startX = alphaBracketX + spaceX/3 + 4;
			startY = alphaBracketY;
			
			a1x = startX + 0 * spaceX;
			a2x = startX + 1 * spaceX;
			a3x = startX + 2 * spaceX;
			a1y = startY + 0 * spaceY;
			a2y = startY + 1 * spaceY;
			a3y = startY + 2 * spaceY;
			
			this.outputAlphaDisplay[0].x = a1x;
			this.outputAlphaDisplay[1].x = a2x;
			this.outputAlphaDisplay[2].x = a3x;
			this.outputAlphaDisplay[3].x = a1x;
			this.outputAlphaDisplay[4].x = a2x;
			this.outputAlphaDisplay[5].x = a3x;
			this.outputAlphaDisplay[6].x = a1x;
			this.outputAlphaDisplay[7].x = a2x;
			this.outputAlphaDisplay[8].x = a3x;
			
			this.outputAlphaDisplay[0].y = a1y;
			this.outputAlphaDisplay[1].y = a1y;
			this.outputAlphaDisplay[2].y = a1y;
			this.outputAlphaDisplay[3].y = a2y;
			this.outputAlphaDisplay[4].y = a2y;
			this.outputAlphaDisplay[5].y = a2y;
			this.outputAlphaDisplay[6].y = a3y;
			this.outputAlphaDisplay[7].y = a3y;
			this.outputAlphaDisplay[8].y = a3y;
			
			//Principal stress components
			spaceX = 0;
			spaceY = prinHeight/3;
			startX = prinBracketX + prinWidth/2.9;
			startY = prinBracketY;
			
			a1x = startX + 0 * spaceX + 7;
			a2x = startX + 1 * spaceX;
			a3x = startX + 2 * spaceX;
			a1y = startY + 0 * spaceY;
			a2y = startY + 1 * spaceY;
			a3y = startY + 2 * spaceY;
			
			this.outputPrinDisplay[0].x = a1x;
			this.outputPrinDisplay[1].x = a1x;
			this.outputPrinDisplay[2].x = a1x;
			
			this.outputPrinDisplay[0].y = a1y;
			this.outputPrinDisplay[1].y = a2y;
			this.outputPrinDisplay[2].y = a3y;
			

		}
		public function RefreshOutputDisplay():void
		{
			this.outputStressDisplay[0].text = this.Number2DisplayText(this.outputBodyStressTensor.getComponent(1, 1));
			this.outputStressDisplay[1].text = this.Number2DisplayText(this.outputBodyStressTensor.getComponent(1, 2));
			this.outputStressDisplay[2].text = this.Number2DisplayText(this.outputBodyStressTensor.getComponent(1, 3));
			this.outputStressDisplay[3].text = this.Number2DisplayText(this.outputBodyStressTensor.getComponent(2, 1));
			this.outputStressDisplay[4].text = this.Number2DisplayText(this.outputBodyStressTensor.getComponent(2, 2));
			this.outputStressDisplay[5].text = this.Number2DisplayText(this.outputBodyStressTensor.getComponent(2, 3));
			this.outputStressDisplay[6].text = this.Number2DisplayText(this.outputBodyStressTensor.getComponent(3, 1));
			this.outputStressDisplay[7].text = this.Number2DisplayText(this.outputBodyStressTensor.getComponent(3, 2));
			this.outputStressDisplay[8].text = this.Number2DisplayText(this.outputBodyStressTensor.getComponent(3, 3));
			
			this.outputAlphaDisplay[0].text = this.Number2DisplayText(this.outputAlphaTensor.getComponent(1, 1));
			this.outputAlphaDisplay[1].text = this.Number2DisplayText(this.outputAlphaTensor.getComponent(1, 2));
			this.outputAlphaDisplay[2].text = this.Number2DisplayText(this.outputAlphaTensor.getComponent(1, 3));
			this.outputAlphaDisplay[3].text = this.Number2DisplayText(this.outputAlphaTensor.getComponent(2, 1));
			this.outputAlphaDisplay[4].text = this.Number2DisplayText(this.outputAlphaTensor.getComponent(2, 2));
			this.outputAlphaDisplay[5].text = this.Number2DisplayText(this.outputAlphaTensor.getComponent(2, 3));
			this.outputAlphaDisplay[6].text = this.Number2DisplayText(this.outputAlphaTensor.getComponent(3, 1));
			this.outputAlphaDisplay[7].text = this.Number2DisplayText(this.outputAlphaTensor.getComponent(3, 2));
			this.outputAlphaDisplay[8].text = this.Number2DisplayText(this.outputAlphaTensor.getComponent(3, 3));
			
			this.outputPrinDisplay[0].text = this.Number2DisplayText(this.sigma[2]);
			this.outputPrinDisplay[1].text = this.Number2DisplayText(this.sigma[1]);
			this.outputPrinDisplay[2].text = this.Number2DisplayText(this.sigma[0]);
			
			this.maxShearDisplay.text = this.Number2DisplayText(this.maxShear);
			
			if (this.DisplayType2D)
			{
				this.outputStressDisplay[2].text = "-";
				this.outputStressDisplay[5].text = "-";
				this.outputStressDisplay[6].text = "-";
				this.outputStressDisplay[7].text = "-";
				this.outputStressDisplay[8].text = "-";
				
				this.outputAlphaDisplay[2].text = "-";
				this.outputAlphaDisplay[5].text = "-";
				this.outputAlphaDisplay[6].text = "-";
				this.outputAlphaDisplay[7].text = "-";
				this.outputAlphaDisplay[8].text = "-";
				
				this.outputPrinDisplay[2].text = "-";
			}
		}
		private function Number2DisplayText(n:Number):String
		{
			var txt:String;
			if (n < 0)
			{
				n = MathEx.RoundDec(n, 3);
				txt = String(n.toFixed(5)).substr(0,6);
			}
			else
			{
				n = MathEx.RoundDec(n, 3);
				txt = String(n.toFixed(6)).substr(0,5);
			}
			if ( n == 0)
			{
				txt = "0";
			}
			return txt;
		}
	}
	
}
