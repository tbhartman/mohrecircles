﻿package MohreCircles.graphics
{
	import flash.display.Sprite;
	import flash.text.*;
	
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class Logo extends Sprite
	{
		[Embed(source='../../../lib/TBH.ttf', fontName = "TBH", unicodeRange="U+0030-U+0039,U+0041-U+005A,U+0061-U+007A,U+003A,U+002E,U+003D,U+0028,U+0029,U+002D,U+00AD", mimeType='application/x-font')]
		public static var TBH:Class;
		Font.registerFont(TBH);
		[Embed(source='../../../lib/symbol.ttf', fontName = "Symbol", unicodeRange="U+0030-U+0039,U+0041-U+005A,U+0061-U+007A,U+003A,U+002E,U+003D,U+0028,U+0029,U+002D,U+00AD", mimeType='application/x-font')]
		public static var Symbol:Class;
		Font.registerFont(Symbol);
		[Embed(source='../../../lib/CALIBRI.ttf', fontName = "Calibri", unicodeRange="U+0030-U+0039,U+0041-U+005A,U+0061-U+007A,U+003A,U+002E,U+003D,U+0028,U+0029,U+002D,U+00AD", mimeType='application/x-font')]
		public static var Calibri:Class;
		Font.registerFont(Calibri);
		
		//Icon Ellipses
		private var bigEx:int;
		private var bigEy:int;
		private var bigEw:int;
		private var bigEh:int;
		private var smallLEw:int;
		private var smallLEh:int;
		private var smallLEx:int;
		private var smallLEy:int;
		private var smallREw:int;
		private var smallREh:int;
		private var smallREx:int;
		private var smallREy:int;
		private var ratio:Number;
			
		public function Logo(width:Number, height:Number,ratio:Number, showText:Boolean = true)
		{
			this.ratio = ratio;
			bigEx = 0;
			bigEy = 7;
			bigEw = width;
			bigEh = height;
			smallLEw = bigEw*ratio;
			smallLEh = bigEh * ratio;// * 2 / 3;
			smallLEx = bigEx;
			smallLEy = bigEy + (bigEh/2) - (smallLEh/2);
			smallREw = bigEw - smallLEw;
			smallREh = bigEh * (1 - ratio);// * 2 / 3;
			smallREx = smallLEx + smallLEw;
			smallREy = smallLEy + (smallLEh/2) - (smallREh/2);
			
			this.graphics.lineStyle(1, 0x0000ff, 1);
			this.graphics.beginFill(0xccccff, 1);
			this.graphics.drawEllipse(bigEx, bigEy, bigEw, bigEh);
			this.graphics.endFill();
			this.graphics.beginFill(0xffffff, 1);
			this.graphics.drawEllipse(smallLEx, smallLEy, smallLEw, smallLEh);
			this.graphics.drawEllipse(smallREx, smallREy, smallREw, smallREh);
			this.graphics.endFill();
			//trace(bigEx, bigEy, bigEw, bigEh);
			//trace(smallLEx, smallLEy, smallLEw, smallLEh);
			//trace(smallREx, smallREy, smallREw, smallREh);
			
			if (showText)
			{
				var txtForm:TextFormat = new TextFormat();
				//txtForm.font = "TBH";
				txtForm.size = 36;
				txtForm.bold = true;
				txtForm.font = "TBH";
				
				var txtMo:TextField = new TextField();
				txtMo.x = 3;
				txtMo.y = 0;
				txtMo.selectable = false;
				txtMo.embedFonts = true;
				txtMo.defaultTextFormat = txtForm;
				txtMo.text = "Mo";
				this.addChild(txtMo);
				
				var txtH:TextField = new TextField();
				txtH.x = txtMo.x + txtMo.textWidth;
				txtH.y = txtMo.y;
				txtH.selectable = false;
				txtH.embedFonts = true;
				txtH.defaultTextFormat = txtForm;
				txtH.textColor = 0x0000ff;
				txtH.text = "h";
				this.addChild(txtH);
				
				var txtRe:TextField = new TextField();
				txtRe.x = txtH.x + txtH.textWidth;
				txtRe.y = txtMo.y;
				txtRe.selectable = false;
				txtRe.embedFonts = true;
				txtRe.defaultTextFormat = txtForm;
				txtRe.text = "re";
				this.addChild(txtRe);
				
				var txtCircle:TextField = new TextField();
				txtCircle.x = 0;
				txtCircle.y = txtMo.y + txtForm.size - 5;
				txtCircle.selectable = false;
				txtCircle.embedFonts = true;
				txtCircle.defaultTextFormat = txtForm;
				txtCircle.autoSize = TextFieldAutoSize.LEFT;
				txtCircle.text = "Circles";
				this.addChild(txtCircle);
			}
		}
		
	}
	
}
