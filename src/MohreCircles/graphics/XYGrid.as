﻿package MohreCircles.graphics
{
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.*;
	import MohreCircles.MathEx;
	
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class XYGrid extends Sprite
	{
		[Embed(source='../../../lib/times.ttf', fontName = "TimesNewRoman", mimeType='application/x-font')]
		public static var TimesNewRoman:Class;
		Font.registerFont(TimesNewRoman);
		
		private var displayType2D:Boolean;
		public function set DisplayType2D(b:Boolean):void
		{
			this.displayType2D = b;
		}
		public function get DisplayType2D():Boolean
		{
			return this.displayType2D;
		}
		private var height1:int = 320;
		private var width1:int = 370;
		
		private var xmin:Number = -10;
		private var xmax:Number = 10;
		private var xscale:Number = 1;
		
		private var yaxis:Shape = new Shape();
		private var xaxis:Shape = new Shape();
		private var yaxis_hash:Shape = new Shape();
		private var xaxis_hash:Shape = new Shape();
		private var yaxis_grid:Shape = new Shape();
		private var xaxis_grid:Shape = new Shape();
		private var yaxis_labels:Sprite = new Sprite();
		private var xaxis_labels:Sprite = new Sprite();
		
		private var ymin:Number = -10;
		private var ymax:Number = 10;
		private var yscale:Number = 1;
		
		private var tempGrid:Shape = new Shape();
		
		private var _gridOn:Boolean = true;
		public function set gridOn(b:Boolean):void
		{
			this._gridOn = b;
		}
		public function get gridOn():Boolean
		{
			return this._gridOn;
		}
		private var _hashOn:Boolean = true;
		public function set hashOn(b:Boolean):void
		{
			this._hashOn = b;
		}
		public function get hashOn():Boolean
		{
			return this._hashOn;
		}
		private var _axesOn:Boolean = true;
		public function set axesOn(b:Boolean):void
		{
			this._axesOn = b;
		}
		public function get axesOn():Boolean
		{
			return this._axesOn;
		}
		private var _labelsOn:Boolean = true;
		public function set labelsOn(b:Boolean):void
		{
			this._labelsOn = b;
		}
		public function get labelsOn():Boolean
		{
			return this._labelsOn;
		}
		
		//sample
		private var textField:TextField = new TextField();
		
		public function XYGrid(type2d:Boolean = false):void
		{
			this.DisplayType2D = type2d;
			
			addChild(yaxis);
			addChild(xaxis);
			addChild(yaxis_hash);
			addChild(xaxis_hash);
			addChild(yaxis_labels);
			addChild(xaxis_labels);
			addChildAt(yaxis_grid, 1);
			addChildAt(xaxis_grid, 1);
			addChild(tempGrid);
			
		}
		public function Rescale(xmin:Number, xmax:Number, xscale:Number, ymin:Number, ymax:Number, yscale:Number,redraw:Boolean = true):void
		{
			// FIXED Axis number labels go to x.00000000000001 sometimes 11 Nov 08
			this.xmin = xmin;
			this.xmax = xmax;
			this.ymin = ymin;
			this.ymax = ymax;
			this.xscale = xscale;
			this.yscale = yscale;
			//trace(xmin, xmax, xscale, ymin, ymax, yscale);
			
			if (redraw)
			{
				this.Redraw();
			}
		}
		public function Redraw():void
		{
			
			yaxis.graphics.clear();
			yaxis_hash.graphics.clear();
			yaxis_grid.graphics.clear();
			var yAxisLoc:Number;
			if (xmin > 0)
			{
				yaxis.graphics.lineStyle(2, 0x888888);
				yaxis_hash.graphics.lineStyle(2, 0x888888);
				yAxisLoc = xmin;
			}
			else if (xmax < 0)
			{
				yaxis.graphics.lineStyle(2, 0x888888);
				yaxis_hash.graphics.lineStyle(2, 0x888888);
				yAxisLoc = xmax;
			}
			else
			{
				yaxis.graphics.lineStyle(2);
				yaxis_hash.graphics.lineStyle(2);
				yAxisLoc = 0;
			}
			if (this.axesOn)
			{
				yaxis.graphics.drawRect(Grid2This(yAxisLoc, 0).x, 2, 1, this.height1 - 2);
			}
			
			yaxis_grid.graphics.lineStyle(1, 0x999999);
			while (yaxis_labels.numChildren > 0)
			{
				//trace(yaxis_labels.numChildren);
				yaxis_labels.removeChildAt(0);
			}
			var yfirstHash:Number = ymin - (ymin % yscale);
			var yCounter:int = 0;
			var yaxisLabel:Array = new Array(int((ymax - ymin) / yscale));
			for ( i = yfirstHash; i <= ymax; i += yscale )
			{
				if (i == 0)
				{
					continue;
				}
				if (this.hashOn)
				{
					var y:Number = Grid2This(yAxisLoc + Pixels2GridLength(5,1,0).x, i).y;
					var hash_xP:Number = Grid2This(yAxisLoc + Pixels2GridLength(5,1,0).x, i).x;
					var hash_xM:Number = Grid2This(yAxisLoc + Pixels2GridLength(-5,1,0).x, i).x;
					yaxis_hash.graphics.moveTo(hash_xP, y);
					yaxis_hash.graphics.lineTo(hash_xM, y);
				}
				
				if (this.gridOn)
				{
					var grid_xP:Number = Grid2This(xmax, i).x;
					var grid_xM:Number = Grid2This(xmin, i).x;
					yaxis_grid.graphics.moveTo(grid_xP, y);
					yaxis_grid.graphics.lineTo(grid_xM, y);
				}
				
				if (this.labelsOn)
				{
					var txtForm:TextFormat = new TextFormat();
					txtForm.font = "TimesNewRoman";
					txtForm.color = 0x990000;
					
					yaxisLabel[yCounter] = new TextField();
					yaxisLabel[yCounter].defaultTextFormat = txtForm;
					yaxisLabel[yCounter].embedFonts = true;
					yaxisLabel[yCounter].maxChars = 4;
					yaxisLabel[yCounter].text = MathEx.round(i, yscale).toString();
					if (this.DisplayType2D)
					{
						yaxisLabel[yCounter].text = (-1 * MathEx.round(i, yscale)).toString();
					}
					if (yaxisLabel[yCounter].text.length > 5)
					{
						yaxisLabel[yCounter].text = yaxisLabel[yCounter].text.substr(0, 5);
					}
					yaxisLabel[yCounter].selectable = false;
					yaxisLabel[yCounter].x = Grid2This(yAxisLoc + Pixels2GridLength(5, 1, 0).x, 0).x;
					yaxisLabel[yCounter].y = Grid2This(0, i + Pixels2GridLength(2, 0, 1).y).y;
					yaxisLabel[yCounter].width = yaxisLabel[yCounter].textWidth + 4;
					yaxisLabel[yCounter].height = yaxisLabel[yCounter].textHeight + 4;
					yaxis_labels.addChild(yaxisLabel[yCounter]);
				}
				
				yCounter++;
			}

			xaxis.graphics.clear();
			xaxis_hash.graphics.clear();
			xaxis_grid.graphics.clear();
			xaxis.graphics.lineStyle(2);
			var xAxisLoc:Number = Grid2This(0, 0).y
			if (ymin > 0)
			{
				xaxis.graphics.lineStyle(2, 0x888888);
				xaxis_hash.graphics.lineStyle(2, 0x888888);
				xAxisLoc = ymin;
			}
			else if (ymax < 0)
			{
				xaxis.graphics.lineStyle(2, 0x888888);
				xaxis_hash.graphics.lineStyle(2, 0x888888);
				xAxisLoc = ymax;
			}
			else
			{
				xaxis.graphics.lineStyle(2);
				xaxis_hash.graphics.lineStyle(2);
				xAxisLoc = 0;
			}
			if (this.axesOn)
			{
				xaxis.graphics.drawRect(1, Grid2This(0, xAxisLoc).y, this.width1 - 2, 1);
			}
			
			var i:Number;
			xaxis_hash.graphics.lineStyle(2, 0x000000);
			xaxis_grid.graphics.lineStyle(1, 0x999999);
			while (xaxis_labels.numChildren > 0)
			{
				//trace(xaxis_labels.numChildren);
				xaxis_labels.removeChildAt(0);
			}
			var xfirstHash:Number = xmin - (xmin % xscale);
			var xCounter:int = 0;
			var xaxisLabel:Array = new Array(int((xmax - xmin) / xscale));
			for (i = xfirstHash; i < xmax; i += xscale)
			{
				if (i == 0)
				{
					continue;
				}
				if (this.hashOn)
				{
					var x:Number = Grid2This(i, Pixels2GridLength(5,0,1).y).x;
					var hash_yP:Number = Grid2This(i, xAxisLoc + Pixels2GridLength(5,0,1).y).y;
					var hash_yM:Number = Grid2This(i, xAxisLoc + Pixels2GridLength(-5,0,1).y).y;
					xaxis_hash.graphics.moveTo(x, hash_yP);
					xaxis_hash.graphics.lineTo(x, hash_yM);
				}
				
				if (this.gridOn)
				{
					var grid_yP:Number = Grid2This(i, ymax).y;
					var grid_yM:Number = Grid2This(i, ymin).y;
					xaxis_grid.graphics.moveTo(x, grid_yP);
					xaxis_grid.graphics.lineTo(x, grid_yM);
				}
				
				if (this.labelsOn)
				{
					//var txtForm:TextFormat = new TextFormat();
					//txtForm.font = "TimesNewRoman";
					//txtForm.color = 0x990000;
					
					xaxisLabel[xCounter] = new TextField();
					xaxisLabel[xCounter].defaultTextFormat = txtForm;
					xaxisLabel[xCounter].embedFonts = true;
					xaxisLabel[xCounter].maxChars = 4;
					xaxisLabel[xCounter].text = MathEx.round(i, xscale).toString();
					if (xaxisLabel[xCounter].text.length > 5)
					{
						xaxisLabel[xCounter].text = xaxisLabel[xCounter].text.substr(0, 5);
					}
					//trace(xaxisLabel[xCounter].text);
					xaxisLabel[xCounter].selectable = false;
					//xaxisLabel[xCounter].rotation = -90;
					xaxisLabel[xCounter].width = xaxisLabel[xCounter].textWidth + 4;
					xaxisLabel[xCounter].height = xaxisLabel[xCounter].textHeight + 4;
					xaxisLabel[xCounter].name = xCounter.toString();
					xaxisLabel[xCounter].x = Grid2This(i - Pixels2GridLength(xaxisLabel[xCounter].width / 2, 1, 0).x, xAxisLoc).x;
					xaxisLabel[xCounter].y = Grid2This(0, xAxisLoc + Pixels2GridLength(-5, 0, 1).y).y;
					xaxis_labels.addChild(xaxisLabel[xCounter]);
				}
				
				xCounter++;
			}

		}
		public function Parent2Grid(x:Number, y:Number):Coordinate
		{
			return this.This2Grid(x - this.x, y - this.y);
		}
		public function Grid2Parent(x:Number, y:Number):Coordinate
		{
			return new Coordinate(Grid2This(x,y).x + this.x, Grid2This(x,y).y + this.y);
		}
		public function GridLength2Pixels(n:Number,x:Number,y:Number):Coordinate
		{
			var resultCoord:Coordinate = new Coordinate();
			var pixelsPerGrid:Coordinate = new Coordinate();
			pixelsPerGrid.x = this.width1 / (xmax - xmin);
			pixelsPerGrid.y = this.height1 / (ymax - ymin);
			resultCoord.x = n * pixelsPerGrid.x;
			resultCoord.y = n * pixelsPerGrid.y;
			return resultCoord;
		}
		public function Pixels2GridLength(n:Number, x:Number, y:Number ):Coordinate
		{
			var resultCoord:Coordinate = new Coordinate();
			var gridLengthPerPixel:Coordinate = new Coordinate();
			gridLengthPerPixel.x = (xmax - xmin) / this.width1;
			gridLengthPerPixel.y = (ymax - ymin) / this.height1;
			resultCoord.x = n * gridLengthPerPixel.x;
			resultCoord.y = n * gridLengthPerPixel.y;
			return resultCoord;
		}
		public function This2Grid(x:Number, y:Number):Coordinate
		{
			var zerox:Number = (this.width1 / (xmax - xmin)) * ( -xmin);//+(this.width1-GridLength2Pixels(xmax-xmin))/2;
			var zeroy:Number = (this.height1 / (ymax - ymin)) * ( -ymin);
			var coord:Coordinate = this.Grid2This(0, 0);
			return new Coordinate(Pixels2GridLength(x - zerox,1,0).x, -Pixels2GridLength(y - zeroy,0,1).y);
		}
		private function Grid2This(x:Number, y:Number):Coordinate
		{
			var zerox:Number = (this.width1 / (xmax - xmin)) * ( -xmin);//+(this.width1-GridLength2Pixels(xmax-xmin))/2;
			var zeroy:Number = (this.height1 / (ymax - ymin)) * ( -ymin);
			return new Coordinate(GridLength2Pixels(x,1,0).x + zerox, -GridLength2Pixels(y,0,1).y + zeroy);
		}
	}
	
}
