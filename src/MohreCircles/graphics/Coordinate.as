﻿package MohreCircles.graphics
{
	
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class Coordinate
	{
		public var x:Number = new Number();
		public var y:Number = new Number();
		
		public function Coordinate(x:Number = 0, y:Number = 0)
		{
			this.x = x;
			this.y = y;
		}
		//public override function ToString():String
		//{
			//return this.x.toString + ", " + this.y.toString;
		//}
		
	}
	
}
