﻿package MohreCircles.graphics
{
	import flash.display.Sprite;
	import flash.events.ActivityEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.events.TextEvent;
	import flash.text.*;
	import MohreCircles.math.*;
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class InputArea extends Sprite
	{
		private var displayType2D:Boolean;
		public function set DisplayType2D(b:Boolean):void
		{
			this.displayType2D = b;
		}
		public function get DisplayType2D():Boolean
		{
			return this.displayType2D;
		}
		private var tensor:Tensor = new Tensor();
		private var componentsDisplay:Array = new Array(9);
		
		//Sample variables
		private const sliderWithText:SliderWithText = new SliderWithText();
		
		public function InputArea(type2d:Boolean = false)
		{
			this.DisplayType2D = type2d;
			this.Init();
		}
		
		private function Init():void
		{
			for (var i:int = 0; i < this.componentsDisplay.length; i++)
			{
				componentsDisplay[i] = new SliderWithText(130,30);
				componentsDisplay[i].name = encodeComponentName(i);
				componentsDisplay[i].addEventListener(InputAreaEvent.VALUE_CHANGE, ComponentValueChanged);
				
			}
			{
				this.addChildAt(componentsDisplay[0],0);
				this.addChildAt(componentsDisplay[1],1);
				this.addChildAt(componentsDisplay[4],0);
				if (!this.DisplayType2D)
				{
					this.addChildAt(componentsDisplay[2], 2);
					this.addChildAt(componentsDisplay[5],4);
					this.addChildAt(componentsDisplay[8],0);
				}
				
				componentsDisplay[0].x = 5;
				componentsDisplay[1].x = 5;
				componentsDisplay[2].x = 135;
				componentsDisplay[4].x = 135;
				componentsDisplay[5].x = 265;
				componentsDisplay[8].x = 265;
				if (this.DisplayType2D)
				{
					componentsDisplay[0].x = 50;
					componentsDisplay[1].x = 135;
					componentsDisplay[4].x = 225;
				}
				
				componentsDisplay[1].y = 40;
				componentsDisplay[2].y = 40;
				componentsDisplay[5].y = 40;
				componentsDisplay[0].y = 5;
				componentsDisplay[4].y = 5;
				componentsDisplay[8].y = 5;
				if (this.DisplayType2D)
				{
					componentsDisplay[0].y = 5;
					componentsDisplay[1].y = 40;
					componentsDisplay[4].y = 5;
				}
				
				componentsDisplay[0].TextLabel = "XX";
				componentsDisplay[1].TextLabel = "XY";
				componentsDisplay[2].TextLabel = "XZ";
				componentsDisplay[4].TextLabel = "YY";
				componentsDisplay[5].TextLabel = "YZ";
				componentsDisplay[8].TextLabel = "ZZ";

			}
		}
		private function encodeComponentName(i:int):String
		{
			return Tensor.Num2I(i).toString() + "," + Tensor.Num2J(i).toString();
		}
		private function decodeComponentName(str:String):int
		{
			var splitArray:Array = str.split(",");
			var iString:String = splitArray[0];
			var jString:String = splitArray[1];
			return Tensor.IJ2Num(new int(iString), new int(jString));
		}
		private function ComponentValueChanged(e:InputAreaEvent):void
		{
			var n:int = this.decodeComponentName(e.target.name);
			var i:int = Tensor.Num2I(n);
			var j:int = Tensor.Num2J(n);
			//trace("**-----**");
			//trace("Name: ", e.target.name);
			//trace("Spot: ", i, ",", j);
			this.DispatchValueChangedEvent(i, j);
		}
		public function getComponentValue(i:int, j:int):Number
		{
			return new Number(this.componentsDisplay[Tensor.IJ2Num(i, j)].value);
		}
		public function setComponentValue(i:int, j:int, setValue:Number):void
		{
			this.componentsDisplay[Tensor.IJ2Num(i, j)].value = setValue;
			var e:KeyboardEvent = new KeyboardEvent(KeyboardEvent.KEY_UP);
			this.DispatchValueChangedEvent(i, j);
		}
		private function DispatchValueChangedEvent(i:int, j:int):void
		{
			var event:InputAreaEvent = new InputAreaEvent(InputAreaEvent.VALUE_CHANGE);
			event.i = i;
			event.j = j;
			event.value = this.getComponentValue(event.i, event.j);
			this.dispatchEvent(event);
		}
	}
	
}
