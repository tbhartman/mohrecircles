﻿package MohreCircles.graphics
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class IntroAnimEvent extends Event
	{
		public var arrayNum:uint = new uint();
		
		public static const END:String = "end";
		
		public function IntroAnimEvent(type:String)
		{
			super(type);
		}
		
	}
	
}
