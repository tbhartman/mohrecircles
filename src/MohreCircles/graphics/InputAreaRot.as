﻿package MohreCircles.graphics
{
	import flash.display.Sprite;
	import flash.events.ActivityEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.events.TextEvent;
	import flash.text.*;
	import MohreCircles.math.*;
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class InputAreaRot extends Sprite
	{
		private var angle:Array = new Array(3);
		private var componentsDisplay:Array = new Array(3);
		
		//Sample variables
		private const sliderWithText:SliderWithText = new SliderWithText();
		
		public function InputAreaRot()
		{
			this.Init();
		}
		
		private function Init():void
		{
			for (var i:int = 0; i < this.componentsDisplay.length; i++)
			{
				componentsDisplay[i] = new SliderWithText(130,30);
				componentsDisplay[i].name = i.toString();
				componentsDisplay[i].addEventListener(InputAreaEvent.VALUE_CHANGE, ComponentValueChanged);
				
			}
			{
				this.addChildAt(componentsDisplay[0],0);
				this.addChildAt(componentsDisplay[1],1);
				this.addChildAt(componentsDisplay[2],2);
				
				componentsDisplay[0].x = 5;
				componentsDisplay[1].x = 135;
				componentsDisplay[2].x = 265;
				
				componentsDisplay[0].y = 20;
				componentsDisplay[1].y = 20;
				componentsDisplay[2].y = 20;
				
				componentsDisplay[0].TextLabel = "Yaw";
				componentsDisplay[1].TextLabel = "Pitch";
				componentsDisplay[2].TextLabel = "Roll";

			}
		}
		private function ComponentValueChanged(e:InputAreaEvent):void
		{
			var n:int = e.target.name;
			this.DispatchValueChangedEvent(n);
		}
		public function getComponentValue(i:int):Number
		{
			return new Number(this.componentsDisplay[i].value);
		}
		public function setComponentValue(i:int, setValue:Number):void
		{
			this.componentsDisplay[i].value = setValue;
			var e:KeyboardEvent = new KeyboardEvent(KeyboardEvent.KEY_UP);
			this.DispatchValueChangedEvent(i);
		}
		private function DispatchValueChangedEvent(i:int):void
		{
			var event:InputAreaEvent = new InputAreaEvent(InputAreaEvent.VALUE_CHANGE);
			event.i = i;
			event.value = this.getComponentValue(event.i);
			this.dispatchEvent(event);
		}
	}
	
}
