﻿package MohreCircles.graphics
{
	import flash.display.Sprite;
	import flash.events.ActivityEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.events.TextEvent;
	import flash.text.*;
	import MohreCircles.math.*;
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class TensorDisplay extends Sprite
	{
		private var tensor:Tensor = new Tensor();
		private var componentsDisplay:Array = new Array(9);
		private var labelField:TextField = new TextField();
		
		public var Label:String = new String();
		//Sample variables
		private const textField:TextField = new TextField();
		
		public function TensorDisplay(t:Tensor,Label:String)
		{
			for (var i:int = 0; i < this.componentsDisplay.length; i++)
			{
				componentsDisplay[i] = new TextField();
				componentsDisplay[i].autoSize = TextFieldAutoSize.NONE;
				componentsDisplay[i].text = "0.0";
				
				addChild(componentsDisplay[i]);
			}
			{
				componentsDisplay[0].visible = true;
				componentsDisplay[1].visible = true;
				componentsDisplay[2].visible = true;
				componentsDisplay[3].visible = false;
				componentsDisplay[4].visible = true;
				componentsDisplay[5].visible = true;
				componentsDisplay[6].visible = false;
				componentsDisplay[7].visible = false;
				componentsDisplay[8].visible = true;
				
				componentsDisplay[0].x = 10;
				componentsDisplay[1].x = 30;
				componentsDisplay[2].x = 50;
				componentsDisplay[3].x = 10;
				componentsDisplay[4].x = 30;
				componentsDisplay[5].x = 50;
				componentsDisplay[6].x = 10;
				componentsDisplay[7].x = 30;
				componentsDisplay[8].x = 50;
				
				componentsDisplay[0].y = 0;
				componentsDisplay[1].y = 0;
				componentsDisplay[2].y = 0;
				componentsDisplay[3].y = 10;
				componentsDisplay[4].y = 10;
				componentsDisplay[5].y = 10;
				componentsDisplay[6].y = 20;
				componentsDisplay[7].y = 20;
				componentsDisplay[8].y = 20;
			}
			
			this.addChild(this.labelField);
			this.Label = Label;
			this.labelField.x = 2;
			this.labelField.y = 10;
			this.labelField.text = this.Label;
			
			
		}
		public function Update():void
		{
			this.textField.text = this.NumberToText(this.tensor.getComponent(1, 1));
		}
		private function NumberToText(n:Number):String
		{
			var result:String = String(n);
			if (result.search(".") > 0 && result.length > 5)
			{
				result = result.substr(0, 5);
			}
			else
			{
				result = result.substr(0, 4);
			}
			return result;
		}
	}
	
}
