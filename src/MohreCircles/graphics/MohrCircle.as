﻿package MohreCircles.graphics
{
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.Transform;
	import MohreCircles.ddd.CoordinateSystem;
	import MohreCircles.ddd.SpaceVector;
	import MohreCircles.ddd.VMath;
	import MohreCircles.math.MohrCalcs;
	import MohreCircles.math.Tensor;
	import MohreCircles.MathEx;
	import flash.text.*;
	import MohreCircles.PushButton;
	import MohreCircles.PushButtonEvent;
	
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class MohrCircle extends Sprite
	{
		[Embed(source='../../../lib/CALIBRI.ttf', fontName = "Calibri", unicodeRange="U+0030-U+0039,U+0041-U+005A,U+0061-U+007A,U+003A,U+002E,U+003D,U+0028,U+0029,U+002D,U+00AD", mimeType='application/x-font')]
		public static var Calibri:Class;
		Font.registerFont(Calibri);
		
		private var displayType2D:Boolean;
		public function set DisplayType2D(b:Boolean):void
		{
			this.displayType2D = b;
		}
		public function get DisplayType2D():Boolean
		{
			return this.displayType2D;
		}
		private var arc2d:Sprite;
		private var arc2d2:Sprite;
		private var arc3d:Sprite;
		private var grid:XYGrid = new XYGrid();
		
		public var mohrMath:MohrCalcs = new MohrCalcs();
		
		private var circles:Array = new Array(3);
		private var circleShade:Sprite = new Sprite();
		private var stressPoints:Sprite = new Sprite();
		
		public var setWidth:int = new int();
		public var setHeight:int = new int();
		private var bodyStress:Tensor = new Tensor;
		public function get AlphaTensor():Tensor
		{
			return this.mohrMath.AlphaTensor;
		}
		
		private var type:String;
		public function set Type(setValue:String):void
		{
			this.type = setValue;
		}
		public function get Type():String
		{
			return this.type;
		}
		
		private var border:Sprite = new Sprite();
		public function set Border(setValue:Boolean):void
		{
			
		}
		
		//Sample Variables
		private var shape:Shape = new Shape();
		public function get EigenValues():Array
		{
			return mohrMath.sigma;
		}
		public function get EigenVectors():Array
		{
			return mohrMath.eigenVectors;
		}
		private var butShow3dGen:PushButton = new PushButton();
		public function get ButShow3dGen():PushButton
		{
			return this.butShow3dGen;
		}
		public function set ButShow3dGen(p:PushButton):void
		{
			this.butShow3dGen = p;
		}
		private var Show3dGenOverlay:Sprite = new Sprite();
		
		public function MohrCircle(type2d:Boolean = false)
		{
			var txtForm:TextFormat = new TextFormat();
			txtForm.font = "Calibri";
			txtForm.size = 16;
			txtForm.bold = true;
			txtForm.letterSpacing = 7;
			
			var txtHorzLabel:TextField = new TextField();
			txtHorzLabel.defaultTextFormat = txtForm;
			txtHorzLabel.autoSize = TextFieldAutoSize.LEFT
			txtHorzLabel.embedFonts = true;
			txtHorzLabel.rotation = -90;
			txtHorzLabel.selectable = false;
			txtHorzLabel.text = "Shear Stress";
			txtHorzLabel.x = 0;
			txtHorzLabel.y = 240;
			this.addChild(txtHorzLabel);
			
			var txtVertLabel:TextField = new TextField();
			txtVertLabel.defaultTextFormat = txtForm;
			txtVertLabel.autoSize = TextFieldAutoSize.LEFT
			txtVertLabel.embedFonts = true;
			txtVertLabel.selectable = false;
			txtVertLabel.text = "Normal Stress";
			txtVertLabel.x = 115;
			txtVertLabel.y = 326;
			this.addChild(txtVertLabel);
			
			this.DisplayType2D = type2d;
			
			this.grid = new XYGrid(this.DisplayType2D);
			addChild(grid);
			for (var i:int = 0; i < circles.length; i++)
			{
				circles[i] = new Shape();
				addChild(circles[i]);
			}
			addChildAt(circleShade, 0);
			if (this.DisplayType2D)
			{
				arc2d2 = new Sprite();
				this.addChildAt(arc2d2,0);
				arc2d = new Sprite();
				this.addChildAt(arc2d,0);
			}
			else
			{
				arc3d = new Sprite();
				this.addChild(arc3d);
				
			}
			addChild(stressPoints);
			grid.x = 25;
			grid.y = 5;
			
			if (!this.DisplayType2D)
			{
				//Show 3d Gen
				{
					var txtFormSmall:TextFormat = new TextFormat();
					txtFormSmall.font = "Calibri";
					txtFormSmall.size = "10";
					txtFormSmall.italic = false;
					txtFormSmall.bold = true;

					var txt:TextField = new TextField();
					Show3dGenOverlay.addChild(txt);
					txt.defaultTextFormat = txtFormSmall;
					txt.text = "3D";
					txt.embedFonts = true;
					txt.autoSize = TextFieldAutoSize.LEFT;
					txt.selectable = false;
					txt.x = 7;
					txt.y = 1;
					var txt2:TextField = new TextField();
					Show3dGenOverlay.addChild(txt2);
					txt2.defaultTextFormat = txtFormSmall;
					txt2.text = "Calcs";
					txt2.embedFonts = true;
					txt2.autoSize = TextFieldAutoSize.LEFT;
					txt2.selectable = false;
					txt2.x = 2;
					txt2.y = 11;
					
					//Show3dGenOverlay.graphics.lineStyle(3, 0, 1);
					//Show3dGenOverlay.graphics.moveTo(10, 25);
					//Show3dGenOverlay.graphics.lineTo(20, 5);
					//Show3dGenOverlay.graphics.lineTo(14, 8);
					//Show3dGenOverlay.graphics.moveTo(20, 5);
					//Show3dGenOverlay.graphics.lineTo(21.5, 11.5);
				}
				this.butShow3dGen = new PushButton(365,5,30,30,false,PushButton.SWITCH_WHEN_PRESSED);
				this.butShow3dGen.Overlay = Show3dGenOverlay;
				this.addChild(this.butShow3dGen);
			}
			
		}
		public function Update(t:Tensor,bx:SpaceVector,by:SpaceVector,bz:SpaceVector):void
		{
			var smallest:Number = 0.1;
			// FIXED Singularity when stresses are equal 11 Nov 08
			mohrMath.setTensor(t);
			var maxRadius:Number = (mohrMath.sigma[2] - mohrMath.sigma[0]) / 2;
			var xRange:Number = (mohrMath.sigma[2] - mohrMath.sigma[0]) * this.setWidth / this.setHeight;
			if (xRange < smallest || maxRadius < smallest)
			{
				xRange = smallest * (this.setWidth / this.setHeight) * 2;
				maxRadius = smallest;
			}
			var xmin:Number = (mohrMath.sigma[2] + mohrMath.sigma[0] - xRange) / 2;
			var xmax:Number = (mohrMath.sigma[2] + mohrMath.sigma[0] + xRange) / 2;
			var ymin:Number = -maxRadius;
			var ymax:Number = maxRadius;
			
			var xscale:Number = this.ConfigureScaleRange(xmax, xmin);
			//trace("*--*");
			var yscale:Number = this.ConfigureScaleRange(ymax, ymin);
			//trace(ymin, ymax, yscale);
			
			/*trace("---------");
			trace(xmax,xmin,"|",xscale);
			trace(ymax,ymin,"|",yscale);
			*/
			grid.Rescale(xmin, xmax, xscale, ymin, ymax, yscale);
			
			circleShade.graphics.clear();
			for (var i:int = 1; i < circles.length; i )
			{
				circles[i].graphics.clear();
				circles[i].graphics.lineStyle(1, 0x0000FF);
				if (this.DisplayType2D)
				{
					circles[i].graphics.lineStyle(1.2, 0x000ff);
				}
				var height:Number = grid.GridLength2Pixels(mohrMath.radii[i], 0, 1).y * 2;
				var width:Number = grid.GridLength2Pixels(mohrMath.radii[i], 1, 0).x * 2;
				var upperLeftX:Number = grid.Grid2Parent(mohrMath.centers[i], 0).x - width / 2;
				var upperLeftY:Number = grid.Grid2Parent(mohrMath.centers[i], 0).y - height / 2;
				//trace(mohrMath.sigma[0],mohrMath.sigma[1],mohrMath.sigma[2]);
				if (width > 0 && height > 0)
				{
					circles[i].graphics.drawEllipse(upperLeftX, upperLeftY, width, height);
				}
				
				if (this.DisplayType2D)
				{
					circleShade.graphics.beginFill(0x0000ff, 0);
					circleShade.graphics.drawEllipse(upperLeftX, upperLeftY, width, height);
					circleShade.graphics.endFill();
					break;
					
				}
				else
				{
					if (i == 1)
					{
						circleShade.graphics.beginFill(0x0000ff, 0.1);
						circleShade.graphics.drawEllipse(upperLeftX, upperLeftY, width, height);
						circleShade.graphics.endFill();
						i--;
					}
					else
					{
						circleShade.graphics.beginFill(0xffffff, 1);
						circleShade.graphics.drawEllipse(upperLeftX, upperLeftY, width, height);
						circleShade.graphics.endFill();
						if (i == 0)
						{
							i += 2;
							continue;
						}
						else if (i == 2)
						{
							i = circles.length;
							continue;
						}
					}
					
				}
				
			}
			
			this.UpdateBodyStress(bx, by, bz);
			
			var xx:Number = this.bodyStress.getComponent(1, 1);
			var yy:Number = this.bodyStress.getComponent(2, 2);
			var zz:Number = this.bodyStress.getComponent(3, 3);
			var xy:Number = this.bodyStress.getComponent(1, 2);
			var xz:Number = this.bodyStress.getComponent(1, 3);
			var yz:Number = this.bodyStress.getComponent(2, 3);
			//var signX:int = -MathEx.sign(xy) * MathEx.sign(xz);
			//var signY:int = MathEx.sign(xy) * MathEx.sign(yz);
			//var signZ:int = -MathEx.sign(xz) * MathEx.sign(yz);
			
			var signX:int = 1;
			var signY:int = 1;
			var signZ:int = 1;
			
			stressPoints.graphics.clear();
			var pointX:Coordinate;
			var pointY:Coordinate;
			var pointZ:Coordinate;
			if (!this.DisplayType2D)
			{
				pointX = grid.Grid2Parent(xx, signX * Math.sqrt(Math.pow(xy, 2) + Math.pow(xz, 2)));
				pointY = grid.Grid2Parent(yy, signY * Math.sqrt(Math.pow(xy, 2) + Math.pow(yz, 2)));
				pointZ = grid.Grid2Parent(zz, signZ * Math.sqrt(Math.pow(xz, 2) + Math.pow(yz, 2)));
				
				this.arc3d.graphics.clear();
				if (this.butShow3dGen.Status && (!MathEx.IsApproxEqual(xx,yy,5) || !MathEx.IsApproxEqual(xx,zz,5) || !MathEx.IsApproxEqual(yy,zz,5)))
				{
					var sp:SpaceVector = new SpaceVector();
					sp.x = this.mohrMath.AlphaTensor.getComponent(1, 1);
					sp.y = this.mohrMath.AlphaTensor.getComponent(1, 2);
					sp.z = this.mohrMath.AlphaTensor.getComponent(1, 3);
					var xA1:Number = VMath.Angle(this.mohrMath.eigenVectors[2], sp);
					var xA2:Number = VMath.Angle(this.mohrMath.eigenVectors[1], sp);
					var xA3:Number = VMath.Angle(this.mohrMath.eigenVectors[0], sp);
					if (xA1.toString() == NaN.toString())
					{
						xA1 = 0;
					}
					if (xA2.toString() == NaN.toString())
					{
						xA2 = 0;
					}
					if (xA3.toString() == NaN.toString())
					{
						xA3 = 0;
					}
					//trace(xA1*180/Math.PI, xA2*180/Math.PI, xA3*180/Math.PI);
					xA1 *= 2;
					xA2 *= 2;
					xA3 *= 2;
					xA1 = xA1;
					var xA2a:Number = xA2;
					var xA2b:Number = Math.PI - xA2;
					xA3 = Math.PI - xA3;
					var c1:Number = this.mohrMath.centers[0];
					var c2:Number = this.mohrMath.centers[1];
					var c3:Number = this.mohrMath.centers[2];
					var r1:Number = this.mohrMath.radii[0];
					var r2:Number = this.mohrMath.radii[1];
					var r3:Number = this.mohrMath.radii[2];
					
					var center3:Coordinate = new Coordinate(c3, 0);
					var center2:Coordinate = new Coordinate(c2, 0);
					var center1:Coordinate = new Coordinate(c1, 0);
					var center3g:Coordinate = this.grid.Grid2Parent(c3, 0);
					var center2g:Coordinate = this.grid.Grid2Parent(c2, 0);
					var center1g:Coordinate = this.grid.Grid2Parent(c1, 0);
					var coord31:Coordinate = new Coordinate(r1 * Math.cos(xA3) + c1, Math.abs(r1 * Math.sin(xA3)))
					var coord32:Coordinate = new Coordinate(r2 * Math.cos(xA3) + c2, Math.abs(r2 * Math.sin(xA3)))
					var coord21:Coordinate = new Coordinate(r1 * Math.cos(xA2a) + c1, Math.abs(r1 * Math.sin(xA2a)))
					var coord23:Coordinate = new Coordinate(r3 * Math.cos(xA2b) + c3, Math.abs(r3 * Math.sin(xA2b)))
					var coord12:Coordinate = new Coordinate(r2 * Math.cos(xA1) + c2, Math.abs(r2 * Math.sin(xA1)))
					var coord13:Coordinate = new Coordinate(r3 * Math.cos(xA1) + c3, Math.abs(r3 * Math.sin(xA1)))
					
					var coord31_a:Coordinate = new Coordinate(center1.x - r1 / 2, center1.y);
					var coord31_b:Coordinate = new Coordinate(center1.x + (coord31.x - center1.x) / 2, coord31.y / 2);
					var coord32_a:Coordinate = new Coordinate(center2.x - r2 / 2, center2.y);
					var coord32_b:Coordinate = new Coordinate(center2.x + (coord32.x - center2.x) / 2, coord32.y / 2);
					var coord21_a:Coordinate = new Coordinate(center1.x + r1 / 3, center1.y);
					var coord21_b:Coordinate = new Coordinate(center1.x + (coord21.x - center1.x) / 3, coord21.y / 3);
					var coord23_a:Coordinate = new Coordinate(center3.x - r3 / 3, center1.y);
					var coord23_b:Coordinate = new Coordinate(center3.x + (coord23.x - center3.x) / 3, coord23.y / 3);
					var coord12_a:Coordinate = new Coordinate(center2.x + r2 / 2.5, center1.y);
					var coord12_b:Coordinate = new Coordinate(center2.x + (coord12.x - center2.x) / 2.5, coord12.y / 2.5);
					var coord13_a:Coordinate = new Coordinate(center3.x + r3 / 2.5, center3.y);
					var coord13_b:Coordinate = new Coordinate(center3.x + (coord13.x - center3.x) / 2.5, coord13.y / 2.5);
					
					var coord31g:Coordinate = this.grid.Grid2Parent(r1 * Math.cos(xA3) + c1, Math.abs(r1 * Math.sin(xA3)))
					var coord32g:Coordinate = this.grid.Grid2Parent(r2 * Math.cos(xA3) + c2, Math.abs(r2 * Math.sin(xA3)))
					var coord21g:Coordinate = this.grid.Grid2Parent(r1 * Math.cos(xA2a) + c1, Math.abs(r1 * Math.sin(xA2a)))
					var coord23g:Coordinate = this.grid.Grid2Parent(r3 * Math.cos(xA2b) + c3, Math.abs(r3 * Math.sin(xA2b)))
					var coord12g:Coordinate = this.grid.Grid2Parent(r2 * Math.cos(xA1) + c2, Math.abs(r2 * Math.sin(xA1)))
					var coord13g:Coordinate = this.grid.Grid2Parent(r3 * Math.cos(xA1) + c3, Math.abs(r3 * Math.sin(xA1)))
					//var radius31:Number = Math.sqrt(Math.pow(center3.x - coord31.x, 2) + Math.pow(center3.y - coord31.y, 2));
					//var radius21:Number = Math.sqrt(Math.pow(center2.x - coord21.x, 2) + Math.pow(center2.y - coord21.y, 2));
					//var radius11:Number = Math.sqrt(Math.pow(center1.x - coord12.x, 2) + Math.pow(center1.y - coord12.y, 2));
					//var radius32:Number = Math.sqrt(Math.pow(center3.x - coord32.x, 2) + Math.pow(center3.y - coord32.y, 2));
					//var radius22:Number = Math.sqrt(Math.pow(center2.x - coord23.x, 2) + Math.pow(center2.y - coord23.y, 2));
					//var radius12:Number = Math.sqrt(Math.pow(center1.x - coord13.x, 2) + Math.pow(center1.y - coord13.y, 2));
					//var radius3:Number = (radius32 + radius31) / 2;
					//var radius2:Number = (radius22 + radius21) / 2;
					//var radius1:Number = (radius12 + radius11) / 2;
					
					this.arc3d.graphics.lineStyle(1, 0x0000ff, 0.6);
					//this.arc3d.graphics.drawCircle(center3.x,center3.y, radius3);
					this.DrawArc(this.arc3d, center3, coord31, coord32);
					this.arc3d.graphics.moveTo(center1g.x, center1g.y);
					this.arc3d.graphics.lineTo(coord31g.x, coord31g.y);
					this.arc3d.graphics.moveTo(center2g.x, center2g.y);
					this.arc3d.graphics.lineTo(coord32g.x, coord32g.y);
					this.DrawArc(this.arc3d, center1, coord31_a, coord31_b);
					this.DrawArc(this.arc3d, center2, coord32_a, coord32_b);
					//trace(coord31_a.x, coord31_a.y, coord31_b.x, coord31_b.y);
					
					this.arc3d.graphics.lineStyle(1, 0x00ff00, 0.6);
					//this.arc3d.graphics.drawCircle(center2.x,center2.y, radius2);
					this.DrawArc(this.arc3d, center2, coord21, coord23);
					this.arc3d.graphics.moveTo(center1g.x, center1g.y);
					this.arc3d.graphics.lineTo(coord21g.x, coord21g.y);
					this.arc3d.graphics.moveTo(center3g.x, center3g.y);
					this.arc3d.graphics.lineTo(coord23g.x, coord23g.y);
					this.DrawArc(this.arc3d, center1, coord21_b, coord21_a);
					this.DrawArc(this.arc3d, center3, coord23_a, coord23_b);
					
					this.arc3d.graphics.lineStyle(1, 0xff0000, 0.6);
					//this.arc3d.graphics.drawCircle(center1.x,center1.y, radius1);
					this.DrawArc(this.arc3d, center1, coord12, coord13);
					this.arc3d.graphics.moveTo(center2g.x, center2g.y);
					this.arc3d.graphics.lineTo(coord12g.x, coord12g.y);
					this.arc3d.graphics.moveTo(center3g.x, center3g.y);
					this.arc3d.graphics.lineTo(coord13g.x, coord13g.y);
					this.DrawArc(this.arc3d, center2, coord12_b, coord12_a);
					this.DrawArc(this.arc3d, center3, coord13_b, coord13_a);
				}
				
			}
			else
			{
				this.arc2d.graphics.clear();
				this.arc2d2.graphics.clear();
				pointX = grid.Grid2Parent(xx, -xy);
				pointY = grid.Grid2Parent(yy, xy);
				//trace(xx, xy, yy);
				//var pointZ:Coordinate = grid.Grid2Parent(zz, signZ * Math.sqrt(Math.pow(xz, 2) + Math.pow(yz, 2)));
				
				if (xx != yy && xy != 0)
				{
					//draw coordlines
					stressPoints.graphics.endFill();
					stressPoints.graphics.lineStyle(3, 0, 0.6);
					//stressPoints.graphics.moveTo(pointX.x, grid.Grid2Parent(0, 0).y);
					//stressPoints.graphics.lineTo(pointX.x, pointX.y);
					stressPoints.graphics.moveTo(pointX.x, pointX.y);
					stressPoints.graphics.lineTo(pointY.x, pointY.y);
					//stressPoints.graphics.lineTo(pointY.x, grid.Grid2Parent(0, 0).y);
					stressPoints.graphics.lineStyle(2, 0, 0.5);
					var pointXX:Coordinate = grid.Grid2Parent(t.getComponent(1, 1), -t.getComponent(1, 2));
					var pointYY:Coordinate = grid.Grid2Parent(t.getComponent(2, 2), t.getComponent(1, 2));
					var pointO:Coordinate = new Coordinate((pointXX.x + pointYY.x) / 2, grid.Grid2Parent(0, 0).y);
					//trace(pointO.x, pointO.y);
					stressPoints.graphics.moveTo(pointO.x, pointO.y);
					stressPoints.graphics.lineTo(pointXX.x, pointXX.y);
					stressPoints.graphics.moveTo(pointXX.x, pointXX.y);
					stressPoints.graphics.lineStyle(0, 0, 0);
					stressPoints.graphics.beginFill(0,0.5);
					stressPoints.graphics.drawCircle(pointXX.x, pointXX.y, 4);
					stressPoints.graphics.endFill();
					
					if (!(MathEx.IsApproxEqual(pointX.x,pointXX.x,5) && MathEx.IsApproxEqual(pointX.y,pointXX.y,5)))
					{
						//draw arc
						this.arc2d.graphics.lineStyle(2, 0, 0.5);
						var mag:Number = Math.sqrt(Math.pow(pointXX.x - pointO.x, 2) + Math.pow(pointXX.y - pointO.y, 2));
						this.arc2d.graphics.drawCircle(pointO.x, pointO.y, mag * 2 / 3);
						
						var arcX:Number = (pointO.x + (pointX.x - pointO.x) * 2 / 3);
						this.arc2d2.graphics.moveTo(pointO.x - mag,pointO.y - mag);
						this.arc2d2.graphics.lineStyle(0, 0, 0);
						this.arc2d2.graphics.beginFill(0xffffff, 1);
						this.arc2d2.graphics.drawRect(pointO.x - mag, pointO.y - mag, mag * 2, mag * 2);
						this.arc2d2.graphics.moveTo(pointO.x, pointO.y);
						this.arc2d2.graphics.lineTo(pointXX.x, pointXX.y);
						
						var cont:Boolean = false;
						var x:Number = pointXX.x;
						var y:Number = pointXX.y;
						var emergencyCount:int = 0;
						var offset:Number = 1;
						do
						{
							emergencyCount++;
							var sameQuad:Boolean;
							//trace(x, y,pointX.x,pointX.y);
							//if (pointX.x == pointXX.x && pointX.y == pointXX.y)
							//{
								//sameQuad = true;
							//}
							if (x >= pointO.x && y <= pointO.y)
							{	//1st quadrant
								if ((pointX.x >= pointO.x && pointX.y <= pointO.y) && (pointX.x <= x || pointX.y <= y))
								{
									sameQuad = true;
								}
								x = pointO.x - offset;
								y = pointO.y - mag;
							}
							else if (x <= pointO.x && y <= pointO.y)
							{	//2nd quadrant
								if ((pointX.x <= pointO.x && pointX.y <= pointO.y) && (pointX.x <= x || pointX.y >= y))
								{
									sameQuad = true;
								}
								x = pointO.x - mag;
								y = pointO.y + offset;
							}
							else if (x <= pointO.x && y >= pointO.y)
							{	//3rd quadrant
								if ((pointX.x <= pointO.x && pointX.y >= pointO.y) && (pointX.x >= x || pointX.y >= y))
								{
									sameQuad = true;
								}
								x = pointO.x + offset;
								y = pointO.y + mag;
							}
							else
							{	//4th quadrant
								if ((pointX.x >= pointO.x && pointX.y >= pointO.y - 1) && (pointX.x >= x || pointX.y <= y))
								{
									sameQuad = true;
								}
								x = pointO.x + mag;
								y = pointO.y - offset;
							}
							//trace("*--*");
							//trace(emergencyCount, x,y,pointO.x,pointO.y);
							if (sameQuad || emergencyCount > 4)
							{
								this.arc2d2.graphics.lineTo(pointX.x, pointX.y);
								cont = true;
							}
							else
							{
								this.arc2d2.graphics.lineTo(x,y);
							}
						} while (!cont)
						this.arc2d2.graphics.lineTo(pointO.x, pointO.y);
						this.arc2d2.graphics.endFill();
					}
				}
			}
			//trace(pointX.x, pointX.y);
			stressPoints.graphics.lineStyle(0, 0, 0);
			
			stressPoints.graphics.moveTo(pointX.x, pointX.y);
			stressPoints.graphics.beginFill(0xff0000,0.6);
			stressPoints.graphics.drawCircle(pointX.x, pointX.y, 4);
			stressPoints.graphics.endFill();
			
			if (!this.ButShow3dGen.Status)
			{
				stressPoints.graphics.moveTo(pointY.x, pointY.y);
				stressPoints.graphics.beginFill(0x00ff00,0.6);
				stressPoints.graphics.drawCircle(pointY.x, pointY.y, 4);
				stressPoints.graphics.endFill();
				if (!this.DisplayType2D)
				{
					stressPoints.graphics.beginFill(0x0000ff,0.6);
					stressPoints.graphics.drawCircle(pointZ.x, pointZ.y, 4);
					stressPoints.graphics.endFill();
				}
			}
		}
		private function ConfigureScaleRange(max:Number, min:Number):Number
		{
			var log:Number = Math.log(max - min) / Math.log(10);
			var logFloor:Number = Math.floor(log);
			var logR:Number = log - logFloor;
			/*trace("v---v");
			trace("max:", max);
			trace("min:", min);
			trace("log:", log);
			trace("logFloor:", logFloor);
			trace("logR:", logR);*/
			//trace(log, logFloor, logR);
			
			if (Math.log(1) / Math.log(10) <= logR && logR <= Math.log(2) / Math.log(10)) //between 1 and 2
			{
				return (Math.pow(10, logFloor - 1) * 2);
			}
			else if (Math.log(2) / Math.log(10) <= logR && logR <= Math.log(5) / Math.log(10))
			{
				return (Math.pow(10, logFloor - 1) * 5);
			}
			else //Math.log(5) <= logR <= log(10)
			{
				return Math.pow(10, logFloor);
			}
		}
		public function GetTransformedStress():Tensor
		{
			return this.bodyStress;
		}
		private function UpdateBodyStress(xAxis:SpaceVector, yAxis:SpaceVector, zAxis:SpaceVector):void
		{
			this.bodyStress = mohrMath.Transformation(xAxis, yAxis, zAxis);
		}
		private function DrawArc(sp:Sprite, center:Coordinate, start:Coordinate, finish:Coordinate):void
		{
			var c:Coordinate = center;
			var s:Coordinate = start;
			var f:Coordinate = finish;
			
			var sRel:Coordinate = new Coordinate(s.x - c.x, s.y - c.y);
			var fRel:Coordinate = new Coordinate(f.x - c.x, f.y - c.y);
			
			var r:Number = Math.sqrt(Math.pow(sRel.x, 2) + Math.pow(sRel.y, 2));
			
			var sAngle:Number = Math.atan2(sRel.y, sRel.x);
			var fAngle:Number = Math.atan2(fRel.y, fRel.x);
			
			if (sAngle < 0)
			{
				sAngle += Math.PI;
			}
			if (fAngle < 0)
			{
				fAngle += Math.PI;
			}
			
			var angle:Number = sAngle;
			sp.graphics.moveTo(this.grid.Grid2Parent(s.x,s.y).x, this.grid.Grid2Parent(s.x,s.y).y);
			do
			{
				//trace(angle * 180 / Math.PI);
				var coord:Coordinate = new Coordinate();
				coord.x = this.grid.Grid2Parent(r * Math.cos(angle) + c.x, r * Math.sin(angle) + c.y).x;
				coord.y = this.grid.Grid2Parent(r * Math.cos(angle) + c.x, r * Math.sin(angle) + c.y).y;
				
				sp.graphics.lineTo(coord.x, coord.y);
				
				angle -= 0.05;
			} while (angle > fAngle)
			
			sp.graphics.lineTo(this.grid.Grid2Parent(f.x,f.y).x, this.grid.Grid2Parent(f.x,f.y).y);
			
		}
	}
}
