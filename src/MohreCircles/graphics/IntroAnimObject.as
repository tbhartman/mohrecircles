﻿package MohreCircles.graphics
{
	import flash.display.Sprite;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import MohreCircles.PushButton;
	import MohreCircles.PushButtonEvent;
	
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class IntroAnimObject extends Sprite
	{
		private var timer:Timer;
		private var disp:Logo;
		private var arrayInt:uint;
		private var w:Number;
		private var h:Number;
		
		public function IntroAnimObject(y:Number,w:Number,h:Number,ratio:Number,speed:Number,arrayInt:uint)
		{
			this.arrayInt = arrayInt;
			this.disp = new Logo(w, h, ratio,false);
			this.w = w;
			this.h = h;
			this.x = -this.w;
			this.y = y;
			this.addChild(disp);
			this.timer = new Timer(1000 / (speed * 5), 0);
			this.timer.addEventListener(TimerEvent.TIMER, TimerEventHandler);
			this.timer.start();
		}
		private function TimerEventHandler(e:TimerEvent):void
		{
			this.x += 1;
			if (this.x >= 1200)
			{
				this.x = -this.w;
				this.y = Math.random() * (525 - this.h);
				//this.parent.removeChild(this);
			}
		}
		
	}
	
}
