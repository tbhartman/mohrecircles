﻿package MohreCircles.graphics
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class InputAreaEvent extends Event
	{
		public var i:int = new int();
		public var j:int = new int();
		public var value:Number = new Number();
		
		public static const VALUE_CHANGE:String = "value_change";
		
		public function InputAreaEvent(type:String)
		{
			super(type);
		}
		
	}
	
}
