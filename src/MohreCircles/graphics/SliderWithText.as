﻿package MohreCircles.graphics
{
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.text.*;
	import flash.ui.Mouse;
	import flash.utils.Timer;
	import MohreCircles.MathEx;
	
	/**
	 * ...
	 * @author Tim Hartman
	 */
	public class SliderWithText extends Sprite
	{
		[Embed(source='../../../lib/CALIBRI.ttf', fontName = "Calibri", unicodeRange="U+0030-U+0039,U+0041-U+005A,U+0061-U+007A,U+003A,U+002E,U+003D,U+0028,U+0029,U+002D,U+00AD", mimeType='application/x-font')]
		public static var Calibri:Class;
		Font.registerFont(Calibri);
		[Embed(source='../../../lib/symbol.ttf', fontName = "Symbol", unicodeRange="U+0030-U+0039,U+0041-U+005A,U+0061-U+007A,U+003A,U+002E,U+003D,U+0028,U+0029,U+002D,U+00AD", mimeType='application/x-font')]
		public static var Symbol:Class;
		Font.registerFont(Symbol);
		[Embed(source='../../../lib/times.ttf', fontName = "TimesNewRoman", unicodeRange="U+0030-U+0039,U+0041-U+005A,U+0061-U+007A,U+003A,U+002E,U+003D,U+0028,U+0029,U+002D,U+00AD", mimeType='application/x-font')]
		public static var TimesNewRoman:Class;
		Font.registerFont(TimesNewRoman);
		[Embed(source='../../../lib/timesi.ttf', fontName = "TimesNewRomanI", unicodeRange="U+0030-U+0039,U+0041-U+005A,U+0061-U+007A,U+003A,U+002E,U+003D,U+0028,U+0029,U+002D,U+00AD", fontStyle = "Italic", mimeType='application/x-font')]
		public static var TimesNewRomanI:Class;
		Font.registerFont(TimesNewRomanI);
		
		private var textDisplay:TextField;
		private var sliderTrack:Shape = new Shape();
		private var slider:Sprite = new Sprite();
		private var sliderLocalStart:int = new int();
		private var valuePrivate:Number = new Number();
		
		private var displayWidth:Number = new Number();
		private var displayHeight:Number = new Number();
		
		private var sliderTimer:Timer;
		private var sliderYieldMax:Number = 500;
		private var sliderYieldMin:Number = .01;
		
		private var textDecimals:int = 5;
		
		private var textLabel:TextField = new TextField();
		
		private var limitValue:Boolean = true;
		private var maxValue:Number = 1000.0;
		private var minValue:Number = -1000.0;
		
		public function SliderWithText(width:Number = 150, height:Number = 30)
		{
			var txtForm:TextFormat = new TextFormat();
			txtForm.bold = true;
			txtForm.color = 0x0;
			txtForm.font = "Calibri";
			
			this.textDisplay = new TextField();
			this.textDisplay.defaultTextFormat = txtForm;
			this.textDisplay.autoSize = TextFieldAutoSize.CENTER;
			this.textDisplay.text = "0.0";
			this.textDisplay.embedFonts = true;
			this.textDisplay.type = TextFieldType.INPUT;
			this.textDisplay.restrict = "0123456789.\\-";
			this.textDisplay.maxChars = textDecimals;
			this.textDisplay.addEventListener(KeyboardEvent.KEY_UP, KeyUpHandler);
			this.addChild(this.textDisplay);
			this.graphics.lineStyle(1.2, 0x000055);
			this.graphics.beginFill(0x0000ff, 0.1);
			this.graphics.drawRect(46, 0, 38, 16);
			this.graphics.endFill();
			this.textDisplay.background = false;
			this.textDisplay.backgroundColor = 0xddddff;
			this.textDisplay.height = 7;
			this.textDisplay.x = 16;
			this.textDisplay.y = -1;
			this.textDisplay.width = 100;
			
			this.addChild(sliderTrack);
			this.displayWidth = width;
			this.displayHeight = height;
			
			sliderTrack.y = 2 / 3 * this.displayHeight;
			sliderTrack.graphics.lineStyle(3, 0x2255ff);
			sliderTrack.graphics.moveTo(5, 0);
			sliderTrack.graphics.lineTo(this.displayWidth - 5, 0);
			
			this.addChild(slider);
			this.DrawSlider(false);
			this.slider.y = sliderTrack.y;
			this.ResetSlider();
			
			this.slider.addEventListener(MouseEvent.MOUSE_DOWN, SliderMouseDownHandler);
			this.slider.addEventListener(MouseEvent.MOUSE_OVER, SliderMouseOverHandler);
			
			var txtForm2:TextFormat = new TextFormat();
			txtForm2.font = "TimesNewRomanBI";
			txtForm2.bold = true;
			txtForm2.italic = true;
			txtForm2.color = 0x222222;
			
			this.addChildAt(textLabel, 0);
			textLabel.selectable = false;
			//textLabel.antiAliasType = AntiAliasType.ADVANCED;
			textLabel.embedFonts = true;
			textLabel.defaultTextFormat = txtForm2;
			textLabel.setTextFormat(txtForm2);
			//textLabel.width = textLabel.textWidth + 3;
			//textLabel.height = textLabel.textHeight + 3;
		}
		private function SliderMouseOverHandler(e:MouseEvent):void
		{
			this.slider.addEventListener(MouseEvent.MOUSE_OUT, SliderMouseOutHandler);
			this.DrawSlider(true);
		}
		private function SliderMouseOutHandler(e:MouseEvent):void
		{
			this.slider.removeEventListener(MouseEvent.MOUSE_OUT, SliderMouseOutHandler);
			this.DrawSlider(false);
		}
		private function DrawSlider(highlighted:Boolean):void
		{
			this.slider.graphics.clear();
			if (!highlighted)
			{
				this.slider.graphics.lineStyle(1, 0x000000,0);
			}
			else
			{
				this.slider.graphics.lineStyle(1, 0xff0000,1);
			}
			this.slider.graphics.beginFill(0x222299,0.8);
			this.slider.graphics.moveTo(0, 5);
			this.slider.graphics.lineTo(0, 10);
			this.slider.graphics.lineTo(8, 10);
			this.slider.graphics.lineTo(8, 5);
			this.slider.graphics.lineTo(4, 0);
			this.slider.graphics.lineTo(0, 5);
			this.slider.graphics.endFill();
		}
		private function ResetSlider():void
		{
			this.slider.x = this.sliderTrack.width / 2;
		}
		private function BeginSliderTimer():void
		{
			this.sliderTimer = new Timer(100);
			this.sliderTimer.addEventListener(TimerEvent.TIMER, SliderTimerEventHandler);
			this.sliderTimer.start();
		}
		private function SliderTimerEventHandler(e:TimerEvent):void
		{
			var percentage:Number = ((2 * this.slider.x) / this.sliderTrack.width ) - 1;
			var addAmount:Number = this.SliderPercent2Yield(percentage,this.sliderTimer.delay);
			this.value += addAmount;
			//this.DispatchValueChangeEvent();
		}
		private function SliderPercent2Yield(p:Number,delay:Number = 1000):Number
		{
			var sign:int;
			if (p == 0) { return 0 };
			if (p < 0) { sign = -1 } else { sign = 1 };
			if (p > 1) { p = 1 };
			if (p < -1) { p = -1 };
			var m:Number = (Math.log(this.sliderYieldMax)/Math.log(10)) - (Math.log(this.sliderYieldMin)/Math.log(10));
			var b:Number = Math.log(this.sliderYieldMin) / Math.log(10);
			var power:Number = m * Math.abs(p) + b;
			var yieldPerSec:Number = Math.pow(10, power);
			var pOrder:int = Math.round(power);
			var yieldPerCycle:Number = yieldPerSec * (delay / 1000) * sign;
			var yield:Number = MathEx.round(yieldPerCycle, sliderYieldMin);
			if (Math.abs(yield) < this.sliderYieldMin)
			{
				yield = this.sliderYieldMin * sign;
			}
			//trace(yieldPerSec, yieldPerCycle, yield);
			return yield;
			
		}
		private function EndSliderTimer():void
		{
			this.sliderTimer.removeEventListener(TimerEvent.TIMER, SliderTimerEventHandler);
			this.sliderTimer.stop();
		}
		private function SliderMouseDownHandler(e:MouseEvent):void
		{
			stage.addEventListener(MouseEvent.MOUSE_MOVE, SliderMouseMoveHandler);
			stage.addEventListener(MouseEvent.MOUSE_UP, SliderMouseUpHandler);
			//stage.addEventListener(MouseEvent.MOUSE_OUT, SliderMouseUpHandler);
			this.BeginSliderTimer();
			this.sliderLocalStart = e.localX;
		}
		private function SliderMouseMoveHandler(e:MouseEvent):void
		{
			var newLoc:int = this.slider.x + this.slider.globalToLocal(new Point(e.stageX, e.stageY)).x - this.sliderLocalStart;
			if (0 >= newLoc)
			{
				this.slider.x = 0;
			}
			else if (newLoc >= this.sliderTrack.width)
			{
				this.slider.x = this.sliderTrack.width;
			}
			else
			{
				this.slider.x = newLoc;
			}
			
			/*if (e.stageX <= 0 || e.stageY <= 0 || e.stageX >= stage.width || e.stageY >= stage.height)
			{
				var tmp:MouseEvent = new MouseEvent(MouseEvent.MOUSE_UP);
				//SliderMouseUpHandler(tmp);
				trace(e.stageX, e.stageY, stage.width);
			}*/
			this.UpdateText();
		}
		private function SliderMouseUpHandler(e:MouseEvent):void
		{
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, SliderMouseMoveHandler);
			stage.removeEventListener(MouseEvent.MOUSE_UP, SliderMouseUpHandler);
			//stage.removeEventListener(MouseEvent.MOUSE_OUT, SliderMouseUpHandler);
			this.EndSliderTimer();
			this.ResetSlider();
		}
		private function KeyUpHandler(e:KeyboardEvent):void
		{
			var textValue:Number = new Number(this.textDisplay.text);
			var oldText:String = this.textDisplay.text;
			if (textValue.toString() != Number.NaN.toString())
			{
				this.value = textValue;
				if (oldText == "")
				{
					this.textDisplay.text = oldText;
				}
				//this.DispatchValueChangeEvent();
			}
		}
		private function UpdateText():void
		{
			if (Number(textDisplay.text) != this.value)
			{
				this.textDisplay.text = new String(Math.round(this.value*Math.pow(10,textDecimals))/Math.pow(10,textDecimals));
			}
		}
		public function get value():Number
		{
			return valuePrivate;
		}
		public function set value(setValue:Number):void
		{
			//this.textDisplay.text = new String(Math.round(setValue*Math.pow(10,textDecimals))/Math.pow(10,textDecimals));
			if (limitValue)
			{
				if (setValue > maxValue)
				{
					setValue = maxValue;
				}
				if (setValue < minValue)
				{
					setValue = minValue;
				}
			}
			this.valuePrivate = setValue;
			this.UpdateText();
			this.DispatchValueChangeEvent();
		}
		private function DispatchValueChangeEvent():void
		{
			var event:InputAreaEvent = new InputAreaEvent(InputAreaEvent.VALUE_CHANGE);
			this.dispatchEvent(event);
		}
		public function set TextLabel(s:String):void
		{
			this.textLabel.text = s;
		}
		public function get TextLabel():String
		{
			return this.textLabel.text;
		}
	}
	
}
